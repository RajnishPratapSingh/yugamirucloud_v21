using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.Data.SQLite;

namespace CloudManager
{
    public partial class syncSettingsMaster : Form
    {
        public syncSettingsMaster()
        {
            InitializeComponent();
        }
        SQLiteConnection sqlite;

        private void chkCustom_CheckedChanged(object sender, EventArgs e)
        {
            if (chkCustom.Checked)
            {
                frmCustomSyncSettingParams frm = new frmCustomSyncSettingParams();
                frm.ShowDialog();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Dictionary<string, string> KeyConfig = new Dictionary<string, string>();

                KeyConfig.Add("Manual", chkManual.Checked ? "TRUE" : "FALSE");
                KeyConfig.Add("Automatic", chkAutomatic.Checked ? "TRUE" : "FALSE");
                KeyConfig.Add("Custom", chkCustom.Checked ? "TRUE" : "FALSE");
                KeyConfig.Add("TakeLatest_Before_Open", chkTakeLatestOnOpen.Checked ? "TRUE" : "FALSE");
                KeyConfig.Add("Block_Edit_NonLocal_Reports", chkBlockEdit.Checked ? "TRUE" : "FALSE");
                KeyConfig.Add("EditAndSave_As_New", chkEditSaveNew.Checked ? "TRUE" : "FALSE");
                KeyConfig.Add("Offilne_Block_Edit_NonLocal_Reports", chkOfflineBlockEdit.Checked ? "TRUE" : "FALSE");
                KeyConfig.Add("Offline_Allow_Edit_But_Save_As_New", chkOfflineEditSaveAsNew.Checked ? "TRUE" : "FALSE");

                //Update database
                SQLiteDataAdapter ad;
                sqlite = new SQLiteConnection("Data Source=" + initCloudSystem.db_file);
                SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
                cmd = sqlite.CreateCommand();
                string qry = "";
                foreach (KeyValuePair<string, string> column in KeyConfig)
                {
                    qry += "update KeyConfig set User1='" + column.Value + "' where KeyName='" + column.Key.ToString() + "';";
                }
                qry = qry + frmCustomSyncSettingParams.sCustomSetting + "UPDATE KeyConfig SET ActiveStatus = User1;" ;
                cmd.CommandText = qry;
                cmd.ExecuteNonQuery();
                sqlite.Close();
                MessageBox.Show("Settings saved successfully. Changes will take effect after next launch of application.");
                this.Close();
            }
            catch (SQLiteException ex)
            {
                //Add your exception code here.
                MessageBox.Show(ex.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void syncSettingsMaster_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRestoreFactorySettings_Click(object sender, EventArgs e)
        {
            try
            {
                //Update database
                sqlite = new SQLiteConnection("Data Source=" + initCloudSystem.db_file);
                SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
                cmd = sqlite.CreateCommand();
                string qry = "UPDATE KeyConfig SET ActiveStatus = DefaultStatus;";
                cmd.CommandText = qry;
                cmd.ExecuteNonQuery();
                sqlite.Close();
                syncSettingsMaster_Load(this, null);
                MessageBox.Show("Settings saved successfully.");
                this.Close();
            }
            catch { }
        }

        private void syncSettingsMaster_Load(object sender, EventArgs e)
        {
            try
            {
                Dictionary<string, string> DictActiveStatus = CloudUtility.SetCloudSetting();

                chkManual.Checked = Convert.ToBoolean(DictActiveStatus["Manual"].ToString());
                chkAutomatic.Checked = Convert.ToBoolean(DictActiveStatus["Automatic"].ToString());
                chkCustom.CheckedChanged -= chkCustom_CheckedChanged;
                chkCustom.Checked = Convert.ToBoolean(DictActiveStatus["Custom"].ToString());
                chkCustom.CheckedChanged += chkCustom_CheckedChanged;
                chkTakeLatestOnOpen.Checked = Convert.ToBoolean(DictActiveStatus["TakeLatest_Before_Open"].ToString());
                chkBlockEdit.Checked = Convert.ToBoolean(DictActiveStatus["Block_Edit_NonLocal_Reports"].ToString());
                chkEditSaveNew.Checked = Convert.ToBoolean(DictActiveStatus["EditAndSave_As_New"].ToString());
                chkManual.Checked = Convert.ToBoolean(DictActiveStatus["Offilne_Block_Edit_NonLocal_Reports"].ToString());
                chkOfflineEditSaveAsNew.Checked = Convert.ToBoolean(DictActiveStatus["Offline_Allow_Edit_But_Save_As_New"].ToString());
            }
            catch { }
        }
    }
}
