﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudManager
{
   public class PC
    {
        public string SyncID { get; set; }
        public string InstallationName { get; set; }
        public string PCName { get; set; }
        public string InstallationDate { get; set; }
        public string InstalledBy { get; set; }
        public string Comments { get; set; }

        public string Reg_User_ID { get; set; }
        public string Stall_ID { get; set; }

        public string Language { get; set; }
        public string Synced { get; set; }
        public string MaxUserID { get; set; }

    }
}
