﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;

namespace CloudManager
{
    
    public static class SyncSchedulekeyConfigValues
    {
        public static string Manual;
        public static string Automatic;
        public static string Custom;
        public static string TakeLatest_Before_Open;
        public static string Block_Edit_NonLocal_Reports;
        public static string EditAndSave_As_New;
        public static string Offilne_Block_Edit_NonLocal_Reports;
        public static string Offline_Allow_Edit_But_Save_As_New;
        public static string If_Application_Is_Idle_For_More_Than_Minute;
        public static string If_Application_Is_Idle_Minute_Value;
        public static string Between_From_To_Date;
        public static string Between_From_Time_Value;
        public static string Between_To_Time_Value;
        public static string Closing_Application;
        public static string Starting_Application;
        public static System.Timers.Timer timer;

        private static string selectAllQuery = "select id, frm_reference, keyname, defaultstatus, bestspeed, user1, user2, activestatus  from KeyConfig;";
        static SyncSchedulekeyConfigValues()
        {
            timer = new System.Timers.Timer(30000); //300000);//5 Minutes [nx1000 seconds]
            timer.AutoReset = true;// false;
            timer.Elapsed += Timer_Elapsed;

            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection("Data Source=" + initCloudSystem.db_file);
            SQLiteCommand cmd;
            sqlite.Open();  //Initiate connection to the db
            cmd = sqlite.CreateCommand();

            cmd.CommandText = selectAllQuery;
            DataTable dt = new DataTable();
            SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
            da.Fill(dt);
            if (dt.Rows != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["keyname"].ToString().ToUpper().Trim() == "MANUAL")
                    {
                        Manual = dr["ActiveStatus"].ToString().ToUpper();
                    }
                    else if (dr["keyname"].ToString().ToUpper().Trim() == "Automatic".ToUpper())
                    {
                        Automatic = dr["ActiveStatus"].ToString().ToUpper();
                    }
                    else if (dr["keyname"].ToString().ToUpper().Trim() == "Custom".ToUpper())
                    {
                        Custom = dr["ActiveStatus"].ToString().ToUpper();
                    }
                    else if (dr["keyname"].ToString().ToUpper().Trim() == "TakeLatest_Before_Open".ToUpper())
                    {
                        TakeLatest_Before_Open = dr["ActiveStatus"].ToString().ToUpper();
                    }
                    else if (dr["keyname"].ToString().ToUpper().Trim() == "Block_Edit_NonLocal_Reports".ToUpper())
                    {
                        Block_Edit_NonLocal_Reports = dr["ActiveStatus"].ToString().ToUpper();
                    }
                    else if (dr["keyname"].ToString().ToUpper().Trim() == "EditAndSave_As_New".ToUpper())
                    {
                        EditAndSave_As_New = dr["ActiveStatus"].ToString().ToUpper();
                    }
                    else if (dr["keyname"].ToString().ToUpper().Trim() == "Offilne_Block_Edit_NonLocal_Reports".ToUpper())
                    {
                        Offilne_Block_Edit_NonLocal_Reports = dr["ActiveStatus"].ToString().ToUpper();
                    }
                    else if (dr["keyname"].ToString().ToUpper().Trim() == "Offline_Allow_Edit_But_Save_As_New".ToUpper())
                    {
                        Offline_Allow_Edit_But_Save_As_New = dr["ActiveStatus"].ToString().ToUpper();
                    }
                    else if (dr["keyname"].ToString().ToUpper().Trim() == "If_Application_Is_Idle_For_More_Than_Minute".ToUpper())
                    {
                        If_Application_Is_Idle_For_More_Than_Minute = dr["ActiveStatus"].ToString().ToUpper().Split('_')[0];
                        If_Application_Is_Idle_Minute_Value = dr["ActiveStatus"].ToString().ToUpper().Split('_')[1];
                    }
                    else if (dr["keyname"].ToString().ToUpper().Trim() == "Between_From_To_Date".ToUpper())
                    {
                        Between_From_To_Date = dr["ActiveStatus"].ToString().ToUpper().Split('_')[0];
                        try
                        {
                            Between_From_Time_Value = dr["ActiveStatus"].ToString().ToUpper().Split('_')[1];
                            Between_To_Time_Value = dr["ActiveStatus"].ToString().ToUpper().Split('_')[2];
                        }
                        catch
                        {
                            Between_From_Time_Value = "0";
                            Between_To_Time_Value = "0";
                        }
                    }
                    else if (dr["keyname"].ToString().ToUpper().Trim() == "Closing_Application".ToUpper())
                    {
                        Closing_Application = dr["ActiveStatus"].ToString().ToUpper();
                    }
                    else if (dr["keyname"].ToString().ToUpper().Trim() == "Starting_Application".ToUpper())
                    {
                        Starting_Application = dr["ActiveStatus"].ToString().ToUpper();
                    }                    
                }
            }
            if (Automatic.ToBool() || (Custom.ToBool() && (Between_From_To_Date.ToBool() || If_Application_Is_Idle_For_More_Than_Minute.ToBool())))
                timer.Start();
        }

        private static void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
             //Test System.Windows.Forms.MessageBox.Show("Timer Ticked");
            if (CloudManager.SyncSchedulekeyConfigValues.Custom.ToBool())
            {
                System.Threading.Thread t = new System.Threading.Thread(DoSyncAfterIdle);
                t.Start();
            }
        }

        public static void DoSyncAfterIdle()
        {
            TimeSpan tpLastUpdate = DateTime.Now - CloudManager.SynchronizationScheduler.lastUpdatedTime;
            if (tpLastUpdate.TotalMinutes < 1)
            {
                return;
            }

            if (CloudManager.SyncSchedulekeyConfigValues.If_Application_Is_Idle_For_More_Than_Minute.ToBool())
            {
                TimeSpan tplastActive = DateTime.Now - CloudManager.SynchronizationScheduler.lastActivityTime;
                if (Math.Round(tplastActive.TotalMinutes) > Convert.ToInt16(CloudManager.SyncSchedulekeyConfigValues.If_Application_Is_Idle_Minute_Value))
                {
                    CloudManager.CloudSyncManager.SynchronizeAll();
                }
            }
            else if(CloudManager.SyncSchedulekeyConfigValues.Between_From_To_Date.ToBool())
            {
                try
                {
                    int fromHour = Convert.ToInt32(CloudManager.SyncSchedulekeyConfigValues.Between_From_Time_Value.ToCharArray()[0] +
                                                        CloudManager.SyncSchedulekeyConfigValues.Between_From_Time_Value.ToCharArray()[1]);
                    int fromMinute = Convert.ToInt32(CloudManager.SyncSchedulekeyConfigValues.Between_From_Time_Value.ToCharArray()[2] +
                                                        CloudManager.SyncSchedulekeyConfigValues.Between_From_Time_Value.ToCharArray()[3]);

                    int toHour = Convert.ToInt32(CloudManager.SyncSchedulekeyConfigValues.Between_To_Time_Value.ToCharArray()[0] +
                                                        CloudManager.SyncSchedulekeyConfigValues.Between_To_Time_Value.ToCharArray()[1]);
                    int toMinute = Convert.ToInt32(CloudManager.SyncSchedulekeyConfigValues.Between_To_Time_Value.ToCharArray()[2] +
                                                        CloudManager.SyncSchedulekeyConfigValues.Between_To_Time_Value.ToCharArray()[3]);
                    int curHour = DateTime.Now.Hour;
                    int curMinute = DateTime.Now.Minute;
                    if (fromHour < curHour && curHour < toHour)
                    {
                        CloudManager.CloudSyncManager.SynchronizeAll();
                    }
                }
                catch(Exception ex)
                {
                    throw ex;
                }
            }
            //CloudManager.SynchronizationScheduler.lastActivityTime = DateTime.Now;  

        }
    }


    public static class SynchronizationScheduler
    {
        public static DateTime lastActivityTime { get; set; }
        public static DateTime lastUpdatedTime { get; set; }
        public static DateTime NextUpdateToBeAtTime { get; set; }        
    }
    
    
}
