﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;

namespace CloudManager
{
    public static class DBHandler
    {

        public static string QryConverter(DataTable dtToConvert, string tableName)
        {
            string qrytemplate = "INSERT OR REPLACE into " + tableName + "(col#) values(val#)";
            if(dtToConvert==null || dtToConvert.Rows.Count==0 || !dtToConvert.Columns.Contains("uniqueid"))
            {
                throw new FormatException("table is not in desired format");
            }
            string cols = string.Empty;
            string vals = string.Empty;

            //-------------Added By Sumit on 28-Dec-18 for GSP-983 START-------------
            string dtRegUserID = (tableName.ToUpper() == "PatientDetails".ToUpper()) ? dtToConvert.Rows[0]["reg_user_id"].ToString().Trim() : "";
            //string stall_id = (tableName.ToUpper() == "PatientDetails".ToUpper()) ? CloudManager.GlobalItems.Get_Stall_ID(dtRegUserID).Trim() : "";
            string stall_id = (dtToConvert.Columns.Contains("Stall_ID")) ? dtToConvert.Rows[0]["Stall_ID"].ToString() : GlobalItems.MY_StallID;
            string owners_unique_ID = "";
            //-------------Added By Sumit on 28-Dec-18 for GSP-983 END---------------


            foreach (DataRow dr in dtToConvert.Rows)
            {
                foreach (DataColumn cl in dtToConvert.Columns)
                {
                    cols = cols + "," + cl.ColumnName;
                    vals = vals+ ",'" + dr[cl.ColumnName].ToString().Trim()+"'";
                }
                //-------------Added By Sumit on 29-Dec-18 for GSP-983 START-------------
                if(tableName.ToUpper()=="PatientDetails".ToUpper())
                {
                    if(dtRegUserID.Trim()==GlobalItems.MY_Reg_User_ID && stall_id.Trim()==GlobalItems.MY_StallID)
                    {
                        owners_unique_ID = "-1";    //Self System
                        cols = cols + "," + "owners_unique_ID";
                        vals = vals + ",'" + owners_unique_ID + "'";

                        cols = cols + "," + "lut";
                        vals = vals + ",'" + "" + "'";

                        cols = cols + "," + "Stall_ID";
                        vals = vals + ",'" + GlobalItems.MY_StallID + "'";

                    }
                    else
                    {
                        //owners_unique_ID = dtToConvert.Rows[0]["UniqueID"].ToString();    //Self System
                        //owners_unique_ID = dtToConvert.Rows[0]["owners_unique_ID"].ToString();    //Self System
                        //cols = cols + "," + "owners_unique_ID";
                        //vals = vals + ",'" + owners_unique_ID + "'";

                        cols = cols + "," + "lut";
                        vals = vals + ",'" + "" + "'";

                        //cols = cols + "," + "Stall_ID";
                        //vals = vals + ",'" + GlobalItems.MY_StallID + "'";
                    }
                }
                //-------------Added By Sumit on 29-Dec-18 for GSP-983 END---------------


                cols = cols.Trim(',');
                vals = vals.Replace("\0\r","").Trim(',');
            }

            string finalQry = qrytemplate.Replace("col#", cols).Replace("val#", vals) + ";";

            return finalQry;
        }

        public static bool ExecQry(string qry,string UniqueId,Tables tableNme)
        {
            try
            {
                if (UniqueId.Trim() == "0")
                {
                    throw new Exception("Invalid command: ID cannot be 0");
                }
                SQLiteConnection sqlite = new SQLiteConnection("Data Source=" + GlobalItems.sqliteDBPath);
                SQLiteCommand cmd = new SQLiteCommand();
                if (tableNme != Tables.PatientDetails)
                {
                    //delete existing
                    cmd = sqlite.CreateCommand();
                    cmd.CommandText = "delete from " + tableNme.ToString() + " where uniqueid='" + UniqueId + "'";
                    //cmd = sqlite.CreateCommand();
                    //cmd.CommandText = qry;
                    sqlite.Open();
                    cmd.ExecuteNonQuery();
                    sqlite.Close();
                }
                //insert new
                sqlite.Open();
                //cmd = new SQLiteCommand();
                cmd = sqlite.CreateCommand();
                cmd.CommandText = qry;
                cmd.ExecuteNonQuery();
                sqlite.Close();
            }
            catch (SQLiteException ex)
            {
                //Add your exception code here.
                //MessageBox.Show(ex.Message);
                throw ex;
            }
            return true;
        }

        public static bool ResetLut(string id)
        {
            #region resetting
            string updateQry = "update PatientDetails set lut='' where uniqueid=" + id + ";";
            SQLiteConnection sqlite = new SQLiteConnection("Data Source=" + GlobalItems.sqliteDBPath);
            SQLiteCommand cmd = new SQLiteCommand();
            cmd=sqlite.CreateCommand();
            //delete existing
            cmd.CommandText = updateQry;            
            sqlite.Open();
            cmd.ExecuteNonQuery();
            sqlite.Close();
            #endregion
            return true;
        }

        /// <summary>
        /// add some string values to imagesbytes data (at data start and at data end) fixed to 'thiscannotbeacoincedenceatstart' and 'atendthisisalsonotacoincedence'
        /// both string one for start another for end is fixed, these will be appended with Imagebytes data
        /// </summary>
        /// <param name="dt">table must have one row only</param>
        /// <returns></returns>
        public static DataTable AddToImageBytesBeforeSend(DataTable dt)
        {
            foreach (DataRow dr in dt.Rows)
            {
                foreach (DataColumn dc in dt.Columns)
                {
                    if (dc.ColumnName.ToUpper() == "IMAGEBYTES")
                    {
                        dr[dc] = "thiscannotbeacoincedenceatstart" + dr[dc].ToString() + "atendthisisalsonotacoincedence";
                        break;
                    }
                }
            }

            return dt;
        }

        public static bool CheckIsRecordExist(Tables tableToCheckIn, string columnName, string valueToCheck)
        {
            bool exist = false;

            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection("Data Source=" + initCloudSystem.db_file);
            SQLiteCommand cmd;
            sqlite.Open();  //Initiate connection to the db
            cmd = sqlite.CreateCommand();

            cmd.CommandText = "select 1 from " + tableToCheckIn.ToString() + " where " + columnName + "='" + valueToCheck + "';";
            DataTable dt = new DataTable();
            SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
            da.Fill(dt);
            if (dt.Rows != null && dt.Rows.Count == 1)
            {
                if(dt.Rows[0][0].ToString().Trim()=="1")
                {
                    exist = true;
                }
            }
            return exist;
        }
        public static string GetUniqueIDFromPatientTable(string server_user_id)
        {
            string id = string.Empty ;

            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection("Data Source=" + initCloudSystem.db_file);
            SQLiteCommand cmd;
            sqlite.Open();  //Initiate connection to the db
            cmd = sqlite.CreateCommand();

            cmd.CommandText = "select uniqueid from PatientDetails where server_user_id='"+server_user_id+"';";
            DataTable dt = new DataTable();
            SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
            da.Fill(dt);
            if (dt.Rows != null && dt.Rows.Count == 1)
            {
                id = dt.Rows[0][0].ToString().Trim();
            }
            return id;
        }

        public static string GetMax_UniqueID()
        {
            string maxId = string.Empty;

            SQLiteConnection sqlite;
            sqlite = new SQLiteConnection("Data Source=" + initCloudSystem.db_file);
            SQLiteCommand cmd;
            sqlite.Open();  //Initiate connection to the db
            cmd = sqlite.CreateCommand();

            cmd.CommandText = "select Max(uniqueid) from PatientDetails";
            DataTable dt = new DataTable();
            SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
            da.Fill(dt);
            if (dt.Rows != null && dt.Rows.Count == 1)
            {
                maxId = dt.Rows[0][0].ToString().Trim();
            }
            if(maxId.Trim()=="")
            {
                maxId = "0";
            }
            return maxId;
        }

    }
}
