﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using System.Data.SQLite;

namespace CloudManager
{
    /// <summary>
    /// Implemented By Sumit For Release V 2.1 (Multilicense System)
    /// </summary>
    public static class CloudSyncManager
    {
        static List<PC> lstPCsToSync = new List<PC>();
        static CloudSyncManager()
        {
            RefreshPCsToSync();
        }
        public static bool RefreshPCsToSync()
        {
            bool success = false;

            SQLiteConnection sqlite;
            DataTable dt = new DataTable();
            SQLiteDataAdapter ad;
            string Server_User_ID = "";
            try
            {
                sqlite = new SQLiteConnection("Data Source=" + initCloudSystem.db_file);
                SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
                cmd = sqlite.CreateCommand();
                cmd.CommandText = "select * from tblInstallInfo where Sync='TRUE'";  //set the passed query
                ad = new SQLiteDataAdapter(cmd);
                ad.Fill(dt); //fill the datasource                
                sqlite.Close();
                lstPCsToSync = new List<PC>();

                foreach (DataRow dr in dt.Rows)
                {
                    PC pc = new PC();
                    pc.SyncID = dr["ID"].ToString();

                    pc.Comments = dr["Comment"].ToString();
                    pc.InstallationDate = dr["Date_of_Install"].ToString();
                    pc.InstallationName = dr["Installation_Name"].ToString();
                    pc.InstalledBy = dr["Installed_By"].ToString();
                    pc.Language = dr["Language"].ToString();
                    pc.MaxUserID = dr["Max_User_ID"].ToString();//Presently not updating DB
                    pc.PCName = dr["Computer_Name"].ToString();
                    pc.Reg_User_ID = dr["Reg_User_ID"].ToString();
                    pc.Stall_ID = dr["Stall_ID"].ToString();
                    pc.Synced = dr["Sync"].ToString();//For present logic will be true only but soon will need all PCs
                    lstPCsToSync.Add(pc);
                }
                success = true;
            }
            catch (SQLiteException ex)
            {
                throw ex;
                //Add your exception code here.
                //MessageBox.Show(ex.Message);
            }


            return success;
        }
        public static bool SynchronizeAll()
        {
            if(!WebComCation.Utility.IsInternetConnected())
            {
                return false ;
            }

            bool syncSuccess = false;
            try
            {
                foreach (PC p in lstPCsToSync)
                {
                    SyncOne(p);
                }
                syncSuccess = true;
            }
            catch (Exception e)
            {
                throw e;
            }
            CloudManager.SynchronizationScheduler.lastUpdatedTime = DateTime.Now;
            return syncSuccess;
        }

        public static SyncResults SyncOne(PC pcToSync)
        {
            SyncResults syncResult = SyncResults.None;

            //First fetch the max available server_user_id
            //string maxUserID = GlobalItems.Get_Max_Server_User_ID(pcToSync.Reg_User_ID, pcToSync.Stall_ID);

            //start infinite loop for receiving unknown amount of data. and break it once done.


            string server_user_id = GlobalItems.Get_Max_Server_User_ID(pcToSync.Reg_User_ID, pcToSync.Stall_ID);

            // for (int k=0;k<0 ;k++ )            
            TryGetNext:
            {
                string strBsonPatient = GetBsonFromServer.getBsonFromServer(JsonRequestForMultiSystem.GetCustomJsonReq(pcToSync, Tables.PatientDetails, server_user_id), "0", CloudUrls.PatientDetailsGetFromUrl);
                if (strBsonPatient.Contains("Data Doesnot exist") && strBsonPatient.Contains("401"))
                {
                    return syncResult;
                }

                DataTable dtPatient = BsonParser.SpliterPatientDetails(strBsonPatient);
                strBsonPatient = string.Empty;
                //Need to modify tables so that existing code can be utilized for DB insertion.
                //Note 1: received uniqueid is not of this system it may already exist here.
                //1.  Make this received uniqueid as'owners_unique_id' before insertion.
                //2.  First Search local DB for received User_ID which is known as server_user_id in local DB
                //3.1 and if it exist then fetch the local uniqueid and then set this uniqueid in all 4 dts to be inserted
                //3.2 or if not exist then fetch then max uniqueid of local database and increment by one then set this value in
                //         all 4 dts to be inserted.
                //    
                string insertUpdateUID = ProcessPatientDetails(pcToSync, dtPatient);
                if (insertUpdateUID == "-1")
                    return SyncResults.NoNewRecord;
                else if (insertUpdateUID == "0")
                    return SyncResults.LocalDBError;

                //
                dtPatient.Dispose();

                string strBsonKneeDown = GetBsonFromServer.getBsonFromServer(JsonRequestForMultiSystem.GetCustomJsonReq(pcToSync, Tables.FrontBodyPositionKneeDown, server_user_id), "0", CloudUrls.FrontBodyPositionKneeDownGetFromUrl);
                DataTable dtKnee = BsonParser.SpliterFrontBodyPositionKneedown(strBsonKneeDown, Tables.FrontBodyPositionKneeDown.ToString());
                strBsonKneeDown = string.Empty;
                dtKnee.Rows[0]["UniqueID"] = insertUpdateUID;
                string q = DBHandler.QryConverter(dtKnee, Tables.FrontBodyPositionKneeDown.ToString());
                ////===insert into database 
                bool insertStatus = DBHandler.ExecQry(q, insertUpdateUID, Tables.FrontBodyPositionKneeDown);



                string strBsonFrontBody = GetBsonFromServer.getBsonFromServer(JsonRequestForMultiSystem.GetCustomJsonReq(pcToSync, Tables.FrontBodyPositionStanding, server_user_id), "0", CloudUrls.FrontBodyPositionStandingGetFromUrl);
                DataTable dtFront = BsonParser.SpliterFrontBodyPositionStanding(strBsonFrontBody);
                strBsonFrontBody = string.Empty;
                dtFront.Rows[0]["UniqueID"] = insertUpdateUID;
                q = DBHandler.QryConverter(dtFront, Tables.FrontBodyPositionStanding.ToString());
                ////===insert into database 
                insertStatus = DBHandler.ExecQry(q, insertUpdateUID, Tables.FrontBodyPositionStanding);


                string strBsonSideBody = GetBsonFromServer.getBsonFromServer(JsonRequestForMultiSystem.GetCustomJsonReq(pcToSync, Tables.SideBodyPosition, server_user_id), "0", CloudUrls.SideBodyPositionGetFromUrl);
                DataTable dtSide = BsonParser.SpliterSideBodyPosition(strBsonSideBody);
                strBsonSideBody = string.Empty;
                dtSide.Rows[0]["UniqueID"] = insertUpdateUID;
                q = DBHandler.QryConverter(dtSide, Tables.SideBodyPosition.ToString());
                ////===insert into database 
                insertStatus = DBHandler.ExecQry(q, insertUpdateUID, Tables.SideBodyPosition);

                syncResult = SyncResults.Success;

                string nowLatestMaxServerUserID = GlobalItems.Get_Max_Server_User_ID(pcToSync.Reg_User_ID, pcToSync.Stall_ID);
                if (server_user_id.Trim() != nowLatestMaxServerUserID.Trim())
                {
                    server_user_id = nowLatestMaxServerUserID;
                    goto TryGetNext;
                }


            }
            return syncResult;
        }
        /// <summary>
        /// Implemented By Rajnish for Search Screen Selected Record
        /// </summary>
        /// <param name="pcToSync"></param>
        /// <returns></returns>
        public static SyncResults SyncSelectedOne(PC pcToSync)
        {
            SyncResults syncResult = SyncResults.None;
            //First fetch the max available server_user_id
            //string maxUserID = GlobalItems.Get_Max_Server_User_ID(pcToSync.Reg_User_ID, pcToSync.Stall_ID);
            //start infinite loop for receiving unknown amount of data. and break it once done.
            DataTable dtRecordsToUpdate = GlobalItems.Get_Records_To_Update(pcToSync.Reg_User_ID, pcToSync.Stall_ID);
            if (dtRecordsToUpdate.Rows.Count > 0)
            {
                bool IsFirstRow = true;
                for (int i = 0; i < dtRecordsToUpdate.Rows.Count; i++)
                {
                    String strJonToSend = string.Empty;
                    strJonToSend = "[{\"Computerid\":\"" + "0" + "\"," + Environment.NewLine;
                    strJonToSend += "\"Activationkey\":\"" + "0" + "\"," + Environment.NewLine;
                    strJonToSend += "\"UniqueId\":\"" + dtRecordsToUpdate.Rows[i]["UniqueId"] + "\"," + Environment.NewLine;
                    strJonToSend += "\"user_id\":\"" + dtRecordsToUpdate.Rows[i]["Server_User_ID"] + "\"," + Environment.NewLine;// 0 for own record-- will need to be edited for edit other's record feature
                    strJonToSend += "\"reg_user_id\":\"" + dtRecordsToUpdate.Rows[i]["reg_user_id"] + "\"," + Environment.NewLine;
                    strJonToSend += "\"Install_id\":\"" + dtRecordsToUpdate.Rows[i]["Stall_ID"] + "\"," + Environment.NewLine;
                    strJonToSend += "\"update_counter\":\"" + dtRecordsToUpdate.Rows[i]["update_counter"] + "\"," + Environment.NewLine;//Added by Rajnish For Update counter
                    strJonToSend = strJonToSend.Remove(strJonToSend.LastIndexOf(Environment.NewLine));
                    strJonToSend = strJonToSend.Remove(strJonToSend.LastIndexOf(","));
                    strJonToSend = strJonToSend + "}]";

                    string responseFromServer = JSONManager.SendJSON_StrToUrl(strJonToSend, CloudUrls.GetUpdateCounterFromUrl);
                    string server_user_id = IsFirstRow ? "0" : dtRecordsToUpdate.Rows[i - 1]["Server_User_ID"].ToString();
                    string uniqueid = dtRecordsToUpdate.Rows[i]["UniqueId"].ToString();
                    IsFirstRow = false;

                    if (responseFromServer.Contains("Latest Update Counter value"))
                    {
                        //string server_user_id = GlobalItems.Get_Max_Server_User_ID(pcToSync.Reg_User_ID, pcToSync.Stall_ID);         
                        //TryGetNext:
                        {

                            string strBsonPatient = GetBsonFromServer.getBsonFromServer(JsonRequestForMultiSystem.GetCustomJsonReq(pcToSync, Tables.PatientDetails, server_user_id), uniqueid, CloudUrls.PatientDetailsGetFromUrl);
                            if (strBsonPatient.Contains("Data Doesnot exist") && strBsonPatient.Contains("401"))
                            {
                                return syncResult;
                            }
                            DataTable dtPatient = BsonParser.SpliterPatientDetails(strBsonPatient);
                            strBsonPatient = string.Empty;
                            //Need to modify tables so that existing code can be utilized for DB insertion.
                            //Note 1: received uniqueid is not of this system it may already exist here.
                            //1.  Make this received uniqueid as'owners_unique_id' before insertion.
                            //2.  First Search local DB for received User_ID which is known as server_user_id in local DB
                            //3.1 and if it exist then fetch the local uniqueid and then set this uniqueid in all 4 dts to be inserted
                            //3.2 or if not exist then fetch then max uniqueid of local database and increment by one then set this value in
                            //         all 4 dts to be inserted.
                            //    
                            string insertUpdateUID = ProcessPatientDetails(pcToSync, dtPatient);
                            if (insertUpdateUID == "-1")
                                return SyncResults.NoNewRecord;
                            else if (insertUpdateUID == "0")
                                return SyncResults.LocalDBError;
                            dtPatient.Dispose();

                            string strBsonKneeDown = GetBsonFromServer.getBsonFromServer(JsonRequestForMultiSystem.GetCustomJsonReq(pcToSync, Tables.FrontBodyPositionKneeDown, server_user_id), "0", CloudUrls.FrontBodyPositionKneeDownGetFromUrl);
                            DataTable dtKnee = BsonParser.SpliterFrontBodyPositionKneedown(strBsonKneeDown, Tables.FrontBodyPositionKneeDown.ToString());
                            strBsonKneeDown = string.Empty;
                            dtKnee.Rows[0]["UniqueID"] = insertUpdateUID;
                            string q = DBHandler.QryConverter(dtKnee, Tables.FrontBodyPositionKneeDown.ToString());

                            bool insertStatus = DBHandler.ExecQry(q, insertUpdateUID, Tables.FrontBodyPositionKneeDown);
                            string strBsonFrontBody = GetBsonFromServer.getBsonFromServer(JsonRequestForMultiSystem.GetCustomJsonReq(pcToSync, Tables.FrontBodyPositionStanding, server_user_id), "0", CloudUrls.FrontBodyPositionStandingGetFromUrl);
                            DataTable dtFront = BsonParser.SpliterFrontBodyPositionStanding(strBsonFrontBody);
                            strBsonFrontBody = string.Empty;
                            dtFront.Rows[0]["UniqueID"] = insertUpdateUID;
                            q = DBHandler.QryConverter(dtFront, Tables.FrontBodyPositionStanding.ToString());

                            insertStatus = DBHandler.ExecQry(q, insertUpdateUID, Tables.FrontBodyPositionStanding);
                            string strBsonSideBody = GetBsonFromServer.getBsonFromServer(JsonRequestForMultiSystem.GetCustomJsonReq(pcToSync, Tables.SideBodyPosition, server_user_id), "0", CloudUrls.SideBodyPositionGetFromUrl);
                            DataTable dtSide = BsonParser.SpliterSideBodyPosition(strBsonSideBody);
                            strBsonSideBody = string.Empty;
                            dtSide.Rows[0]["UniqueID"] = insertUpdateUID;
                            q = DBHandler.QryConverter(dtSide, Tables.SideBodyPosition.ToString());

                            insertStatus = DBHandler.ExecQry(q, insertUpdateUID, Tables.SideBodyPosition);
                            syncResult = SyncResults.Success;
                            //string nowLatestMaxServerUserID = GlobalItems.Get_Max_Server_User_ID(pcToSync.Reg_User_ID, pcToSync.Stall_ID);
                            //if (server_user_id.Trim() != nowLatestMaxServerUserID.Trim() && !IsFirstRow && dtRecordsToUpdate.Rows.Count > 1)
                            //{
                            //    server_user_id = nowLatestMaxServerUserID;
                            //    goto TryGetNext;
                            //}                            
                        }
                    }
                }
            }
            return syncResult;
        }



        //Internal Private Classes
        static string ProcessPatientDetails(PC pcToSync, DataTable dtPatient)
        {

            //Need to modify tables so that existing code can be utilized for DB insertion.
            //Note 1: received uniqueid is not of this system it may already exist here.
            //1.  Make this received uniqueid as'owners_unique_id' before insertion.
            //2.  First Search local DB for received User_ID which is known as server_user_id in local DB
            //3.1 and if it exist then fetch the local uniqueid and then set this uniqueid in all 4 dts to be inserted
            //3.2 or if not exist then fetch then max uniqueid of local database and increment by one then set this value in
            //         all 4 dts to be inserted.
            //  
            string insertUpdateUniqueID = "0";
            string server_user_Id = string.Empty;
            try
            {
                if (dtPatient.Rows != null && dtPatient.Rows.Count > 0)
                {
                    string owners_unique_id = dtPatient.Rows[0]["UniqueId"].ToString();
                    dtPatient.Columns.Add("owners_unique_id", typeof(System.String));
                    dtPatient.Rows[0]["owners_unique_id"] = owners_unique_id;

                    dtPatient.Columns.Add("Stall_ID", typeof(System.String));
                    dtPatient.Rows[0]["Stall_ID"] = pcToSync.Stall_ID;

                    dtPatient.Columns["user_id"].ColumnName = "server_user_id";

                    server_user_Id = dtPatient.Rows[0]["server_user_id"].ToString();

                    //Special Char Handling-
                    foreach (DataRow dr in dtPatient.Rows)
                    {
                        //PatientId column
                        dr["PatientId"] = SplCharHandler.GetNormalString(dr["PatientId"].ToString());
                        if (dr["PatientId"].ToString().Contains("'"))
                        {
                            dr["PatientId"] = dr["PatientId"].ToString().Replace("'", @"''");
                        }
                        //Name column
                        dr["Name"] = SplCharHandler.GetNormalString(dr["Name"].ToString());
                        if (dr["Name"].ToString().Contains("'"))
                        {
                            dr["Name"] = dr["Name"].ToString().Replace("'", @"''");
                        }
                        //Comment Column
                        dr["Comment"] = SplCharHandler.GetNormalString(dr["Comment"].ToString());
                        
                        dr["Comment"] = System.Text.RegularExpressions.Regex.Replace(dr["Comment"].ToString(), @"[^\u0020-\u007E]", string.Empty);

                        if (dr["Comment"].ToString().Contains("'"))
                        {
                            dr["Comment"] = dr["Comment"].ToString().Replace("'", @"''");
                        }

                    }
                    //Special Char Done
                }
                else
                {
                    insertUpdateUniqueID = "-1";//will indicate no data to insert
                    return insertUpdateUniqueID;
                }
                //Database operations for table PatientDetails
                //Check if server_user_id exist in local DB
                bool recordExist = DBHandler.CheckIsRecordExist(Tables.PatientDetails, "server_user_id", server_user_Id);
                bool insertStatus = false;

                //Decide Update or insert
                if (recordExist) //update
                {
                    // As record exist already we need to fetch the local DB's UniqueId for updating this record.
                    string LocalUniqueIDToUpdate = DBHandler.GetUniqueIDFromPatientTable(server_user_Id);

                    dtPatient.Rows[0]["UniqueId"] = LocalUniqueIDToUpdate;
                    string pdqr = DBHandler.QryConverter(dtPatient, Tables.PatientDetails.ToString());
                    ////===insert into database 

                    insertStatus = DBHandler.ExecQry(pdqr, LocalUniqueIDToUpdate, Tables.PatientDetails);

                    insertUpdateUniqueID = LocalUniqueIDToUpdate;

                }
                else //Insert as New Record
                {
                    //As the record doesn't exist yet in local DB, we need max available UniqueId of local DB
                    //then we can increment it by one then can utilize our existing qry builder function.
                    string maxLocalUniqueID = DBHandler.GetMax_UniqueID().Trim();
                    string nextMaxUID = (Convert.ToUInt32(maxLocalUniqueID) + 1).ToString();
                    dtPatient.Rows[0]["UniqueId"] = nextMaxUID;
                    string pdqr = DBHandler.QryConverter(dtPatient, Tables.PatientDetails.ToString());
                    insertStatus = DBHandler.ExecQry(pdqr, nextMaxUID, Tables.PatientDetails);
                    insertUpdateUniqueID = nextMaxUID;
                }


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return insertUpdateUniqueID;
        }


    }



    public static class JsonRequestForMultiSystem
    {
        static string oneBaseJson = @"[{""TableName"":""" + "TableNamekgjyftyfjvjhg" + @""",""Computerid"":""ComputerIdhgjyfjhg"",""Activationkey"":""activationKeykgjyftyfjvjhg"",""UniqueId"":""0"",""user_id"":""user_idkgjyftyfjvjhg"",""reg_user_id"":""reg_user_idkgjyftyfjvjhg"",""Install_id"":""install_idkgjyftyfjvjhg""}]";



        public static string GetCustomJsonReq(PC pc, Tables tableName, string maxServerUserID = "")
        {

            string customJson = string.Empty;

            string activationKey = "0";
            string computerId = "0";
            if (pc.Synced.ToString().Trim() == "THIS")
            {
                activationKey = GlobalItems.ActivationKey;
                computerId = GlobalItems.ComputerID;
            }

            if (maxServerUserID == "")
                maxServerUserID = GlobalItems.Get_Max_Server_User_ID(pc.Reg_User_ID, pc.Stall_ID);

            customJson = oneBaseJson.Replace("TableNamekgjyftyfjvjhg", tableName.ToString())
                                     .Replace("ComputerIdhgjyfjhg", computerId)
                                     .Replace("activationKeykgjyftyfjvjhg", activationKey)
                                     .Replace("reg_user_idkgjyftyfjvjhg", pc.Reg_User_ID)
                                     .Replace("install_idkgjyftyfjvjhg", pc.Stall_ID)
                                     //.Replace("user_idkgjyftyfjvjhg", GlobalItems.Get_Max_Server_User_ID(pc.Reg_User_ID, pc.Stall_ID));
                                     .Replace("user_idkgjyftyfjvjhg", maxServerUserID);



            return customJson;
        }

    }
}
