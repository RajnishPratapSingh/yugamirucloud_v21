﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Data.SQLite;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;

namespace CloudManager
{
    public static class BsonParser
    {
        //var outputs = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8, Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty), new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(input)));


        //outputs =  Regex.Replace(outputs, "[^a-zA-Z0-9]+", " ");

        public static DataTable PatientDetailsBSONtoDT(string bsonstring)
        {
        
            //Read one row from PatientDetails table with additional JSON columns
            DataTable tablesPatientDetails = ColumnNamesPatientDetails(bsonstring);
            return tablesPatientDetails;
        }
        public static DataTable FrontBodyPositionKneeDownBSONtoDT(string bsonstring)
        {

            //Read one row from FrontBodyPositionKneedown table with additional JSON columns
            DataTable tablesPatientDetails = ColumnNamesFrontBodyPositionKneedown(bsonstring);
            return tablesPatientDetails;
        }
        public static DataTable FrontBodyPositionStandingBSONtoDT(string bsonstring)
        {

            //Read one row from FrontBodyPositionStanding table with additional JSON columns
            //DataTable tablesPatientDetails = ColumnNamesPatientDetails(bsonstring);
            DataTable tablesPatientDetails = ColumnNamesFrontBodyPositionStanding(bsonstring);
            return tablesPatientDetails;
        }
        public static DataTable SideBodyPositionBSONtoDT(string bsonstring)
        {

            //Read one row from sideBodyPosition table with additional JSON columns
            //DataTable tablesPatientDetails = ColumnNamesPatientDetails(bsonstring);
            DataTable tablesPatientDetails = ColumnNamesSideBodyPosition(bsonstring);
            return tablesPatientDetails;
        }

        public static DataTable ColumnNamesPatientDetails(string bsonData)
        {
            List<string> lstCols = new List<string>();
            string sqliteDBPath = GlobalItems.sqliteDBPath;//@"E:\GSPORT\SQLDBTest\cpdb.sqlite";
            SQLiteConnection AppDBCon = new SQLiteConnection("Data Source=" + sqliteDBPath);
            try
            {
                SQLiteCommand cmd;
                AppDBCon.Open();
                cmd = AppDBCon.CreateCommand();
                //cmd.CommandText = "SELECT * FROM SideBodyPosition ORDER BY ROWID ASC LIMIT 1;";//patientdetails
                //cmd.CommandText = "SELECT * FROM patientdetails ORDER BY ROWID ASC LIMIT 1;";//
                //cmd.CommandText = "SELECT * FROM FrontBodyPositionStanding ORDER BY ROWID ASC LIMIT 1;";//FrontBodyPositionKneedown
                cmd.CommandText = "SELECT * FROM patientdetails ORDER BY ROWID ASC LIMIT 1;";//
                SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
                DataTable dt = new DataTable();
                //dt.TableName = "SideBodyPosition";
                //dt.TableName = "FrontBodyPositionStanding";
                ad.Fill(dt); //fill the datasource
                //close connection
                AppDBCon.Close();


                //now iterate and add column names in list
                //lstCols.Add("Status");
                //lstCols.Add("TableName");
                //lstCols.Add("ComputerId");
                //lstCols.Add("ActivationKey");

                //foreach (DataColumn cl in dt.Columns)
                //{
                //    lstCols.Add(cl.ColumnName);
                //}

                DataTable tblPatientDetails= SpliterPatientDetails( bsonData);
                //DataTable SideBodyPosition = SpliterSideBodyPosition(lstCols);
                //DataTable tblFrontBodyPositionStanding = SpliterFrontBodyPositionStanding(lstCols);
                //DataTable tblSpliterFrontBodyPositionKneedown = SpliterFrontBodyPositionKneedown(lstCols);
                return tblPatientDetails;

            }
            catch (SQLiteException dbex)
            {

            }
            catch (Exception ex)
            {

            }
            return null;
        }
        public static DataTable ColumnNamesSideBodyPosition(string bsonData)
        {
            List<string> lstCols = new List<string>();
            //string sqliteDBPath = @"E:\GSPORT\SQLDBTest\cpdb.sqlite";
            SQLiteConnection AppDBCon = new SQLiteConnection("Data Source=" +GlobalItems.sqliteDBPath);
            try
            {
                SQLiteCommand cmd;
                AppDBCon.Open();
                cmd = AppDBCon.CreateCommand();
                cmd.CommandText = "SELECT * FROM SideBodyPosition ORDER BY ROWID ASC LIMIT 1;";//patientdetails
                
                SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
                DataTable dt = new DataTable();
                //dt.TableName = "SideBodyPosition";
                //dt.TableName = "SideBodyPosition";
                ad.Fill(dt); //fill the datasource
                //close connection
                AppDBCon.Close();


                //now iterate and add column names in list
                lstCols.Add("Status");
                lstCols.Add("TableName");
                lstCols.Add("ComputerId");
                lstCols.Add("ActivationKey");

                foreach (DataColumn cl in dt.Columns)
                {
                    lstCols.Add(cl.ColumnName);
                }

                //DataTable tblPatientDetails = SpliterPatientDetails(lstCols);
                DataTable SideBodyPosition = SpliterSideBodyPosition(lstCols,bsonData);
                //DataTable tblFrontBodyPositionStanding = SpliterFrontBodyPositionStanding(lstCols);
                //DataTable tblSpliterFrontBodyPositionKneedown = SpliterFrontBodyPositionKneedown(lstCols);
                return SideBodyPosition;

            }
            catch (SQLiteException dbex)
            {

            }
            catch (Exception ex)
            {

            }
            return null;
        }
        public static DataTable ColumnNamesFrontBodyPositionStanding(string bsonData)
        {
            List<string> lstCols = new List<string>();
            //string sqliteDBPath = @"E:\GSPORT\SQLDBTest\cpdb.sqlite";
            SQLiteConnection AppDBCon = new SQLiteConnection("Data Source=" + GlobalItems.sqliteDBPath);
            try
            {
                SQLiteCommand cmd;
                AppDBCon.Open();
                cmd = AppDBCon.CreateCommand();
                //cmd.CommandText = "SELECT * FROM SideBodyPosition ORDER BY ROWID ASC LIMIT 1;";//patientdetails
                //cmd.CommandText = "SELECT * FROM patientdetails ORDER BY ROWID ASC LIMIT 1;";//
                cmd.CommandText = "SELECT * FROM FrontBodyPositionStanding ORDER BY ROWID ASC LIMIT 1;";//FrontBodyPositionKneedown
                //cmd.CommandText = "SELECT * FROM patientdetails ORDER BY ROWID ASC LIMIT 1;";//
                SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
                DataTable dt = new DataTable();
                //dt.TableName = "SideBodyPosition";
                //dt.TableName = "FrontBodyPositionStanding";
                ad.Fill(dt); //fill the datasource
                //close connection
                AppDBCon.Close();


                //now iterate and add column names in list
                lstCols.Add("Status");
                lstCols.Add("TableName");
                lstCols.Add("ComputerId");
                lstCols.Add("ActivationKey");

                foreach (DataColumn cl in dt.Columns)
                {
                    lstCols.Add(cl.ColumnName);
                }

                //DataTable tblPatientDetails = SpliterPatientDetails(lstCols);
                //DataTable SideBodyPosition = SpliterSideBodyPosition(lstCols);
                DataTable tblFrontBodyPositionStanding = SpliterFrontBodyPositionStanding(lstCols,bsonData);
                //DataTable tblSpliterFrontBodyPositionKneedown = SpliterFrontBodyPositionKneedown(lstCols);
                return tblFrontBodyPositionStanding;

            }
            catch (SQLiteException dbex)
            {

            }
            catch (Exception ex)
            {

            }
            return null;
        }
        public static DataTable ColumnNamesFrontBodyPositionKneedown(string bsonData)
        {
            List<string> lstCols = new List<string>();
            //string sqliteDBPath = @"E:\GSPORT\SQLDBTest\cpdb.sqlite";
            SQLiteConnection AppDBCon = new SQLiteConnection("Data Source=" + GlobalItems.sqliteDBPath);
            try
            {
                SQLiteCommand cmd;
                AppDBCon.Open();
                cmd = AppDBCon.CreateCommand();
                //cmd.CommandText = "SELECT * FROM SideBodyPosition ORDER BY ROWID ASC LIMIT 1;";//patientdetails
                //cmd.CommandText = "SELECT * FROM patientdetails ORDER BY ROWID ASC LIMIT 1;";//
                cmd.CommandText = "SELECT * FROM FrontBodyPositionKneedown ORDER BY ROWID ASC LIMIT 1;";//FrontBodyPositionKneedown
                //cmd.CommandText = "SELECT * FROM patientdetails ORDER BY ROWID ASC LIMIT 1;";//
                SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
                DataTable dt = new DataTable();
                //dt.TableName = "SideBodyPosition";
                //dt.TableName = "FrontBodyPositionKneedown";
                ad.Fill(dt); //fill the datasource
                //close connection
                AppDBCon.Close();


                //now iterate and add column names in list
                lstCols.Add("Status");
                lstCols.Add("TableName");
                lstCols.Add("ComputerId");
                lstCols.Add("ActivationKey");

                foreach (DataColumn cl in dt.Columns)
                {
                    lstCols.Add(cl.ColumnName);
                }

               // DataTable tblPatientDetails = SpliterPatientDetails(lstCols);
                //DataTable SideBodyPosition = SpliterSideBodyPosition(lstCols);
                //DataTable dtFrontBodyPositionKneedown = SpliterFrontBodyPositionStanding(lstCols);
                DataTable tblSpliterFrontBodyPositionKneedown = SpliterFrontBodyPositionKneedown(lstCols,bsonData);
                return tblSpliterFrontBodyPositionKneedown;

            }
            catch (SQLiteException dbex)
            {

            }
            catch (Exception ex)
            {

            }
            return null;
        }
       

        public static DataTable SpliterSideBodyPosition(List<string> bsoncols, string bsonData)
        {
            //string filepath = @"E:\GSPORT\2.0 Version\JSON\Samples_Server_To_Client\BSON_STATIC\sidebodyposition.bson";
            //string filepath = @"E:\GSPORT\2.0 Version\JSON\Samples_Server_To_Client\BSON_STATIC\FrontBodyPositionStanding.bson";
            string content = bsonData;//File.ReadAllText(filepath);

            //remove additional contents
            string bson1 = content.Replace("\a�\0\0\u00030\0��\0\0\u0002", "");//\a\0\0\u00030\0\0\0\u0002
            bson1 = bson1.Replace("\a�\0\0\u00030\0��\0\0\u0002", "");
            bson1 = bson1.Replace("\0\u0004\0\0\0", "");
            bson1 = bson1.Replace("\0\0\u0002", "");
            bson1 = bson1.Replace("\0��", "");
            bson1 = bson1.Replace("\0\u0011\0\0\0", "");
            bson1 = bson1.Replace("\0\u0004\0\0\0", "");
            bson1 = bson1.Replace("\0\u0002", "");
            bson1 = bson1.Replace("\0\u001d\0\0\0 ", "");
            bson1 = bson1.Replace("\0\u001b\0\0\0", "");
            bson1 = bson1.Replace("\0\v�\0\0", "").Replace("\0�", "").Replace("�", "").Replace("\0#", "").Replace("\0?", "");
            bson1 = bson1.Replace("\0\v�", "");

           



            string strColVal = bson1.Replace("\0\u001d", "");
            List<string> ar = new List<string>();
       
            string[] checksp = strColVal.Split(bsoncols.ToArray(), StringSplitOptions.None);

            string[] checktp = strColVal.Split(checksp, StringSplitOptions.None);

            DataTable dtBSONValues = new DataTable();
            foreach(string cl in checktp)
            {
                if(cl.Trim().Length==0)
                {

                }
                else
                {
                    dtBSONValues.Columns.Add(cl);
                }
            }
            
            List<string> lstVals = new List<string>();
            foreach(string cel in checksp )
            {
                if(cel.Trim().Length==0)
                {

                }
                else
                {
                    lstVals.Add(cel);
                }
            }
            dtBSONValues.Rows.Add(lstVals.ToArray());
            return dtBSONValues;

        }
        public static DataTable SpliterFrontBodyPositionStanding(List<string> bsoncols, string bsonData)
        {
            //string filepath = @"E:\GSPORT\2.0 Version\JSON\Samples_Server_To_Client\BSON_STATIC\sidebodyposition.bson";
            //string filepath = @"E:\GSPORT\2.0 Version\JSON\Samples_Server_To_Client\BSON_STATIC\FrontBodyPositionStanding.bson";
            string content = bsonData;//File.ReadAllText(filepath);

            //remove additional contents
            string bson1 = content.Replace("\a�\0\0\u00030\0��\0\0\u0002", "");//\a\0\0\u00030\0\0\0\u0002
            bson1 = bson1.Replace("\a�\0\0\u00030\0��\0\0\u0002", "");
            bson1 = bson1.Replace("\0\u0004\0\0\0", "");
            bson1 = bson1.Replace("\0\0\u0002", "");
            bson1 = bson1.Replace("\0��", "");
            bson1 = bson1.Replace("\0\u0011\0\0\0", "");
            bson1 = bson1.Replace("\0\u0004\0\0\0", "");
            bson1 = bson1.Replace("\0\u0002", "");
            bson1 = bson1.Replace("\0\u001d\0\0\0 ", "");
            bson1 = bson1.Replace("\0\u001b\0\0\0", "");
            bson1 = bson1.Replace("\0\v�\0\0", "").Replace("\0�", "").Replace("�", "").Replace("\0#", "").Replace("\0?", "");
            bson1 = bson1.Replace("\0\v�", "");
            //================frontBodyPositionStanding=========
            bson1 = bson1.Replace("Y\0\0\u00030\0Q", "");
            bson1 = bson1.Replace("\0\u001a\0\0\0", "");
            bson1 = bson1.Replace("\0\u0005\0\0\0", "");
            bson1 = bson1.Replace("\0\u0005\0\0\0", "");
            //bson1 = bson1.Replace("", "");
            //bson1 = bson1.Replace("", "");
            //bson1 = bson1.Replace("", "");
            //==================================================


            string strColVal = bson1.Replace("\0\u001d", "");
            List<string> ar = new List<string>();

            string[] checksp = strColVal.Split(bsoncols.ToArray(), StringSplitOptions.None);

            string[] checktp = strColVal.Split(checksp, StringSplitOptions.None);

            DataTable dtBSONValues = new DataTable();
            foreach (string cl in checktp)
            {
                if (cl.Trim().Length == 0)
                {

                }
                else
                {
                    dtBSONValues.Columns.Add(cl);
                }
            }

            List<string> lstVals = new List<string>();
            foreach (string cel in checksp)
            {
                if (cel.Trim().Length == 0)
                {

                }
                else
                {
                    lstVals.Add(cel);
                }
            }
            dtBSONValues.Rows.Add(lstVals.ToArray());
            return dtBSONValues;

        }
        public static DataTable SpliterFrontBodyPositionStanding( string bsonData)
        {
            //=====================STANDING
            //-------------Added By Sumit on 28-Dec-18 for GSP-983 START-------------
            DataTable dtFrontStanding = SpliterFrontBodyPositionKneedown(bsonData);
            return dtFrontStanding;
            //-------------Added By Sumit on 28-Dec-18 for GSP-983 END---------------

            string content = bsonData;
            string[] uniqueId = { "UniqueId" };
            string[] discardBeforeUnique = content.Split(uniqueId, StringSplitOptions.None);

            string withUniqTillEnd = uniqueId[0] + " " + discardBeforeUnique[2];//[1];// Image is there


            string[] ImageBytes = { "ImageBytes" };

            string[] startToTillBeforeImage = withUniqTillEnd.Split(ImageBytes, StringSplitOptions.None);

            string firstHalf = startToTillBeforeImage[0];
            string secondHalf = "ImageBytes " + startToTillBeforeImage[1];

            //Now get AfterImageData
            string[] kpd = { "KneePositionDetected" };
            string[] withImageData = secondHalf.Split(kpd, StringSplitOptions.None);
            string afterImageTillEnd = "KneePositionDetected" + withImageData[1];

            string f1data = firstHalf;
            string imageData9 = "ImageBytes " + (content.Split(ImageBytes, StringSplitOptions.None)[1]).Split(kpd, StringSplitOptions.None)[0];
            string s2data = afterImageTillEnd;


            imageData9 = imageData9.Replace("\0\0\0\u0001\0", "");
            imageData9 = imageData9.Replace("\0\u0002", "");
            //sumit v2.0
            string[] starttag = { "thiscannotbeacoincedenceatstart" };
            string[] endtag = { "atendthisisalsonotacoincedence" };
            string imagedatahalfclean = imageData9.Split(starttag, StringSplitOptions.None)[1];
            string imagedataallcleaned = imagedatahalfclean.Split(endtag, StringSplitOptions.None)[0];
            imageData9 = imagedataallcleaned;
            //v2.0 end
            //Clean non image Data
            var outputsFirst = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8,
                  Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty),
                      new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(f1data)));
            outputsFirst = Regex.Replace(outputsFirst, "[^a-zA-Z0-9]+", " ");

            var outputSecond = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8,
              Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty),
                  new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(s2data)));
            outputSecond = Regex.Replace(outputSecond, "[^a-zA-Z0-9]+", " ");


            List<string> gather = new List<string>();
            foreach (string k1 in outputsFirst.Split(' '))
            {
                if (k1.Length > 0)
                    gather.Add(k1);
            }

            gather.Add("ImageBytes");//.Split(' ')[0]);
            gather.Add(imageData9);//.Split(' ')[1]);

            foreach (string k2 in outputSecond.Split(' '))
            {
                if (k2.Length > 0)
                    gather.Add(k2);
            }
            List<string> columnList = new List<string>();
            List<string> valuesList = new List<string>();

            for (int i = 0; i < gather.Count - 1; i += 2)
            {
                if (gather[i] == "Status" || gather[i] == "TableName" || gather[i] == "Computerid" || gather[i] == "Activationkey")
                {
                    continue;
                }
                columnList.Add(gather[i]);
                valuesList.Add(gather[i + 1]);
            }
            DataTable dtServerKneeDown = new DataTable();
            foreach (string cl in columnList)
            {
                dtServerKneeDown.Columns.Add(cl);
            }
            dtServerKneeDown.Rows.Add(valuesList.ToArray());
            string imagebytesone = dtServerKneeDown.Rows[0]["ImageBytes"].ToString();
            //non image data cleaned


            //============TESTING for imagedata
            //File.WriteAllText("ImgBytFrmProg", imagebytesone,Encoding.UTF8);

            //string t1 = File.ReadAllText("ImgBytFrmProg", Encoding.Convert(Encoding.UTF8,Encoding.ASCII);
            //===================================


            return dtServerKneeDown;

        }
        public static DataTable SpliterFrontBodyPositionKneedown(List<string> bsoncols, string bsonData)
        {
            //string filepath = @"E:\GSPORT\2.0 Version\JSON\Samples_Server_To_Client\BSON_STATIC\sidebodyposition.bson";
            //string filepath = @"E:\GSPORT\2.0 Version\JSON\Samples_Server_To_Client\BSON_STATIC\FrontBodyPositionKneedown.bson";
            string content = bsonData;//File.ReadAllText(filepath);
            string[] uniqueId = { "UniqueId" };
            string[] discardBeforeUnique = content.Split(uniqueId, StringSplitOptions.None);

            string withUniqTillEnd = uniqueId[0] + " " + discardBeforeUnique[1];// Image is there


            string[] ImageBytes = { "ImageBytes" };

            string[] startToTillBeforeImage = withUniqTillEnd.Split(ImageBytes, StringSplitOptions.None);

            string firstHalf = startToTillBeforeImage[0];
            string secondHalf = "ImageBytes " + startToTillBeforeImage[1];

            //Now get AfterImageData
            string[] kpd = { "KneePositionDetected" };
            string[] withImageData = secondHalf.Split(kpd, StringSplitOptions.None);
            string afterImageTillEnd = "KneePositionDetected" + withImageData[1];

            string f1data = firstHalf;
            string imageData9 ="ImageBytes "+ (content.Split(ImageBytes, StringSplitOptions.None)[1]).Split(kpd, StringSplitOptions.None)[0];
            string s2data = afterImageTillEnd;


            //Clean non image Data
            var outputsFirst = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8, 
                  Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty), 
                      new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(f1data)));          
                           outputsFirst =  Regex.Replace(outputsFirst, "[^a-zA-Z0-9]+", " ");

            var outputSecond = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8,
              Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty),
                  new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(s2data)));
            outputSecond = Regex.Replace(outputSecond, "[^a-zA-Z0-9]+", " ");


            List<string> gather = new List<string>();
            foreach(string k1 in outputsFirst.Split(' '))
            {
                if (k1.Length > 0)
                    gather.Add(k1);
            }

            gather.Add(imageData9.Split(' ')[0]);
            gather.Add(imageData9.Split(' ')[1]);

            foreach (string k2 in outputSecond.Split(' '))
            {
                if (k2.Length > 0)
                    gather.Add(k2);
            }
            List<string> columnList = new List<string>();
            List<string> valuesList = new List<string>();

            for(int i=0;i<gather.Count-1;i+=2)
            {
                columnList.Add(gather[i]);
                valuesList.Add(gather[i + 1]);
            }
            DataTable dtServerKneeDown = new DataTable();
            foreach(string cl in columnList)
            {
                dtServerKneeDown.Columns.Add(cl);
            }
            dtServerKneeDown.Rows.Add(valuesList.ToArray());
            string imagebytesone = dtServerKneeDown.Rows[0]["ImageBytes"].ToString();
            //non image data cleaned

            return dtServerKneeDown;

        }

        public static DataTable SpliterFrontBodyPositionKneedown(string bsonData, string tableName = "")
        {
            //-------------Added By Sumit on 29-Dec-18 for GSP-983 START-------------
            List<string> lstNodeNamesInBson = new List<string>();
            //Create Node name collections later will mapp with local DB
            
            if (tableName.ToUpper() != "SideBodyPosition".ToUpper())
            {
                lstNodeNamesInBson.Add("Status");
                lstNodeNamesInBson.Add("TableName");
                lstNodeNamesInBson.Add("Computerid");
                lstNodeNamesInBson.Add("Activationkey");
                lstNodeNamesInBson.Add("UniqueId");
                lstNodeNamesInBson.Add("user_id");
                lstNodeNamesInBson.Add("reg_user_id");
                lstNodeNamesInBson.Add("ChinX");
                lstNodeNamesInBson.Add("ChinY");
                lstNodeNamesInBson.Add("GlabellaX");
                lstNodeNamesInBson.Add("GlabellaY");
                lstNodeNamesInBson.Add("ImageBytes");
                lstNodeNamesInBson.Add("KneePositionDetected");
                lstNodeNamesInBson.Add("LeftAnkleX");
                lstNodeNamesInBson.Add("LeftAnkleY");
                lstNodeNamesInBson.Add("LeftBeltX");
                lstNodeNamesInBson.Add("LeftBeltY");
                lstNodeNamesInBson.Add("LeftEarX");
                lstNodeNamesInBson.Add("LeftEarY");
                lstNodeNamesInBson.Add("LeftHipX");
                lstNodeNamesInBson.Add("LeftHipY");
                lstNodeNamesInBson.Add("LeftKneeX");
                lstNodeNamesInBson.Add("LeftKneeY");
                lstNodeNamesInBson.Add("LeftShoulderX");
                lstNodeNamesInBson.Add("LeftShoulderY");
                lstNodeNamesInBson.Add("RightAnkleX");
                lstNodeNamesInBson.Add("RightAnkleY");
                lstNodeNamesInBson.Add("RightBeltX");
                lstNodeNamesInBson.Add("RightBeltY");
                lstNodeNamesInBson.Add("RightEarX");
                lstNodeNamesInBson.Add("RightEarY");
                lstNodeNamesInBson.Add("RightHipX");
                lstNodeNamesInBson.Add("RightHipY");
                lstNodeNamesInBson.Add("RightKneeX");
                lstNodeNamesInBson.Add("RightKneeY");
                lstNodeNamesInBson.Add("RightShoulderX");
                lstNodeNamesInBson.Add("RightShoulderY");
                lstNodeNamesInBson.Add("UnderBodyPositionDetected");
                lstNodeNamesInBson.Add("UpperBodyPositionDetected");
            }
            else if(tableName.ToUpper() == "SideBodyPosition".ToUpper())
            {
                lstNodeNamesInBson.Add("Status");
                lstNodeNamesInBson.Add("TableName");
                lstNodeNamesInBson.Add("Computerid");
                lstNodeNamesInBson.Add("Activationkey");
                lstNodeNamesInBson.Add("UniqueId");
                lstNodeNamesInBson.Add("user_id");
                lstNodeNamesInBson.Add("reg_user_id");
                lstNodeNamesInBson.Add("AnkleLeftBeltX");
                lstNodeNamesInBson.Add("AnkleLeftBeltY");
                lstNodeNamesInBson.Add("AnkleRightBeltX");
                lstNodeNamesInBson.Add("AnkleRightBeltY");
                lstNodeNamesInBson.Add("AnkleX");
                lstNodeNamesInBson.Add("AnkleY");
                lstNodeNamesInBson.Add("BenchMark1X");
                lstNodeNamesInBson.Add("BenchMark1Y");
                lstNodeNamesInBson.Add("BenchMark2X");
                lstNodeNamesInBson.Add("BenchMark2Y");
                lstNodeNamesInBson.Add("ChinX");
                lstNodeNamesInBson.Add("ChinY");
                lstNodeNamesInBson.Add("EarX");
                lstNodeNamesInBson.Add("EarY");
                lstNodeNamesInBson.Add("GlabellaX");
                lstNodeNamesInBson.Add("GlabellaY");
                lstNodeNamesInBson.Add("HipX");
                lstNodeNamesInBson.Add("HipY");
                lstNodeNamesInBson.Add("ImageBytes");
                lstNodeNamesInBson.Add("KneeLeftBeltX");
                lstNodeNamesInBson.Add("KneeLeftBeltY");
                lstNodeNamesInBson.Add("KneeRightBeltX");
                lstNodeNamesInBson.Add("KneeRightBeltY");
                lstNodeNamesInBson.Add("KneeX");
                lstNodeNamesInBson.Add("KneeY");
                lstNodeNamesInBson.Add("LeftBeltX");
                lstNodeNamesInBson.Add("LeftBeltY");
                lstNodeNamesInBson.Add("RightBeltX");
                lstNodeNamesInBson.Add("RightBeltY");
                lstNodeNamesInBson.Add("ShoulderX");
                lstNodeNamesInBson.Add("ShoulderY");
            }

            string[] arLstNodeValues = bsonData.Split(lstNodeNamesInBson.ToArray(), StringSplitOptions.None);
            List<string> lstValuesOfNode = new List<string>(); 
            lstValuesOfNode.AddRange(arLstNodeValues);
            lstValuesOfNode.RemoveAt(0);

            //for FrontBodyPositionStanding multiple records are coming from server, 
            //later we can remove this section if record count is fixed to one at a time from Adarsh side
            for (; true;)
            {
               if(lstValuesOfNode.Count>39)
                {
                    int mxIndex = lstValuesOfNode.Count - 1;
                    lstValuesOfNode.RemoveAt(mxIndex);
                }
                else
                {
                    if (lstValuesOfNode[lstValuesOfNode.Count-1].Contains("\0\0\u001c�\0\0\u0002"))
                    {
                        lstValuesOfNode[lstValuesOfNode.Count - 1] = lstValuesOfNode[lstValuesOfNode.Count - 1].Replace("\0\0\u001c�\0\0\u0002", "");
                    }
                        break;//\0\0\u001c�\0\0\u0002
                    
                }
            }
            //

            DataTable dtToInsert = new DataTable();
            List<string> lstClForDT = new List<string>();
            List<string> lstRwsForDt = new List<string>();
            if(lstNodeNamesInBson.Count==lstValuesOfNode.Count)
            {
                for(int q=0;q<lstNodeNamesInBson.Count;q++)
                {
                    if(lstNodeNamesInBson[q]== "Status" ||
                       lstNodeNamesInBson[q] == "TableName" ||
                       lstNodeNamesInBson[q] == "Computerid" ||
                       lstNodeNamesInBson[q] == "Activationkey" ||
                       lstNodeNamesInBson[q] == "user_id" ||
                       lstNodeNamesInBson[q] == "reg_user_id")
                    {
                        continue;
                    }
                    lstClForDT.Add(lstNodeNamesInBson[q]);
                    dtToInsert.Columns.Add(lstNodeNamesInBson[q]);
                    if(lstNodeNamesInBson[q] != "ImageBytes")
                    {
                        string dirtyValue = lstValuesOfNode[q];
                        string cleanValue = "";
                        foreach(DataRow dr in InstallInfoManager.GetRemoverStringsTable().Rows)
                        {
                            dirtyValue = dirtyValue.Replace(dr[0].ToString(), "");
                        }
                        cleanValue = dirtyValue;
                        lstRwsForDt.Add(cleanValue);
                    }
                    else if (lstNodeNamesInBson[q] == "ImageBytes")
                    {
                        string[] ar1 = { "thiscannotbeacoincedenceatstart", "atendthisisalsonotacoincedence" };
                        lstRwsForDt.Add(lstValuesOfNode[q].Split(ar1, StringSplitOptions.None)[1]);
                    }
                }
            }
            if(lstClForDT.Count==lstRwsForDt.Count)
            {
                dtToInsert.Rows.Add(lstRwsForDt.ToArray());
            }
            return dtToInsert;
            //-------------Added By Sumit on 29-Dec-18 for GSP-983 END---------------
            //string filepath = @"E:\GSPORT\2.0 Version\JSON\Samples_Server_To_Client\BSON_STATIC\sidebodyposition.bson";
            //string filepath = @"E:\GSPORT\2.0 Version\JSON\Samples_Server_To_Client\BSON_STATIC\FrontBodyPositionKneedown.bson";
            string content = bsonData;//File.ReadAllText(filepath);
            string[] uniqueId = { "UniqueId" };
            string[] discardBeforeUnique = content.Split(uniqueId, StringSplitOptions.None);

            string withUniqTillEnd = uniqueId[0] + " " + discardBeforeUnique[1];// Image is there


            string[] ImageBytes = { "ImageBytes" };

            string[] startToTillBeforeImage = withUniqTillEnd.Split(ImageBytes, StringSplitOptions.None);

            string firstHalf = startToTillBeforeImage[0];
            string secondHalf = "ImageBytes " + startToTillBeforeImage[1];

            //Now get AfterImageData
            string[] kpd = { "KneePositionDetected" };
            string[] withImageData = secondHalf.Split(kpd, StringSplitOptions.None);
            string afterImageTillEnd = "KneePositionDetected" + withImageData[1];

            string f1data = firstHalf;
            string imageData9 = "ImageBytes " + (content.Split(ImageBytes, StringSplitOptions.None)[1]).Split(kpd, StringSplitOptions.None)[0];
            string s2data = afterImageTillEnd;


            imageData9 = imageData9.Replace("\0\0\0\u0001\0", "");
            imageData9 = imageData9.Replace("\0\u0002", "");
            //Sumit v2.0 Image data cleaning 
            string[] starttag = { "thiscannotbeacoincedenceatstart" };
            string[] endtag = { "atendthisisalsonotacoincedence" };
            string imagedatahalfclean = imageData9.Split(starttag,StringSplitOptions.None)[1];
            string imagedataallcleaned = imagedatahalfclean.Split(endtag,StringSplitOptions.None)[0];
            imageData9 = imagedataallcleaned;
            //

            //Clean non image Data
            var outputsFirst = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8,
                  Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty),
                      new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(f1data)));
            outputsFirst = Regex.Replace(outputsFirst, "[^a-zA-Z0-9]+", " ");

            var outputSecond = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8,
              Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty),
                  new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(s2data)));
            outputSecond = Regex.Replace(outputSecond, "[^a-zA-Z0-9]+", " ");


            List<string> gather = new List<string>();
            foreach (string k1 in outputsFirst.Split(' '))
            {
                if (k1.Length > 0)
                    gather.Add(k1);
            }

            gather.Add("ImageBytes");//imageData9.Split(' ')[0]);
            gather.Add(imageData9);//.Split(' ')[1]);

            foreach (string k2 in outputSecond.Split(' '))
            {
                if (k2.Length > 0)
                    gather.Add(k2);
            }
            List<string> columnList = new List<string>();
            List<string> valuesList = new List<string>();

            for (int i = 0; i < gather.Count - 1; i += 2)
            {
                if (gather[i] == "Status" || gather[i] == "TableName" || gather[i] == "Computerid" || gather[i] == "Activationkey")
                {
                    continue;
                }
                columnList.Add(gather[i]);
                valuesList.Add(gather[i + 1]);
            }
            DataTable dtServerKneeDown = new DataTable();
            foreach (string cl in columnList)
            {
                dtServerKneeDown.Columns.Add(cl);
            }
            dtServerKneeDown.Rows.Add(valuesList.ToArray());
            string imagebytesone = dtServerKneeDown.Rows[0]["ImageBytes"].ToString();
            //non image data cleaned

            return dtServerKneeDown;

        }
        //--Added by Rajnish for GSP-676--//
        public static Boolean IsBase64String(this String value)
        {
            if (value == null || value.Length == 0 || value.Length % 4 != 0
                || value.Contains(' ') || value.Contains('\t') || value.Contains('\r') || value.Contains('\n'))
                return false;
            var index = value.Length - 1;
            if (value[index] == '=')
                index--;
            if (value[index] == '=')
                index--;
            for (var i = 0; i <= index; i++)
                if (IsInvalid(value[i]))
                    return false;
            return true;
        }
        private static Boolean IsInvalid(char value)
        {
            var intValue = (Int32)value;
            if (intValue >= 48 && intValue <= 57)
                return false;
            if (intValue >= 65 && intValue <= 90)
                return false;
            if (intValue >= 97 && intValue <= 122)
                return false;
            return intValue != 43 && intValue != 47;
        }
        //-------------------------------//
        public static DataTable SpliterPatientDetails(string bsondata)
        {

            #region Commented: (Needed to replace with fail safe logic)
            /*
            if(bsondata.Length==0)
            {
                throw new EvaluateException("bsondata cannot be empty");
            }

            //======================================
            var outputs = Encoding.ASCII.GetString(Encoding.Convert
                (Encoding.UTF8, Encoding.GetEncoding(Encoding.ASCII.EncodingName, 
            new EncoderReplacementFallback(String.Empty), new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(bsondata)));


            outputs = Regex.Replace(outputs, "[^a-zA-Z0-9]+", " ");
            //======================================

            //string filepath = @"E:\GSPORT\2.0 Version\JSON\Samples_Server_To_Client\BSON_STATIC\sidebodyposition.bson";
            //string filepath = @"E:\GSPORT\2.0 Version\JSON\Samples_Server_To_Client\BSON_STATIC\PatientDetails.bson";
            string content = bsondata;//File.ReadAllText(filepath);

            //remove additional contents
            string bson1 = content.Replace("r\u0001\0\0\u0002", "").Replace("\a�\0\0\u00030\0��\0\0\u0002", "");//\a\0\0\u00030\0\0\0\u0002
            bson1 = bson1.Replace("\a�\0\0\u00030\0��\0\0\u0002", "");
            bson1 = bson1.Replace("\0\u0004\0\0\0", "");
            bson1 = bson1.Replace("\0\0\u0002", "");
            bson1 = bson1.Replace("\0��", "");
            bson1 = bson1.Replace("\0\u0011\0\0\0", "");
            bson1 = bson1.Replace("\0\u0004\0\0\0", "");
            bson1 = bson1.Replace("\0\u0002", "");
            bson1 = bson1.Replace("\0\u001d\0\0\0 ", "");
            bson1 = bson1.Replace("\0\u001b\0\0\0", "");
            bson1 = bson1.Replace("\0\v�\0\0", "").Replace("\0�", "").Replace("�", "").Replace("\0#", "").Replace("\0?", "");
            bson1 = bson1.Replace("\0\v�", "");
            //================frontBodyPositionStanding=========
            bson1 = bson1.Replace("Y\0\0\u00030\0Q", "");
            bson1 = bson1.Replace("\0\u001a\0\0\0", "");
            bson1 = bson1.Replace("\0\u0005\0\0\0", "");
            bson1 = bson1.Replace("\0\u0005\0\0\0", "");
            //=================PatientDetails==============
            bson1 = bson1.Replace("\u0001\0\0\u00030\u0001", "");
            bson1 = bson1.Replace("\0\u000f\0\0\0", "");
            bson1 = bson1.Replace("\0\u0003\0\0\0", "");
            bson1 = bson1.Replace("\0\u0001\0\0", "");
            bson1 = bson1.Replace("\0\u0016\0\0\0", "");            
            bson1 = bson1.Replace("\0\a\0\0\0", "");            
            bson1 = bson1.Replace("\0\u0003\0\0\0", "");
            bson1 = bson1.Replace("\0\0\0", "");
            //bson1 = bson1.Replace("", "");
            //bson1 = bson1.Replace("", "");
            //bson1 = bson1.Replace("", "");
            //==================================================


            string strColVal = bson1.Replace("\0\u001d", "");
            strColVal = strColVal.Replace("\u0001", "");
            strColVal = strColVal.Replace("\0\u0013", "");
            strColVal = strColVal.Replace("\0\n", "");
            //strColVal = strColVal.Replace("\0\r", "");
            strColVal = strColVal.Replace("\0\0", "");
            strColVal = strColVal.Replace("Comment", "Comment$");
            strColVal = strColVal.Replace(" ", "$");
            List<string> ar = new List<string>();
            strColVal = strColVal.Replace("Computerid", "ComputerId");
            strColVal = strColVal.Replace("Activationkey", "ActivationKey");

            //================================================================================================
            //strColVal = strColVal.Replace("-", "#");
            //var outputs = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8, Encoding.GetEncoding(Encoding.ASCII.EncodingName, 
            //    new EncoderReplacementFallback(String.Empty), new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(strColVal)));


            //outputs = Regex.Replace(outputs, "[^a-zA-Z0-9]+", " ");
            //strColVal = outputs;

            strColVal = strColVal.Replace("#", "-");
            //================================================================================================
            string[] checkspValues = strColVal.Split(bsoncols.ToArray(), StringSplitOptions.None);

            string[] checktpColNames = strColVal.Split(checkspValues, StringSplitOptions.None);

            List<string> correctColumns = new List<string>();
            foreach (string columnsMayBWrong in checktpColNames)
            {
                if (columnsMayBWrong.Contains("Month"))
                {
                    correctColumns.Add("Month");
                    continue;
                }
                //if (columnsMayBWrong.Contains("-") || columnsMayBWrong == "0" || columnsMayBWrong == "" || columnsMayBWrong.Length == 0
                //    || columnsMayBWrong == "1" || columnsMayBWrong == "2" || columnsMayBWrong == "3" || columnsMayBWrong == "4"
                //    || columnsMayBWrong == "5" || columnsMayBWrong == "6" || columnsMayBWrong == "7" || columnsMayBWrong == "8"
                //    || columnsMayBWrong == "9")
                if (columnsMayBWrong.Contains("-") || columnsMayBWrong.Contains("0") || columnsMayBWrong.Contains("") || columnsMayBWrong.Length == 0
                    || columnsMayBWrong.Contains("1") || columnsMayBWrong.Contains("2") || columnsMayBWrong.Contains("3") || columnsMayBWrong.Contains("4")
                    || columnsMayBWrong.Contains("5") || columnsMayBWrong.Contains("6") || columnsMayBWrong.Contains("7") || columnsMayBWrong.Contains("8")
                    || columnsMayBWrong.Contains("9"))

                {
                    continue;
                }
                if (columnsMayBWrong.Contains("00"))
                {
                    correctColumns.Add(columnsMayBWrong.Replace("00", "").Replace("$", " "));
                    continue;
                }
                else
                {
                }
                correctColumns.Add(columnsMayBWrong.Replace("$", " "));
            }
            List<string> correctValues = new List<string>();
            int cntColVal = 1;
            foreach (string valuesMayBWrong in checkspValues)
            {
                if(cntColVal==1)
                {
                    cntColVal++;
                    continue;
                }
                correctValues.Add(valuesMayBWrong.Replace("$", " "));
                
            }



            DataTable dtBSONValues = new DataTable();
            int k = 0;
            foreach (string cl in correctColumns)
            {
                if (cl.Trim().Length == 0 )
                {
                    continue;
                }
                else if(k<=3)
                {
                    //continue;
                }
                else
                {
                    dtBSONValues.Columns.Add(cl);
                }
                k++;
            }

            List<string> lstVals = new List<string>();

            

            //List<string> lstVals = new List<string>();
            //foreach (string cel in correctValues)
            //{
            //    //if (cel.Trim().Length == 0)
            //    //{

            //    //}
            //    //else
            //    //{
            //    //    lstVals.Add(cel);
            //    //}
            //    lstVals.Add(cel.Replace(" ", "-"));
            //}
            lstVals = correctValues;
            lstVals.RemoveAt(0);
            lstVals.RemoveAt(0);
            lstVals.RemoveAt(0);
            lstVals.RemoveAt(0);
            dtBSONValues.Rows.Add(lstVals.ToArray());

            #region commented
            //dtBSONValues.Rows[0]["MeasurementTime"] = dtBSONValues.Rows[0]["MeasurementTime"].ToString().Replace(" ", "-");
            //dtBSONValues.Rows[0]["DOB"] = dtBSONValues.Rows[0]["DOB"].ToString().Replace(" ", "-");
            //edit table values for 'time' data

            //foreach(DataColumn dc in dtBSONValues.Columns)
            //{
            //    if(dc.ColumnName.Trim() == "MeasurementTime" || dc.ColumnName.Trim()=="DOB")
            //    {
            //        continue;
            //    }
            //    else
            //    {
            //        var outputs = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8, 
            //        Encoding.GetEncoding(Encoding.ASCII.EncodingName,
            //        new EncoderReplacementFallback(String.Empty), new DecoderExceptionFallback()), 
            //        Encoding.UTF8.GetBytes(dtBSONValues.Rows[0][dc].ToString())));


            //        dtBSONValues.Rows[0][dc]=Regex.Replace(outputs, "[^a-zA-Z0-9]+", " ");
            //    }
            //}
            #endregion

    */
            #endregion

            DataTable dtRet = new DataTable();
            string content = bsondata;//File.ReadAllText(filepath);
            //===========================
            //string[] columnsName = { "UniqueId", "BenchmarkDistance","Comment","DOB","Date","Gender", "Height", "MeasurementTime","Month",
            //                        "Name", "PatientId","Year","lut"};

            string[] columnsName = { "UniqueId","user_id","reg_user_id", "update_counter", "BenchmarkDistance","Comment","DOB","Date","Gender", "Height", "MeasurementTime","Month",
                                    "Name", "PatientId","Year"};


            string[] valuesOnly = content.Split(columnsName, StringSplitOptions.None);
            //===========================
            int k = 2;
            DataTable dtpd = new DataTable();
            foreach(string gp in columnsName)
            {
                dtpd.Columns.Add(gp); 
            }
            List<string> kspl = new List<string>();
            kspl.AddRange(valuesOnly);
            kspl.RemoveAt(0);
            kspl.RemoveAt(0);

            for(int rmv = 0; rmv < kspl.Count; rmv++)
            {
                //kspl[rmv] = System.Text.RegularExpressions.Regex.Replace(kspl[rmv], @"[^\u0020-\u007E]", string.Empty);
                //continue;
                //kspl[rmv]
                foreach (DataRow drmv in  InstallInfoManager.GetRemoverStringsTable().Rows)
                {
                    kspl[rmv] = kspl[rmv].Replace(drmv[0].ToString(), "");
                }
            }
            try
            {
                dtpd.Rows.Add(kspl.ToArray());
            }
            catch(Exception xx1)
            {
                throw new Exception("May be a Column Name conflict in data: " + xx1.Message);
            }

            //Added by Sumit GSP-1016---------START
            //PatientId Column
            string[] spliterPIDStart = { "xSTARTxpxaxtxixexnxtxixdx" };
            string[] spliterPIDEnd = { "xENDxpxaxtxixexnxtxixdx" };
            try
            {
                if (dtpd.Rows[0]["PatientId"].ToString().ToUpper().Contains("xSTARTxpxaxtxixexnxtxixdx".ToUpper())
                            && dtpd.Rows[0]["PatientId"].ToString().ToUpper().Contains("xENDxpxaxtxixexnxtxixdx".ToUpper()))
                {
                    dtpd.Rows[0]["PatientId"] = dtpd.Rows[0]["PatientId"].ToString().Split(spliterPIDStart, StringSplitOptions.None)[1].Split(spliterPIDEnd, StringSplitOptions.None)[0];
                }
            }
            catch (Exception epid)
            {
                dtpd.Rows[0]["PatientId"] = dtpd.Rows[0]["PatientId"].ToString().Replace(spliterPIDStart[0], "").Replace(spliterPIDEnd[0], "");
            }

            //Name Column
            string[] spliterNameStart = { "xSTARTxnxaxmxex" };
            string[] spliterNameEnd = { "xENDxnxaxmxex" };
            try
            {
                if (dtpd.Rows[0]["Name"].ToString().ToUpper().Contains("xSTARTxnxaxmxex".ToUpper())
                            && dtpd.Rows[0]["Name"].ToString().ToUpper().Contains("xENDxnxaxmxex".ToUpper()))
                {
                    dtpd.Rows[0]["Name"] = dtpd.Rows[0]["Name"].ToString().Split(spliterNameStart, StringSplitOptions.None)[1].Split(spliterNameEnd, StringSplitOptions.None)[0];
                }
            }
            catch (Exception exname)
            {
                dtpd.Rows[0]["Name"] = dtpd.Rows[0]["Name"].ToString().Replace(spliterNameStart[0], "").Replace(spliterNameEnd[0], "");
            }

            //Comment Cloumn
            string[] spliterStart = { "xSTARTxcxoxmxmxexnxtx" };
            string[] spliterEnd = { "xENDxcxoxmxmxexnxtx" };
            try
            {
                if (dtpd.Rows[0]["comment"].ToString().ToUpper().Contains("xSTARTxcxoxmxmxexnxtx".ToUpper())
                            && dtpd.Rows[0]["comment"].ToString().ToUpper().Contains("xENDxcxoxmxmxexnxtx".ToUpper()))
                {
                    dtpd.Rows[0]["comment"] = dtpd.Rows[0]["comment"].ToString().Split(spliterStart, StringSplitOptions.None)[1].Split(spliterEnd, StringSplitOptions.None)[0];
                }
            }
            catch(Exception exspl)
            {
                dtpd.Rows[0]["comment"] = dtpd.Rows[0]["comment"].ToString().Replace(spliterStart[0], "").Replace(spliterEnd[0], "");
            }

            //Added by Sumit GSP-1016---------END

            //-------------Added By Sumit on 28-Dec-18 for GSP-983 START-------------
            return dtpd;
            //-------------Added By Sumit on 28-Dec-18 for GSP-983 END---------------

            for (int h=0;h<dtpd.Columns.Count;h++)
            {
                string clname = dtpd.Columns[h].ColumnName;
                //Condition modified by Rajnish for GSP-676 (&& clname != "Comment" && clname != "Name" && clname != "PatientId")
                if (clname != "DOB" && clname != "MeasurementTime" && clname != "Comment" && clname != "Name" && clname != "PatientId")
                {
                    var tmp = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8,
                         Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty),
                             new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(dtpd.Rows[0][clname].ToString())));
                    dtpd.Rows[0][clname] = Regex.Replace(tmp.Trim(), "[^a-zA-Z0-9]+", " ");
                }
                else if (clname == "Comment" || clname == "Name" || clname == "PatientId")
                {
                    //--else if case Added by Rajnish for GSP-676--//
                    var tmp1 = string.Empty;
                    tmp1 = dtpd.Rows[0][clname].ToString();
                    tmp1 = Regex.Replace(tmp1.Trim(), "[^a-zA-Z0-9+*/=]+", " ");

                    try
                    {
                        string[] tmp2 = tmp1.Split(' ');
                        foreach (string base64data in tmp2)
                        {
                            if (base64data.Length > 0)
                            {
                                bool IsValidBase64 = IsBase64String(base64data);
                                if (IsValidBase64)
                                {
                                    byte[] data = Convert.FromBase64String(base64data);
                                    tmp1 = System.Text.Encoding.UTF8.GetString(data);
                                    dtpd.Rows[0][clname] = tmp1;
                                    dtpd.Rows[0][clname] = tmp1.Replace("'","''");//Added by Rajnish For GSP-901
                                    break;
                                }
                            }
                            else
                            {
                                dtpd.Rows[0][clname] = "";
                            }
                        }
                    }
                    catch (Exception ex) { ex.ToString(); }

                    //-----------------------------------------------------//
                }
                else
                {
                    string valHold = dtpd.Rows[0][clname].ToString().Replace('-', 'x').Replace(':', 'y');
                    var clearval = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8,
                         Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty),
                             new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(valHold)));
                    valHold = Regex.Replace(clearval.Trim(), "[^a-zA-Z0-9]+", " ");
                    dtpd.Rows[0][clname] = valHold.Replace('x', '-').Replace('y', ':');
                }
            }

            #region obsoleteLogic
            //string[] uniqueId = { "UniqueId" };
            //string[] discardBeforeUnique = content.Split(uniqueId, StringSplitOptions.None);

            //string withUniqTillEnd = uniqueId[0] + " " + discardBeforeUnique[1];// Image is there

            ////=====================================DOB Extraction===========================
            //string[] dob = { "DOB" };
            //string[] mtime = { "MeasurementTime" };

            //string[] startToTillBeforedob = withUniqTillEnd.Split(dob, StringSplitOptions.None);

            //string firstHalf = startToTillBeforedob[0];
            //string secondHalf = "DOB " + startToTillBeforedob[1];

            ////Now get AfterImageData
            //string[] kpd = { "Date" };
            //string[] withdob = secondHalf.Split(kpd, StringSplitOptions.None);
            //string afterdobTillEnd = "Date" + withdob[1];

            //string f1data = firstHalf;
            //string dobData9 = "DOB " + (content.Split(dob, StringSplitOptions.None)[1]).Split(kpd, StringSplitOptions.None)[0];
            //string s2data = afterdobTillEnd;
            ////===============================================================


            ////=============================MeasurementTime extraction===================
            //string[] mt = { "MeasurementTime" };
            //string[] b4mt = { "Height","Month" };
            //string[] aftmt = { "Month" };
            //string[] withmt = s2data.Split(b4mt, StringSplitOptions.None);

            //string mtNameValue = withmt[1];

            //mtNameValue = mtNameValue.Replace('-', 'z').Replace(':', 'y');
            //var mtclear = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8,
            //     Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty),
            //         new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(mtNameValue)));
            //mtclear = Regex.Replace(mtclear, "[^a-zA-Z0-9]+", " ");



            ////==========================================================================





            ////Clean non image Data
            //var outputsFirst = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8,
            //      Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty),
            //          new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(f1data)));
            //outputsFirst = Regex.Replace(outputsFirst, "[^a-zA-Z0-9]+", " ");

            //var outputSecond = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8,
            //  Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty),
            //      new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(s2data)));
            //outputSecond = Regex.Replace(outputSecond, "[^a-zA-Z0-9]+", " ");


            //List<string> gather = new List<string>();
            //foreach (string k1 in outputsFirst.Split(' '))
            //{
            //    if (k1.Length > 0)
            //        gather.Add(k1);
            //}

            //gather.Add(dobData9.Split(' ')[0]);
            //gather.Add(dobData9.Split(' ')[1]);

            //foreach (string k2 in outputSecond.Split(' '))
            //{
            //    if (k2.Length > 0)
            //        gather.Add(k2);
            //}
            //List<string> columnList = new List<string>();
            //List<string> valuesList = new List<string>();

            //for (int i = 0; i < gather.Count - 1; i += 2)
            //{
            //    columnList.Add(gather[i]);
            //    valuesList.Add(gather[i + 1]);
            //}
            //DataTable dtServerKneeDown = new DataTable();
            //foreach (string cl in columnList)
            //{
            //    dtServerKneeDown.Columns.Add(cl);
            //}
            //dtServerKneeDown.Rows.Add(valuesList.ToArray());
            //string imagebytesone = dtServerKneeDown.Rows[0]["DOB"].ToString();
            ////non image data cleaned

            //return dtServerKneeDown;

            #endregion



            return dtpd;

        }

        public static DataTable SpliterSideBodyPosition(string bsonData)
        {
            //=====================STANDING
            string content = bsonData;
            DataTable dtSideBody = SpliterFrontBodyPositionKneedown(bsonData, "SideBodyPosition");
            return dtSideBody;



            string[] uniqueId = { "UniqueId" };
            string[] discardBeforeUnique = content.Split(uniqueId, StringSplitOptions.None);

            string withUniqTillEnd = uniqueId[0] + " " + discardBeforeUnique[1];// Image is there


            string[] ImageBytes = { "ImageBytes" };

            string[] startToTillBeforeImage = withUniqTillEnd.Split(ImageBytes, StringSplitOptions.None);

            string firstHalf = startToTillBeforeImage[0];
            string secondHalf = "ImageBytes " + startToTillBeforeImage[1];

            //Now get AfterImageData
            string[] kpd = { "KneeLeftBeltX" };
            string[] withImageData = secondHalf.Split(kpd, StringSplitOptions.None);
            string afterImageTillEnd = "KneeLeftBeltX" + withImageData[1];

            string f1data = firstHalf;
            string imageData9 = "ImageBytes " + (content.Split(ImageBytes, StringSplitOptions.None)[1]).Split(kpd, StringSplitOptions.None)[0];
            string s2data = afterImageTillEnd;

            imageData9 = imageData9.Replace("\0\0\0\u0001\0", "");
            imageData9 = imageData9.Replace("\0\u0002", "");

            //Sumit v2.0
            string[] starttag = { "thiscannotbeacoincedenceatstart" };
            string[] endtag = { "atendthisisalsonotacoincedence" };
            string imagedatahalfclean = imageData9.Split(starttag, StringSplitOptions.None)[1];
            string imagedataallcleaned = imagedatahalfclean.Split(endtag, StringSplitOptions.None)[0];
            imageData9 = imagedataallcleaned;
            //end v2.0

            //Clean non image Data
            var outputsFirst = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8,
                  Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty),
                      new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(f1data)));
            outputsFirst = Regex.Replace(outputsFirst, "[^a-zA-Z0-9]+", " ");

            var outputSecond = Encoding.ASCII.GetString(Encoding.Convert(Encoding.UTF8,
              Encoding.GetEncoding(Encoding.ASCII.EncodingName, new EncoderReplacementFallback(String.Empty),
                  new DecoderExceptionFallback()), Encoding.UTF8.GetBytes(s2data)));
            outputSecond = Regex.Replace(outputSecond, "[^a-zA-Z0-9]+", " ");


            List<string> gather = new List<string>();
            foreach (string k1 in outputsFirst.Split(' '))
            {
                if (k1.Length > 0)
                    gather.Add(k1);
            }

            gather.Add("ImageBytes");//imageData9.Split(' ')[0]);
            gather.Add(imageData9);//.Split(' ')[1]);

            foreach (string k2 in outputSecond.Split(' '))
            {
                if (k2.Length > 0)
                    gather.Add(k2);
            }
            List<string> columnList = new List<string>();
            List<string> valuesList = new List<string>();

            for (int i = 0; i < gather.Count - 1; i += 2)
            {
                if(gather[i]=="Status" || gather[i] == "TableName" || gather[i] == "Computerid" || gather[i] == "Activationkey")
                {
                    continue;
                }
                columnList.Add(gather[i]);
                valuesList.Add(gather[i + 1]);
            }
            DataTable dtServerSideBody = new DataTable();
            foreach (string cl in columnList)
            {
                dtServerSideBody.Columns.Add(cl);
            }
            dtServerSideBody.Rows.Add(valuesList.ToArray());
            string imagebytesone = dtServerSideBody.Rows[0]["ImageBytes"].ToString();
            //non image data cleaned


            //============TESTING for imagedata
            //File.WriteAllText("ImgBytFrmProg", imagebytesone,Encoding.UTF8);

            //string t1 = File.ReadAllText("ImgBytFrmProg", Encoding.Convert(Encoding.UTF8,Encoding.ASCII);
            //===================================


            return dtServerSideBody;

        }

    }
}
