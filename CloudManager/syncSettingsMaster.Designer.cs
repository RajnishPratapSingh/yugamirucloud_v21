namespace CloudManager
{
    partial class syncSettingsMaster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbDownloadData = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkCustom = new System.Windows.Forms.CheckBox();
            this.chkAutomatic = new System.Windows.Forms.CheckBox();
            this.chkManual = new System.Windows.Forms.CheckBox();
            this.gbOverWriting = new System.Windows.Forms.GroupBox();
            this.chkEditSaveNew = new System.Windows.Forms.CheckBox();
            this.chkBlockEdit = new System.Windows.Forms.CheckBox();
            this.chkTakeLatestOnOpen = new System.Windows.Forms.CheckBox();
            this.gbNetConnection = new System.Windows.Forms.GroupBox();
            this.chkOfflineEditSaveAsNew = new System.Windows.Forms.CheckBox();
            this.chkOfflineBlockEdit = new System.Windows.Forms.CheckBox();
            this.btnRestoreFactorySettings = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.gbDownloadData.SuspendLayout();
            this.gbOverWriting.SuspendLayout();
            this.gbNetConnection.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbDownloadData
            // 
            this.gbDownloadData.Controls.Add(this.label1);
            this.gbDownloadData.Controls.Add(this.chkCustom);
            this.gbDownloadData.Controls.Add(this.chkAutomatic);
            this.gbDownloadData.Controls.Add(this.chkManual);
            this.gbDownloadData.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.gbDownloadData.Location = new System.Drawing.Point(18, 23);
            this.gbDownloadData.Name = "gbDownloadData";
            this.gbDownloadData.Size = new System.Drawing.Size(222, 159);
            this.gbDownloadData.TabIndex = 0;
            this.gbDownloadData.TabStop = false;
            this.gbDownloadData.Text = "Download Patient Records From Cloud";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label1.Location = new System.Drawing.Point(86, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "(Highly Recommended)";
            // 
            // chkCustom
            // 
            this.chkCustom.AutoSize = true;
            this.chkCustom.Location = new System.Drawing.Point(16, 122);
            this.chkCustom.Name = "chkCustom";
            this.chkCustom.Size = new System.Drawing.Size(61, 17);
            this.chkCustom.TabIndex = 2;
            this.chkCustom.Text = "Custom";
            this.chkCustom.UseVisualStyleBackColor = true;
            this.chkCustom.CheckedChanged += new System.EventHandler(this.chkCustom_CheckedChanged);
            // 
            // chkAutomatic
            // 
            this.chkAutomatic.AutoSize = true;
            this.chkAutomatic.Location = new System.Drawing.Point(16, 83);
            this.chkAutomatic.Name = "chkAutomatic";
            this.chkAutomatic.Size = new System.Drawing.Size(73, 17);
            this.chkAutomatic.TabIndex = 1;
            this.chkAutomatic.Text = "Automatic";
            this.chkAutomatic.UseVisualStyleBackColor = true;
            // 
            // chkManual
            // 
            this.chkManual.AutoSize = true;
            this.chkManual.Checked = true;
            this.chkManual.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkManual.Enabled = false;
            this.chkManual.Location = new System.Drawing.Point(16, 41);
            this.chkManual.Name = "chkManual";
            this.chkManual.Size = new System.Drawing.Size(61, 17);
            this.chkManual.TabIndex = 0;
            this.chkManual.Text = "Manual";
            this.chkManual.UseVisualStyleBackColor = true;
            // 
            // gbOverWriting
            // 
            this.gbOverWriting.Controls.Add(this.chkEditSaveNew);
            this.gbOverWriting.Controls.Add(this.chkBlockEdit);
            this.gbOverWriting.Controls.Add(this.chkTakeLatestOnOpen);
            this.gbOverWriting.Location = new System.Drawing.Point(253, 23);
            this.gbOverWriting.Name = "gbOverWriting";
            this.gbOverWriting.Size = new System.Drawing.Size(209, 159);
            this.gbOverWriting.TabIndex = 1;
            this.gbOverWriting.TabStop = false;
            this.gbOverWriting.Text = "Report Over Writing";
            // 
            // chkEditSaveNew
            // 
            this.chkEditSaveNew.AutoSize = true;
            this.chkEditSaveNew.Location = new System.Drawing.Point(17, 122);
            this.chkEditSaveNew.Name = "chkEditSaveNew";
            this.chkEditSaveNew.Size = new System.Drawing.Size(128, 17);
            this.chkEditSaveNew.TabIndex = 4;
            this.chkEditSaveNew.Text = "Edit and save as new";
            this.chkEditSaveNew.UseVisualStyleBackColor = true;
            // 
            // chkBlockEdit
            // 
            this.chkBlockEdit.AutoSize = true;
            this.chkBlockEdit.Checked = true;
            this.chkBlockEdit.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBlockEdit.Location = new System.Drawing.Point(17, 83);
            this.chkBlockEdit.Name = "chkBlockEdit";
            this.chkBlockEdit.Size = new System.Drawing.Size(169, 17);
            this.chkBlockEdit.TabIndex = 3;
            this.chkBlockEdit.Text = "Block edit for non-local reports";
            this.chkBlockEdit.UseVisualStyleBackColor = true;
            // 
            // chkTakeLatestOnOpen
            // 
            this.chkTakeLatestOnOpen.AutoSize = true;
            this.chkTakeLatestOnOpen.Location = new System.Drawing.Point(17, 41);
            this.chkTakeLatestOnOpen.Name = "chkTakeLatestOnOpen";
            this.chkTakeLatestOnOpen.Size = new System.Drawing.Size(139, 17);
            this.chkTakeLatestOnOpen.TabIndex = 2;
            this.chkTakeLatestOnOpen.Text = "Take latest before open";
            this.chkTakeLatestOnOpen.UseVisualStyleBackColor = true;
            // 
            // gbNetConnection
            // 
            this.gbNetConnection.Controls.Add(this.chkOfflineEditSaveAsNew);
            this.gbNetConnection.Controls.Add(this.chkOfflineBlockEdit);
            this.gbNetConnection.Controls.Add(this.btnRestoreFactorySettings);
            this.gbNetConnection.Location = new System.Drawing.Point(468, 23);
            this.gbNetConnection.Name = "gbNetConnection";
            this.gbNetConnection.Size = new System.Drawing.Size(209, 159);
            this.gbNetConnection.TabIndex = 5;
            this.gbNetConnection.TabStop = false;
            this.gbNetConnection.Text = "Working Offline";
            // 
            // chkOfflineEditSaveAsNew
            // 
            this.chkOfflineEditSaveAsNew.AutoSize = true;
            this.chkOfflineEditSaveAsNew.Checked = true;
            this.chkOfflineEditSaveAsNew.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkOfflineEditSaveAsNew.Location = new System.Drawing.Point(20, 83);
            this.chkOfflineEditSaveAsNew.Name = "chkOfflineEditSaveAsNew";
            this.chkOfflineEditSaveAsNew.Size = new System.Drawing.Size(152, 17);
            this.chkOfflineEditSaveAsNew.TabIndex = 4;
            this.chkOfflineEditSaveAsNew.Text = "Allow edit but save as new";
            this.chkOfflineEditSaveAsNew.UseVisualStyleBackColor = true;
            // 
            // chkOfflineBlockEdit
            // 
            this.chkOfflineBlockEdit.AutoSize = true;
            this.chkOfflineBlockEdit.Checked = true;
            this.chkOfflineBlockEdit.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkOfflineBlockEdit.Location = new System.Drawing.Point(20, 41);
            this.chkOfflineBlockEdit.Name = "chkOfflineBlockEdit";
            this.chkOfflineBlockEdit.Size = new System.Drawing.Size(169, 17);
            this.chkOfflineBlockEdit.TabIndex = 3;
            this.chkOfflineBlockEdit.Text = "Block edit for non-local reports";
            this.chkOfflineBlockEdit.UseVisualStyleBackColor = true;
            // 
            // btnRestoreFactorySettings
            // 
            this.btnRestoreFactorySettings.Location = new System.Drawing.Point(20, 122);
            this.btnRestoreFactorySettings.Name = "btnRestoreFactorySettings";
            this.btnRestoreFactorySettings.Size = new System.Drawing.Size(155, 23);
            this.btnRestoreFactorySettings.TabIndex = 6;
            this.btnRestoreFactorySettings.Text = "Restore Factory Settings";
            this.btnRestoreFactorySettings.UseVisualStyleBackColor = true;
            this.btnRestoreFactorySettings.Click += new System.EventHandler(this.btnRestoreFactorySettings_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(165, 192);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(61, 192);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // syncSettingsMaster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(254, 227);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.gbNetConnection);
            this.Controls.Add(this.gbOverWriting);
            this.Controls.Add(this.gbDownloadData);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(270, 266);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(270, 266);
            this.Name = "syncSettingsMaster";
            this.ShowIcon = false;
            this.Text = "Cloud Settings";
            this.Load += new System.EventHandler(this.syncSettingsMaster_Load);
            this.gbDownloadData.ResumeLayout(false);
            this.gbDownloadData.PerformLayout();
            this.gbOverWriting.ResumeLayout(false);
            this.gbOverWriting.PerformLayout();
            this.gbNetConnection.ResumeLayout(false);
            this.gbNetConnection.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbDownloadData;
        private System.Windows.Forms.CheckBox chkManual;
        private System.Windows.Forms.GroupBox gbOverWriting;
        private System.Windows.Forms.CheckBox chkEditSaveNew;
        private System.Windows.Forms.CheckBox chkBlockEdit;
        private System.Windows.Forms.CheckBox chkTakeLatestOnOpen;
        private System.Windows.Forms.GroupBox gbNetConnection;
        private System.Windows.Forms.CheckBox chkOfflineEditSaveAsNew;
        private System.Windows.Forms.CheckBox chkOfflineBlockEdit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkCustom;
        private System.Windows.Forms.CheckBox chkAutomatic;
        private System.Windows.Forms.Button btnRestoreFactorySettings;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
    }
}