namespace CloudManager
{
    partial class frmCustomSyncSettingParams
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkIdleFor = new System.Windows.Forms.CheckBox();
            this.txtIdleMinutes = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.chkBetweenTime = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.chkWhenClosingApp = new System.Windows.Forms.CheckBox();
            this.chkWhenLaunchApp = new System.Windows.Forms.CheckBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cbFromHours = new System.Windows.Forms.ComboBox();
            this.cbFromMinutes = new System.Windows.Forms.ComboBox();
            this.cbToMinutes = new System.Windows.Forms.ComboBox();
            this.cbToHours = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // chkIdleFor
            // 
            this.chkIdleFor.AutoSize = true;
            this.chkIdleFor.Location = new System.Drawing.Point(12, 12);
            this.chkIdleFor.Name = "chkIdleFor";
            this.chkIdleFor.Size = new System.Drawing.Size(180, 17);
            this.chkIdleFor.TabIndex = 0;
            this.chkIdleFor.Text = "If application is idle for more than";
            this.chkIdleFor.UseVisualStyleBackColor = true;
            this.chkIdleFor.CheckedChanged += new System.EventHandler(this.chkIdleFor_CheckedChanged);
            // 
            // txtIdleMinutes
            // 
            this.txtIdleMinutes.AutoCompleteCustomSource.AddRange(new string[] {
            "2",
            "4",
            "6",
            "8",
            "10"});
            this.txtIdleMinutes.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtIdleMinutes.Enabled = false;
            this.txtIdleMinutes.Location = new System.Drawing.Point(198, 10);
            this.txtIdleMinutes.MaxLength = 2;
            this.txtIdleMinutes.Name = "txtIdleMinutes";
            this.txtIdleMinutes.Size = new System.Drawing.Size(24, 20);
            this.txtIdleMinutes.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(235, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "minutes";
            // 
            // chkBetweenTime
            // 
            this.chkBetweenTime.AutoSize = true;
            this.chkBetweenTime.Location = new System.Drawing.Point(12, 50);
            this.chkBetweenTime.Name = "chkBetweenTime";
            this.chkBetweenTime.Size = new System.Drawing.Size(68, 17);
            this.chkBetweenTime.TabIndex = 3;
            this.chkBetweenTime.Text = "Between";
            this.chkBetweenTime.UseVisualStyleBackColor = true;
            this.chkBetweenTime.CheckedChanged += new System.EventHandler(this.chkBetweenTime_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(201, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(16, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "to";
            // 
            // chkWhenClosingApp
            // 
            this.chkWhenClosingApp.AutoSize = true;
            this.chkWhenClosingApp.Location = new System.Drawing.Point(12, 87);
            this.chkWhenClosingApp.Name = "chkWhenClosingApp";
            this.chkWhenClosingApp.Size = new System.Drawing.Size(163, 17);
            this.chkWhenClosingApp.TabIndex = 8;
            this.chkWhenClosingApp.Text = "When closing the application";
            this.chkWhenClosingApp.UseVisualStyleBackColor = true;
            // 
            // chkWhenLaunchApp
            // 
            this.chkWhenLaunchApp.AutoSize = true;
            this.chkWhenLaunchApp.Location = new System.Drawing.Point(12, 116);
            this.chkWhenLaunchApp.Name = "chkWhenLaunchApp";
            this.chkWhenLaunchApp.Size = new System.Drawing.Size(164, 17);
            this.chkWhenLaunchApp.TabIndex = 9;
            this.chkWhenLaunchApp.Text = "When starting the application";
            this.chkWhenLaunchApp.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(294, 107);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label3.Location = new System.Drawing.Point(110, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "HH";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label4.Location = new System.Drawing.Point(165, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "MM";
            // 
            // cbFromHours
            // 
            this.cbFromHours.Enabled = false;
            this.cbFromHours.FormattingEnabled = true;
            this.cbFromHours.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23"});
            this.cbFromHours.Location = new System.Drawing.Point(77, 47);
            this.cbFromHours.Name = "cbFromHours";
            this.cbFromHours.Size = new System.Drawing.Size(34, 21);
            this.cbFromHours.TabIndex = 13;
            // 
            // cbFromMinutes
            // 
            this.cbFromMinutes.Enabled = false;
            this.cbFromMinutes.FormattingEnabled = true;
            this.cbFromMinutes.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "38",
            "39",
            "40",
            "41",
            "42",
            "43",
            "44",
            "45",
            "46",
            "47",
            "48",
            "49",
            "50",
            "51",
            "52",
            "53",
            "54",
            "55",
            "56",
            "57",
            "58",
            "59",
            "60"});
            this.cbFromMinutes.Location = new System.Drawing.Point(133, 47);
            this.cbFromMinutes.Name = "cbFromMinutes";
            this.cbFromMinutes.Size = new System.Drawing.Size(34, 21);
            this.cbFromMinutes.TabIndex = 14;
            // 
            // cbToMinutes
            // 
            this.cbToMinutes.Enabled = false;
            this.cbToMinutes.FormattingEnabled = true;
            this.cbToMinutes.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24",
            "25",
            "26",
            "27",
            "28",
            "29",
            "30",
            "31",
            "32",
            "33",
            "34",
            "35",
            "36",
            "37",
            "38",
            "39",
            "40",
            "41",
            "42",
            "43",
            "44",
            "45",
            "46",
            "47",
            "48",
            "49",
            "50",
            "51",
            "52",
            "53",
            "54",
            "55",
            "56",
            "57",
            "58",
            "59",
            "60"});
            this.cbToMinutes.Location = new System.Drawing.Point(294, 47);
            this.cbToMinutes.Name = "cbToMinutes";
            this.cbToMinutes.Size = new System.Drawing.Size(34, 21);
            this.cbToMinutes.TabIndex = 18;
            // 
            // cbToHours
            // 
            this.cbToHours.Enabled = false;
            this.cbToHours.FormattingEnabled = true;
            this.cbToHours.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23"});
            this.cbToHours.Location = new System.Drawing.Point(238, 47);
            this.cbToHours.Name = "cbToHours";
            this.cbToHours.Size = new System.Drawing.Size(34, 21);
            this.cbToHours.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label5.Location = new System.Drawing.Point(326, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "MM";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label6.Location = new System.Drawing.Point(271, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "HH";
            // 
            // frmCustomSyncSettingParams
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 143);
            this.Controls.Add(this.cbToMinutes);
            this.Controls.Add(this.cbToHours);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbFromMinutes);
            this.Controls.Add(this.cbFromHours);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.chkWhenLaunchApp);
            this.Controls.Add(this.chkWhenClosingApp);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.chkBetweenTime);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtIdleMinutes);
            this.Controls.Add(this.chkIdleFor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(416, 182);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(416, 182);
            this.Name = "frmCustomSyncSettingParams";
            this.ShowIcon = false;
            this.Text = "Custom Settings";
            this.Load += new System.EventHandler(this.frmCustomSyncSettingParams_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkIdleFor;
        private System.Windows.Forms.TextBox txtIdleMinutes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkBetweenTime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkWhenClosingApp;
        private System.Windows.Forms.CheckBox chkWhenLaunchApp;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbFromHours;
        private System.Windows.Forms.ComboBox cbFromMinutes;
        private System.Windows.Forms.ComboBox cbToMinutes;
        private System.Windows.Forms.ComboBox cbToHours;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}