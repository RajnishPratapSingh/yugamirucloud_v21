﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;

namespace CloudManager
{
    public class GetBsonFromServer
    {
        //create BSON request.Replace # with actual UniqueId
        //edited by Sumit for GSP-983
        //public static string initReqPatientJson = @"[{""TableName"":""" + Tables.PatientDetails.ToString() + @""",""Computerid"":""ComputerIdhgjyfjhg"",""Activationkey"":""activationKeykgjyftyfjvjhg"",""UniqueId"":""#""}]";
        public static string initReqPatientJson = @"[{""TableName"":""" + Tables.PatientDetails.ToString() + @""",""Computerid"":""ComputerIdhgjyfjhg"",""Activationkey"":""activationKeykgjyftyfjvjhg"",""UniqueId"":""#"",""user_id"":""user_idkgjyftyfjvjhg"",""reg_user_id"":""reg_user_idkgjyftyfjvjhg"",""Install_id"":""install_idkgjyftyfjvjhg""}]";
        public static string initReqKneeDownJson = @"[{""TableName"":""" + Tables.FrontBodyPositionKneeDown.ToString() + @""",""Computerid"":""ComputerIdhgjyfjhg"",""Activationkey"":""activationKeykgjyftyfjvjhg"",""UniqueId"":""#"",""user_id"":""user_idkgjyftyfjvjhg"",""reg_user_id"":""reg_user_idkgjyftyfjvjhg"",""Install_id"":""install_idkgjyftyfjvjhg""}]";
        public static string initReqStandingJson = @"[{""TableName"":""" + Tables.FrontBodyPositionStanding.ToString() + @""",""Computerid"":""ComputerIdhgjyfjhg"",""Activationkey"":""activationKeykgjyftyfjvjhg"",""UniqueId"":""#"",""user_id"":""user_idkgjyftyfjvjhg"",""reg_user_id"":""reg_user_idkgjyftyfjvjhg"",""Install_id"":""install_idkgjyftyfjvjhg""}]";
        public static string initReqsideJson = @"[{""TableName"":"""+Tables.SideBodyPosition.ToString()+ @""",""Computerid"":""ComputerIdhgjyfjhg"",""Activationkey"":""activationKeykgjyftyfjvjhg"",""UniqueId"":""#"",""user_id"":""user_idkgjyftyfjvjhg"",""reg_user_id"":""reg_user_idkgjyftyfjvjhg"",""Install_id"":""install_idkgjyftyfjvjhg""}]";

        static GetBsonFromServer()
        {
            initReqPatientJson = initReqPatientJson.Replace("ComputerIdhgjyfjhg", GlobalItems.ComputerID)
                                                    .Replace("activationKeykgjyftyfjvjhg", GlobalItems.ActivationKey)
                                                    .Replace("reg_user_idkgjyftyfjvjhg", GlobalItems.MY_Reg_User_ID)
                                                    .Replace("install_idkgjyftyfjvjhg", GlobalItems.MY_StallID);//user_idkgjyftyfjvjhg
                                                                                                                //.Replace("user_idkgjyftyfjvjhg", 
                                                                                                                //                                GlobalItems.Get_Max_Server_User_ID
                                                                                                                //                                (GlobalItems.MY_Reg_User_ID, GlobalItems.MY_StallID));//Need to update for other system data.


            initReqKneeDownJson = initReqKneeDownJson.Replace("ComputerIdhgjyfjhg", GlobalItems.ComputerID)
                                                    .Replace("activationKeykgjyftyfjvjhg", GlobalItems.ActivationKey)
                                                    .Replace("reg_user_idkgjyftyfjvjhg", GlobalItems.MY_Reg_User_ID)
                                                    .Replace("install_idkgjyftyfjvjhg", GlobalItems.MY_StallID);
            //.Replace("user_idkgjyftyfjvjhg",
            //                                GlobalItems.Get_Max_Server_User_ID
            //                                (GlobalItems.MY_Reg_User_ID, GlobalItems.MY_StallID));//Need to update for other system data.

            initReqStandingJson = initReqStandingJson.Replace("ComputerIdhgjyfjhg", GlobalItems.ComputerID)
                                                    .Replace("activationKeykgjyftyfjvjhg", GlobalItems.ActivationKey)
                                                    .Replace("reg_user_idkgjyftyfjvjhg", GlobalItems.MY_Reg_User_ID)
                                                    .Replace("install_idkgjyftyfjvjhg", GlobalItems.MY_StallID);
            //.Replace("user_idkgjyftyfjvjhg",
            //                                GlobalItems.Get_Max_Server_User_ID
            //                                (GlobalItems.MY_Reg_User_ID, GlobalItems.MY_StallID));//Need to update for other system data.

            initReqsideJson = initReqsideJson.Replace("ComputerIdhgjyfjhg", GlobalItems.ComputerID)
                                                    .Replace("activationKeykgjyftyfjvjhg", GlobalItems.ActivationKey)
                                                    .Replace("reg_user_idkgjyftyfjvjhg", GlobalItems.MY_Reg_User_ID)
                                                    .Replace("install_idkgjyftyfjvjhg", GlobalItems.MY_StallID);
                                                    //.Replace("user_idkgjyftyfjvjhg",
                                                    //                                GlobalItems.Get_Max_Server_User_ID
                                                    //                                (GlobalItems.MY_Reg_User_ID, GlobalItems.MY_StallID));//Need to update for other system data.
        }
        public static string getBsonFromServer(string jsonQuery, string uniqueID,string url,string server_user_id="")
        {
            var queryString = jsonQuery;
            if (queryString.Contains("#") && uniqueID != "0")// && server_user_id.Length==0)
            {
                queryString = queryString.Replace("#", uniqueID);

                queryString = queryString.Replace("user_idkgjyftyfjvjhg", server_user_id);
            }            
            else
            {
                queryString = queryString.Replace("#", uniqueID);

                queryString = queryString.Replace("user_idkgjyftyfjvjhg", server_user_id);
                //Call is from Other PC's data (Multilicense)
            }
                                                                       //GlobalItems.Get_Max_Server_User_ID
                                                                       //(GlobalItems.MY_Reg_User_ID, GlobalItems.MY_StallID));//Need to update for other system data.


            #region sample JSON
            //var studentObject = queryString;//Newtonsoft.Json.JsonConvert.DeserializeObject(@"[{""TableName"":""PatientDetails"",""Computerid"":""ComputerIdhgjyfjhg"",""Activationkey"":""activationKeykgjyftyfjvjhg"",""UniqueId"":""1""}]");//queryString);
            #endregion
            //3.
            var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(queryString);
            JsonSerializer jsonSerializer = new JsonSerializer();
            //4.
            MemoryStream objBsonMemoryStream = new MemoryStream();
            //5.
            Newtonsoft.Json.Bson.BsonWriter bsonWriterObject = new Newtonsoft.Json.Bson.BsonWriter(objBsonMemoryStream);
            //6.
            jsonSerializer.Serialize(bsonWriterObject, studentObject); //studentObject);
           
            byte[] requestByte = objBsonMemoryStream.ToArray();
            
            WebRequest webRequest = WebRequest.Create(url);//@"http://gs-demo.jma.website/apioauthdata/index.php/cloudData/downloadpatient_data");//CloudUrls.FrontBodyPositionKneeDownSendToUrl);
            webRequest.Method = "POST";
            webRequest.ContentType = "application/json";
            webRequest.ContentLength = requestByte.Length;

            // create our stram to send
            Stream webDataStream = null;
            try
            {
                webDataStream = webRequest.GetRequestStream();
                webDataStream.Write(requestByte, 0, requestByte.Length);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            string ed = webDataStream.ToString();

            // get the response from our stream

            WebResponse webResponse = webRequest.GetResponse();
            
            string tp = webResponse.ContentType;
            try
            {
                webDataStream = webResponse.GetResponseStream();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            // convert the result into a String
            StreamReader webResponseSReader = new StreamReader(webDataStream);
            String responseFromServer = webResponseSReader.ReadToEnd();           
            return responseFromServer;
        }
    }
}
