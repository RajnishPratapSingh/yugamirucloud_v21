﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    /// <summary>
    /// Class Implemented by sumit for GSP-1081
    /// </summary>
    public class clsIDNameHandler
    {
        
        string AppLanguage { get; set; }

       string ID_full = string.Empty;
       string Name_full = string.Empty;

        public string GetFull_ID
        {
            get { return ID_full; }
        }
        public string GetFull_Name
        {
            get { return Name_full; }
        }

        public clsIDNameHandler(string fullID, string fullName,string AppLanguage)
        {
            this.ID_full = fullID;
            this.Name_full = fullName;
            this.AppLanguage = AppLanguage;
        }

        public bool Is_Name_Entered_In_EN_Alphabet()
        {
            bool isEn = true;
            if (Name_full.Length > 0)
            {
                foreach (char c in Name_full)
                {
                    if (System.Text.Encoding.UTF8.GetByteCount(new char[] { c }) > 1)
                    {
                        //means it is non english alphabet.
                        isEn = false;
                        break;
                    }
                }
            }
            return isEn;
        }

        #region Get Data Section For displaying it in Application

        public string GetIDForApp()
        {
            string ret_id = string.Empty;
            if (ID_full.Length > 7)
            {
                ret_id = ID_full.Substring(0, 7) + "...";
            }
            else
            {
                ret_id = ID_full;
            }
            return ret_id;
        }       
        public string GetNameForApp()
        {
            string ret_name = string.Empty;
            if(Is_Name_Entered_In_EN_Alphabet())
            {
                if (Name_full.Length > 11)
                {
                    ret_name = Name_full.Substring(0, 11) + "...";
                }
                else
                {
                    ret_name = Name_full;
                }
            }
            else
            {
                if (Name_full.Length > 8)
                {
                    ret_name = Name_full.Substring(0, 8) + "...";
                }
                else
                {
                    ret_name = Name_full;
                }
            }           
            return ret_name;
        }
        #endregion

        #region Get Data Section For displaying it in Report
       public string GetIDForReport()
        {
            return ID_full;//As now this is the requirement but may be changed in future so better to have a function.
        }

        public string GetNameForReport()
        {
            string ret_name = string.Empty;
            if (Is_Name_Entered_In_EN_Alphabet())
            {
                if (Name_full.Length > 16)
                {
                    ret_name = Name_full.Substring(0, 16) + "...";
                }
                else
                {
                    ret_name = Name_full;
                }
            }
            else
            {
                if (Name_full.Length > 9)
                {
                    ret_name = Name_full.Substring(0, 9) + "...";
                }
                else
                {
                    ret_name = Name_full;
                }
            }
            return ret_name;
        }
        #endregion
    }
}
