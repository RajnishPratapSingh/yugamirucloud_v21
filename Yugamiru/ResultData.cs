﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Yugamiru
{
    public class ResultData
    {

        int YUGAMIRU_POINT_RANK_NONE = 0;
        int YUGAMIRU_POINT_RANK_A = 1;
        int YUGAMIRU_POINT_RANK_B = 2;
        int YUGAMIRU_POINT_RANK_C = 3;
        int YUGAMIRU_POINT_RANK_D = 4;
        FrontBodyResultData m_FrontBodyResultDataStanding = new FrontBodyResultData();
        FrontBodyResultData m_FrontBodyResultDataKneedown = new FrontBodyResultData();
        SideBodyResultData m_SideBodyResultData = new SideBodyResultData(); 
        int m_iPosturePatternID;

        public int  m_Score;

        public void Reset()
        {
            m_FrontBodyResultDataStanding.Reset();
            m_FrontBodyResultDataKneedown.Reset();
            m_SideBodyResultData.Reset();
            m_iPosturePatternID = Constants.POSTUREPATTERNID_NONE;
        }



             public void Calc(
              int sex,
              int gene,

              FrontBodyAngle FrontBodyAngleStanding, 
              FrontBodyAngle FrontBodyAngleKneedown,
              SideBodyAngle SideBodyAngle,
              ResultActionScriptMgr ResultActionScriptMgr,
              ResultActionScriptMgr ResultActionScriptMgrSide )
      {
          m_FrontBodyResultDataStanding.Calc( sex, gene, FrontBodyAngleStanding, ResultActionScriptMgr );
          m_FrontBodyResultDataKneedown.Calc( sex, gene, FrontBodyAngleKneedown, ResultActionScriptMgr );
          m_SideBodyResultData.Calc( sex, gene, SideBodyAngle, ResultActionScriptMgrSide );
      }

        public void Calc(
    int sex,
    int gene,
        FrontBodyAngle FrontBodyAngleStanding, 
		FrontBodyAngle FrontBodyAngleKneedown,
		SideBodyAngle SideBodyAngle,
		ResultActionScriptMgr ResultActionScriptMgr,
		ResultActionScriptMgr ResultActionScriptMgrSide,
		int iJudgeMode )
{

    m_FrontBodyResultDataStanding.Calc( sex, gene, FrontBodyAngleStanding, ResultActionScriptMgr );
	m_FrontBodyResultDataKneedown.Calc( sex, gene, FrontBodyAngleKneedown, ResultActionScriptMgr );
	m_SideBodyResultData.Calc( sex, gene, SideBodyAngle, ResultActionScriptMgrSide );

	if (( 12.0 <= SideBodyAngle.GetShoulderEarAngle() ) &&
		( 10.0 <= SideBodyAngle.GetPelvicAngle() )){
		m_iPosturePatternID = Constants.POSTUREPATTERNID_STOOP_AND_BEND_BACKWARD;
	}
	if ( iJudgeMode == 1 ){
		if (( 12.0 <= SideBodyAngle.GetShoulderEarAngle() ) &&
			(( 10.0 <= SideBodyAngle.GetPelvicAngle() ) ||
			 ( -5.0 <= SideBodyAngle.GetBodyBalance() ))) {
			m_iPosturePatternID = Constants.POSTUREPATTERNID_STOOP_AND_BEND_BACKWARD;
		}
		else if (( SideBodyAngle.GetPelvicAngle() < 10.0 ) &&
			 ( SideBodyAngle.GetBodyBalance() > -5.0 ) &&
			 ( 12.0 <= SideBodyAngle.GetShoulderEarAngle() )) {
			m_iPosturePatternID = Constants.POSTUREPATTERNID_STOOP;	
		}
		else if (( SideBodyAngle.GetShoulderEarAngle() < 12.0 ) &&
			(( 10.0 <= SideBodyAngle.GetPelvicAngle() ) ||
			( SideBodyAngle.GetBodyBalance() < -5.0 ))) {
			m_iPosturePatternID = Constants.POSTUREPATTERNID_BEND_BACKWARD;
		}
		else if (( SideBodyAngle.GetPelvicAngle() < 5.0 ) &&
			( SideBodyAngle.GetShoulderEarAngle() < 12.0 ) &&
			( -5.0 <= SideBodyAngle.GetBodyBalance() )){
			m_iPosturePatternID = Constants.POSTUREPATTERNID_FLATBACK;
		}
		else{
			int iFrontSideUnbalanced = 0;
			if ( m_FrontBodyResultDataStanding.GetCenterBalance() != 0 ){
				iFrontSideUnbalanced = 1;			
			}
			if ( m_FrontBodyResultDataStanding.GetHip() <= -2 ){
				iFrontSideUnbalanced = 1;
			}
			if ( 2 <= m_FrontBodyResultDataStanding.GetHip() ){
				iFrontSideUnbalanced = 1;		
			}
			if ( m_FrontBodyResultDataStanding.GetRightKnee() <= -2 ){
				iFrontSideUnbalanced = 1;		
			}
			if ( 2 <= m_FrontBodyResultDataStanding.GetRightKnee() ){
				iFrontSideUnbalanced = 1;		
			}
			if ( m_FrontBodyResultDataStanding.GetLeftKnee() <= -2 ){
				iFrontSideUnbalanced = 1;
			}
			if ( m_FrontBodyResultDataStanding.GetLeftKnee() >= 2 ){
				iFrontSideUnbalanced = 1;
			}
			if ( m_FrontBodyResultDataStanding.GetBodyCenter() <= -2 ){
				iFrontSideUnbalanced = 1;
			}
			if ( m_FrontBodyResultDataStanding.GetBodyCenter() >= 2 ){
				iFrontSideUnbalanced = 1;
			}
			if ( m_FrontBodyResultDataStanding.GetShoulderBal() <= -2 ){
				iFrontSideUnbalanced = 1;
			}
			if ( m_FrontBodyResultDataStanding.GetShoulderBal() >= 2 ){
				iFrontSideUnbalanced = 1;
			}
			if ( m_FrontBodyResultDataStanding.GetHeadCenter() <= -2 ){
				iFrontSideUnbalanced = 1;
			}
			if ( m_FrontBodyResultDataStanding.GetHeadCenter() >= 2 ){
				iFrontSideUnbalanced = 1;
			}
			if ( m_FrontBodyResultDataStanding.GetEarBal() <= -2 ){
				iFrontSideUnbalanced = 1;
			}
			if ( m_FrontBodyResultDataStanding.GetEarBal() >= 2 ){
				iFrontSideUnbalanced = 1;
			}
			if ( m_FrontBodyResultDataKneedown.GetCenterBalance() != 0 ){
				iFrontSideUnbalanced = 1;			
			}
			if ( m_FrontBodyResultDataKneedown.GetHip() <= -2 ){
				iFrontSideUnbalanced = 1;
			}
			if ( 2 <= m_FrontBodyResultDataKneedown.GetHip() ){
				iFrontSideUnbalanced = 1;		
			}
			if ( m_FrontBodyResultDataKneedown.GetRightKnee() <= -2 ){
				iFrontSideUnbalanced = 1;		
			}
			if ( 2 <= m_FrontBodyResultDataKneedown.GetRightKnee() ){
				iFrontSideUnbalanced = 1;		
			}
			if ( m_FrontBodyResultDataKneedown.GetLeftKnee() <= -2 ){
				iFrontSideUnbalanced = 1;
			}
			if ( m_FrontBodyResultDataKneedown.GetLeftKnee() >= 2 ){
				iFrontSideUnbalanced = 1;
			}
			if ( m_FrontBodyResultDataKneedown.GetBodyCenter() <= -2 ){
				iFrontSideUnbalanced = 1;
			}
			if ( m_FrontBodyResultDataKneedown.GetBodyCenter() >= 2 ){
				iFrontSideUnbalanced = 1;
			}
			if ( m_FrontBodyResultDataKneedown.GetShoulderBal() <= -2 ){
				iFrontSideUnbalanced = 1;
			}
			if ( m_FrontBodyResultDataKneedown.GetShoulderBal() >= 2 ){
				iFrontSideUnbalanced = 1;
			}
			if ( m_FrontBodyResultDataKneedown.GetHeadCenter() <= -2 ){
				iFrontSideUnbalanced = 1;
			}
			if ( m_FrontBodyResultDataKneedown.GetHeadCenter() >= 2 ){
				iFrontSideUnbalanced = 1;
			}
			if ( m_FrontBodyResultDataKneedown.GetEarBal() <= -2 ){
				iFrontSideUnbalanced = 1;
			}
			if ( m_FrontBodyResultDataKneedown.GetEarBal() >= 2 ){
				iFrontSideUnbalanced = 1;
			}
			if ( iFrontSideUnbalanced == 1 ){
				m_iPosturePatternID = Constants.POSTUREPATTERNID_FRONTSIDE_UNBALANCED;		
			}
			else{
				m_iPosturePatternID = Constants.POSTUREPATTERNID_NORMAL;
			}
		}
	}
	else{
		if (( 12.0 <= SideBodyAngle.GetShoulderEarAngle() ) &&
			( SideBodyAngle.GetPelvicAngle() < 10.0 )){
			m_iPosturePatternID = Constants.POSTUREPATTERNID_STOOP;	
		}
		if (( SideBodyAngle.GetShoulderEarAngle() < 12.0 ) &&
			( 10.0 <= SideBodyAngle.GetPelvicAngle() )){
			m_iPosturePatternID = Constants.POSTUREPATTERNID_BEND_BACKWARD;
		}
		if (( SideBodyAngle.GetShoulderEarAngle() < 12.0 ) &&
			( SideBodyAngle.GetPelvicAngle() < 10.0 )){
			if (( SideBodyAngle.GetShoulderEarAngle() < 12.0 ) &&
				( SideBodyAngle.GetPelvicAngle() <= 5.0 )){
				m_iPosturePatternID = Constants.POSTUREPATTERNID_FLATBACK;
			}
			if (( SideBodyAngle.GetShoulderEarAngle() < 12.0 ) &&
				( 5.0 < SideBodyAngle.GetPelvicAngle() ) &&
				( SideBodyAngle.GetPelvicAngle() < 10.0 )){
				m_iPosturePatternID = Constants.POSTUREPATTERNID_NORMAL;	
			}
		}
	}
}

public int GetStandingLeftKnee() 
{
	return m_FrontBodyResultDataStanding.GetLeftKnee();
}

        public int GetStandingRightKnee()
{
	return m_FrontBodyResultDataStanding.GetRightKnee();
}

        public int GetKneedownLeftKnee() 
{
	return m_FrontBodyResultDataKneedown.GetLeftKnee();
}

        public int GetKneedownRightKnee() 
{
	return m_FrontBodyResultDataKneedown.GetRightKnee();
}

        public int GetStandingHip() 
{
	return m_FrontBodyResultDataStanding.GetHip();
}

        public int GetKneedownHip() 
{
	return m_FrontBodyResultDataKneedown.GetHip();
}

         public int GetStandingCenterBalance() 
{
	return m_FrontBodyResultDataStanding.GetCenterBalance();
}

        public int GetKneedownCenterBalance() 
{
	return m_FrontBodyResultDataKneedown.GetCenterBalance();
}

        public int GetStandingShoulderBal() 
{
	return m_FrontBodyResultDataStanding.GetShoulderBal();
}

        public int GetKneedownShoulderBal() 
{
	return m_FrontBodyResultDataKneedown.GetShoulderBal();
}

        public int GetStandingEarBal() 
{
	return m_FrontBodyResultDataStanding.GetEarBal();
}

        public int GetKneedownEarBal() 
{
	return m_FrontBodyResultDataKneedown.GetEarBal();
}

        public int GetStandingBodyCenter() 
{
	return m_FrontBodyResultDataStanding.GetBodyCenter();
}

        public int GetKneedownBodyCenter() 
{
	return m_FrontBodyResultDataKneedown.GetBodyCenter();
}

        public int GetStandingHeadCenter() 
{
	return m_FrontBodyResultDataStanding.GetHeadCenter();
}

        public int GetKneedownHeadCenter() 
{
	return m_FrontBodyResultDataKneedown.GetHeadCenter();
}

        public int GetSideBodyBalance() 
{
            return m_SideBodyResultData.GetSideBodyBalance();
            //return 1;
}

        public int GetNeckForwardLeaning() 
{
            return m_SideBodyResultData.GetNeckForwardLeaning();
            //return 2;
}

        public int GetPosturePatternID() 
{
	return m_iPosturePatternID;
    //return 3;
}



    public int CalcYugamiPoint()/*FrontBodyAngle m_FrontBodyAngleStanding,
            FrontBodyAngle m_FrontBodyAngleKneedown, SideBodyAngle m_SideBodyAngle)*/
{   
    double iDistPoint =
          Math.Abs(m_FrontBodyResultDataStanding.GetRightKnee())     // XO Right
        + Math.Abs(m_FrontBodyResultDataStanding.GetLeftKnee())      // XO Left
        + Math.Abs(m_FrontBodyResultDataKneedown.GetRightKnee()) * 3 // InOut Right
        + Math.Abs(m_FrontBodyResultDataKneedown.GetLeftKnee()) * 3  // InOut Left
        + Math.Abs(m_FrontBodyResultDataStanding.GetHip()) * 2           // œ”Õ‰ñù@—§ˆÊ
        + Math.Abs(m_FrontBodyResultDataKneedown.GetHip()) * 2           // œ”Õ‰ñù@‹üL
        + Math.Abs(m_FrontBodyResultDataStanding.GetCenterBalance()) * 4 // œ”ÕƒVƒtƒg@—§ˆÊ
        + Math.Abs(m_FrontBodyResultDataKneedown.GetCenterBalance()) * 3 // œ”ÕƒVƒtƒg@‹üL
        + Math.Abs(m_FrontBodyResultDataStanding.GetShoulderBal())   // Œ¨‹“ã@—§ˆÊ
        + Math.Abs(m_FrontBodyResultDataKneedown.GetShoulderBal())   // Œ¨‹“ã@‹üL 
        + Math.Abs(m_FrontBodyResultDataStanding.GetEarBal())        // “ª•”ŒX‚«@—§ˆÊ
        + Math.Abs(m_FrontBodyResultDataKneedown.GetEarBal())        // “ª•”ŒX‚«@‹üL  
        + Math.Abs(m_FrontBodyResultDataStanding.GetBodyCenter())        // ‘ÌŠ²ƒVƒtƒg@—§ˆÊ
        + Math.Abs(m_FrontBodyResultDataKneedown.GetBodyCenter())        // ‘ÌŠ²ƒVƒtƒg@‹üL  
        + Math.Abs(m_FrontBodyResultDataStanding.GetHeadCenter())        // ŽñƒVƒtƒg@—§ˆÊ
        + Math.Abs(m_FrontBodyResultDataKneedown.GetHeadCenter())        // ŽñƒVƒtƒg@—§ˆÊ  
        + Math.Abs(m_SideBodyResultData.GetSideBodyBalance())	// ã‘Ì@—§ˆÊ  


    ;

            int iScore = 0;
	if( iDistPoint< 12 ) {
		iScore = 100 - 4 *(int) iDistPoint;
	} else if( iDistPoint< 37 ) {
		iScore = 52 - ((int)iDistPoint - 12) * 4 / 10;
	} else if (iDistPoint <= 43 ) {
		iScore = 42 - ((int)iDistPoint - 37) * 15 / 10;
	} else {
		iScore = 33;
	}
	if(iScore< 0){
		iScore = 0;
	}
	return iScore;
}

public int CalcYugamiPointRank()/*FrontBodyAngle m_FrontBodyAngleStanding, 
    FrontBodyAngle m_FrontBodyAngleKneedown, SideBodyAngle m_SideBodyAngle) */
{
            int iPoint = CalcYugamiPoint();//m_FrontBodyAngleStanding, m_FrontBodyAngleKneedown, m_SideBodyAngle);
            SetScore(iPoint);
	if (( 91 <= iPoint ) && ( iPoint <= 100 )){
		return( YUGAMIRU_POINT_RANK_A );
	}
	if ( ( 61 <= iPoint ) && ( iPoint <= 90 )){
		return( YUGAMIRU_POINT_RANK_B );
	}
	if ( ( 41 <= iPoint ) && ( iPoint <= 60 )){
		return( YUGAMIRU_POINT_RANK_C );
	}
	if ( ( 0 <= iPoint ) && ( iPoint <= 40 )){
		return( YUGAMIRU_POINT_RANK_D );
	}
	return( YUGAMIRU_POINT_RANK_NONE );
}
        public void SetScore(int Score)
        {
            m_Score = Score;
        }
        public int GetScore()
        {
            return m_Score;
        }

    }
}
