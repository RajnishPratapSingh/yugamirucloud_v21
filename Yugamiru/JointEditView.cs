﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Yugamiru.stretchDIBbits;

namespace Yugamiru
{
    public partial class JointEditView : Form
    {
        //FrontJointEditWnd m_wndStandingPosture = new FrontJointEditWnd();
        PictureBox m_MainPic = new PictureBox();
        FrontJointEditWnd m_wndStandingPosture = new FrontJointEditWnd();
        FrontJointEditWnd m_wndKneedownPosture = new FrontJointEditWnd();
        JointEditDoc m_JointEditDoc;
        FrontBodyPosition m_FrontBodyPositionStanding = new FrontBodyPosition();
        FrontBodyPosition m_FrontBodyPositionKneedown = new FrontBodyPosition();
        public static bool backbutton = false; //added by rohini for GSP-1179
        byte[] m_pbyteBits;

        int m_ZoomFlag = 0;
        public void DisposeControls()
        {
            m_MainPic.Image.Dispose();

            IDC_Mag1Btn.Image.Dispose();
            IDC_Mag2Btn.Image.Dispose();
            IDC_ResetImgBtn.Image.Dispose();

            IDC_OkBtn.Image.Dispose();
            IDC_CancelBtn.Image.Dispose();

            Yugamiru.Properties.Resources.Mainpic4.Dispose();

            Yugamiru.Properties.Resources.mag2.Dispose();
            Yugamiru.Properties.Resources.mag1.Dispose();
            Yugamiru.Properties.Resources.mag3_down.Dispose();

            Yugamiru.Properties.Resources.gonextgreen_on.Dispose();
            Yugamiru.Properties.Resources.gobackgreen_on.Dispose();


            this.Dispose();
            this.Close();
        }
        public JointEditView(JointEditDoc GetDocument)
        {
            InitializeComponent();

            label1.Font = new Font("MS UI Gothic", 12, FontStyle.Regular);
            label2.Font = new Font("MS UI Gothic", 12, FontStyle.Regular);

            m_JointEditDoc = GetDocument;
            m_MainPic.Size = new Size(Yugamiru.Properties.Resources.Mainpic4.Size.Width,
                Yugamiru.Properties.Resources.Mainpic4.Size.Height);
            m_MainPic.BackColor = Color.Transparent;
            this.Controls.Add(m_MainPic);
            m_MainPic.Image = Yugamiru.Properties.Resources.Mainpic4;

            IDC_Mag1Btn.Image = Yugamiru.Properties.Resources.mag2_up;
            IDC_Mag2Btn.Image = Yugamiru.Properties.Resources.mag1_up;
            IDC_ResetImgBtn.Image = Yugamiru.Properties.Resources.mag3_up;

            IDC_OkBtn.Image = Yugamiru.Properties.Resources.gonextred_on;
            IDC_CancelBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;

            pictureBox_standing.Size = new Size(423, 470);
            pictureBox_standing.Visible = true;
            //pictureBox_standing.Image = Yugamiru.Properties.Resources.kutui;
            //pictureBox_KneeDown.Image = Yugamiru.Properties.Resources.ritui;


            int iBmpWidthStep = (1024 * 3 + 3) / 4 * 4;
            int iBmpBitsSize = iBmpWidthStep * 1280;

            if (m_pbyteBits != null)
            {
                //delete[] m_pbyteBits;
                m_pbyteBits = null;
            }
            m_pbyteBits = new byte[iBmpBitsSize];
            int i = 0;
            for (i = 0; i < iBmpBitsSize; i++)
            {

                m_pbyteBits[i] = 0;
            }
            m_pbyteBits = converterDemo(Yugamiru.Properties.Resources.sokui);



            //m_JointEditDoc.OnNewDocument();

            m_wndStandingPosture.SetMarkerSizeBase(m_JointEditDoc.GetMarkerSize());
            m_wndStandingPosture.SetDataVersion(m_JointEditDoc.GetDataVersion());
            m_wndStandingPosture.SetShoulderOffset0(m_JointEditDoc.GetShoulderOffset0());
            m_wndStandingPosture.SetShoulderOffset1(m_JointEditDoc.GetShoulderOffset1());
            m_wndStandingPosture.SetShoulderOffset2(m_JointEditDoc.GetShoulderOffset2());
            m_wndStandingPosture.SetStyleLine(m_JointEditDoc.IsValidStyleLine());
            m_wndStandingPosture.SetStyleLineStyle(m_JointEditDoc.GetStyleLineStyle());
            m_wndStandingPosture.SetStyleLineColor(m_JointEditDoc.GetStyleLineColor());
            m_wndStandingPosture.SetStyleLineWidth(m_JointEditDoc.GetStyleLineWidth());
            m_wndStandingPosture.m_iOffscreenWidth = 423;
            m_wndStandingPosture.m_iOffscreenHeight = 470;


            // m_BalanceLabData.InitBodyBalance(1024, 1280, m_CalibInf, m_CalcPostureProp);
            //m_FrontBodyPositionStanding = m_BalanceLabData.m_FrontBodyPositionStanding;
            if (m_JointEditDoc.GetInputMode() == Constants.INPUTMODE_NEW)
            {
                m_JointEditDoc.GetStandingFrontBodyPosition(ref m_FrontBodyPositionStanding);
                m_wndStandingPosture.SetFrontBodyPosition(m_FrontBodyPositionStanding);
            }
            else
                m_wndStandingPosture.SetFrontBodyPosition(m_JointEditDoc.m_FrontBodyPositionStanding);
            /*       m_wndStandingPosture.SetBackgroundBitmap(Yugamiru.Properties.Resources.sokui.Width,
                       Yugamiru.Properties.Resources.sokui.Height, m_pbyteBits);*/
            //m_FrontBodyPositionStanding.SetAllPositionDetected(); --- by meena
            m_wndStandingPosture.SetBackgroundBitmap(1024, 1280, m_pbyteBits);




            m_wndKneedownPosture.SetMarkerSizeBase(m_JointEditDoc.GetMarkerSize());
            m_wndKneedownPosture.SetDataVersion(m_JointEditDoc.GetDataVersion());
            m_wndKneedownPosture.SetShoulderOffset0(m_JointEditDoc.GetShoulderOffset0());
            m_wndKneedownPosture.SetShoulderOffset1(m_JointEditDoc.GetShoulderOffset1());
            m_wndKneedownPosture.SetShoulderOffset2(m_JointEditDoc.GetShoulderOffset2());

            m_wndKneedownPosture.SetStyleLine(m_JointEditDoc.IsValidStyleLine());
            m_wndKneedownPosture.SetStyleLineStyle(m_JointEditDoc.GetStyleLineStyle());
            m_wndKneedownPosture.SetStyleLineColor(m_JointEditDoc.GetStyleLineColor());
            m_wndKneedownPosture.SetStyleLineWidth(m_JointEditDoc.GetStyleLineWidth());
            m_wndKneedownPosture.m_iOffscreenWidth = 423;
            m_wndKneedownPosture.m_iOffscreenHeight = 470;

            //m_FrontBodyPositionKneedown = m_BalanceLabData.m_FrontBodyPositionKneedown;
            if (m_JointEditDoc.GetInputMode() == Constants.INPUTMODE_NEW)
            {
                m_JointEditDoc.GetKneedownFrontBodyPosition(ref m_FrontBodyPositionKneedown);
                m_wndKneedownPosture.SetFrontBodyPosition(m_FrontBodyPositionKneedown);
            }
            else
                m_wndKneedownPosture.SetFrontBodyPosition(m_JointEditDoc.m_FrontBodyPositionKneedown);

            /*m_wndKneedownPosture.SetBackgroundBitmap(Yugamiru.Properties.Resources.sokui.Width,
                 Yugamiru.Properties.Resources.sokui.Height, m_pbyteBits);*/
            m_wndKneedownPosture.SetBackgroundBitmap(1024, 1280, m_pbyteBits);
            //m_FrontBodyPositionKneedown.SetAllPositionDetected(); --- by meena

            switch (m_JointEditDoc.GetJointEditViewMode())
            {
                case Constants.JOINTEDITVIEWMODE_ANKLE_AND_HIP:
                    SetAnkleAndHipMode();
                    break;
                case Constants.JOINTEDITVIEWMODE_KNEE:
                    SetKneeMode();
                    break;
                case Constants.JOINTEDITVIEWMODE_UPPERBODY:
                    SetUpperBodyMode();
                    break;
                case Constants.JOINTEDITVIEWMODE_SIDE:
                case Constants.JOINTEDITVIEWMODE_NONE:
                default:
                    break;
            }


            /*
                        Panel pan = new Panel();
                        //pan.Name = "panel" + i;
                        TrackBar idc_slider1 = new TrackBar();

                        pan.Location = new Point(500, 300);
                        pan.Size = new Size(34, 192);  // just an example
                        pan.Controls.Add(idc_slider1);
                        this.Controls.Add(pan);
                        */
            /*
            IDC_SLIDER1.Size = new Size(34, 192);
            IDC_SLIDER1.Location = new Point(1115, 300); */

        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            m_wndStandingPosture.m_pbyteBits = m_JointEditDoc.m_FrontStandingImageBytes;
            m_wndStandingPosture.UpdateOffscreen(e.Graphics);
            //OnPicture1(e.Graphics);
            SetEditPointMessage(m_wndStandingPosture.GetEditPtID());
            //m_JointEditDoc.SetStandingMarkerTouchFlag(m_wndStandingPosture.GetEditPtID());
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            Graphics temp_Graphics = this.CreateGraphics();
            if ((m_wndStandingPosture.m_iMouseCaptureMode == 1) && (m_wndStandingPosture.m_iEditPtID != -1) &&
                (m_wndStandingPosture.m_isValidEditPoint[m_wndStandingPosture.m_iEditPtID]))
            {
                // ŠÖßˆÊ’uC³
                m_wndStandingPosture.m_aptEdit[m_wndStandingPosture.m_iEditPtID].X =
                    e.X * m_wndStandingPosture.m_iSrcWidth / m_wndStandingPosture.m_iOffscreenWidth + m_wndStandingPosture.m_iSrcX;
                m_wndStandingPosture.m_aptEdit[m_wndStandingPosture.m_iEditPtID].Y =
                    e.Y * m_wndStandingPosture.m_iSrcHeight / m_wndStandingPosture.m_iOffscreenHeight + m_wndStandingPosture.m_iSrcY;

                if (m_wndStandingPosture.m_aptEdit[m_wndStandingPosture.m_iEditPtID].X < 0)
                {
                    m_wndStandingPosture.m_aptEdit[m_wndStandingPosture.m_iEditPtID].X = 0;
                }
                if (m_wndStandingPosture.m_aptEdit[m_wndStandingPosture.m_iEditPtID].Y < 0)
                {
                    m_wndStandingPosture.m_aptEdit[m_wndStandingPosture.m_iEditPtID].Y = 0;
                }
                if (m_wndStandingPosture.m_aptEdit[m_wndStandingPosture.m_iEditPtID].X >=
                    m_wndStandingPosture.m_iBackgroundWidth)
                {
                    m_wndStandingPosture.m_aptEdit[m_wndStandingPosture.m_iEditPtID].X =
                        m_wndStandingPosture.m_iBackgroundWidth - 1;
                }
                if (m_wndStandingPosture.m_aptEdit[m_wndStandingPosture.m_iEditPtID].Y >= m_wndStandingPosture.m_iBackgroundHeight)
                {
                    m_wndStandingPosture.m_aptEdit[m_wndStandingPosture.m_iEditPtID].Y =
                        m_wndStandingPosture.m_iBackgroundHeight - 1;
                }

                //m_wndStandingPosture.UpdateOffscreen(temp_Graphics);
                /*       if (GetParent() != NULL)
                       {
                           GetParent()->SendMessage(WM_COMMAND, (WPARAM)GetDlgCtrlID(), (LPARAM)GetSafeHwnd());
                       }
                       UpdateOffscreen();
                       CRect rcClient;
                       GetClientRect(&rcClient);
                       InvalidateRect(&rcClient);
                       UpdateWindow();
                       */
                pictureBox_standing.Invalidate();
            }
            else if (m_wndStandingPosture.m_iMouseCaptureMode == 2)
            {
                int iDiffX = e.X - m_wndStandingPosture.m_iMousePointXOnDragStart;
                int iDiffY = e.Y - m_wndStandingPosture.m_iMousePointYOnDragStart;
                m_wndStandingPosture.m_iSrcX = m_wndStandingPosture.m_iSrcXOnDragStart - iDiffX * m_wndStandingPosture.m_iSrcWidthOnDragStart / m_wndStandingPosture.m_iScreenWidthOnDragStart;
                m_wndStandingPosture.m_iSrcY = m_wndStandingPosture.m_iSrcYOnDragStart - iDiffY * m_wndStandingPosture.m_iSrcHeightOnDragStart / m_wndStandingPosture.m_iScreenHeightOnDragStart;
                if (m_wndStandingPosture.m_iSrcX < 0)
                {
                    m_wndStandingPosture.m_iSrcX = 0;
                }
                if (m_wndStandingPosture.m_iSrcY < 0)
                {
                    m_wndStandingPosture.m_iSrcY = 0;
                }
                if (m_wndStandingPosture.m_iSrcX + m_wndStandingPosture.m_iSrcWidthOnDragStart >=
                    m_wndStandingPosture.m_iBackgroundWidth)
                {
                    m_wndStandingPosture.m_iSrcX = m_wndStandingPosture.m_iBackgroundWidth -
                        m_wndStandingPosture.m_iSrcWidthOnDragStart;
                }
                if (m_wndStandingPosture.m_iSrcY + m_wndStandingPosture.m_iSrcHeightOnDragStart >=
                    m_wndStandingPosture.m_iBackgroundHeight)
                {
                    m_wndStandingPosture.m_iSrcY = m_wndStandingPosture.m_iBackgroundHeight -
                        m_wndStandingPosture.m_iSrcHeightOnDragStart;
                }

                //m_wndStandingPosture.UpdateOffscreen(temp_Graphics);
                /*        if (GetParent() != NULL)
                        {
                            GetParent()->SendMessage(WM_COMMAND, (WPARAM)GetDlgCtrlID(), (LPARAM)GetSafeHwnd());
                        }
                        UpdateOffscreen();
                        CRect rcClient;
                        GetClientRect(&rcClient);
                        InvalidateRect(&rcClient);
                        UpdateWindow();*/

                m_wndKneedownPosture.m_iSrcX = m_wndKneedownPosture.m_iSrcXOnDragStart - iDiffX * m_wndKneedownPosture.m_iSrcWidthOnDragStart / m_wndKneedownPosture.m_iScreenWidthOnDragStart;
                m_wndKneedownPosture.m_iSrcY = m_wndKneedownPosture.m_iSrcYOnDragStart - iDiffY * m_wndKneedownPosture.m_iSrcHeightOnDragStart / m_wndKneedownPosture.m_iScreenHeightOnDragStart;
                if (m_wndKneedownPosture.m_iSrcX < 0)
                {
                    m_wndKneedownPosture.m_iSrcX = 0;
                }
                if (m_wndKneedownPosture.m_iSrcY < 0)
                {
                    m_wndKneedownPosture.m_iSrcY = 0;
                }
                if (m_wndKneedownPosture.m_iSrcX + m_wndKneedownPosture.m_iSrcWidthOnDragStart >=
                    m_wndKneedownPosture.m_iBackgroundWidth)
                {
                    m_wndKneedownPosture.m_iSrcX = m_wndKneedownPosture.m_iBackgroundWidth -
                        m_wndKneedownPosture.m_iSrcWidthOnDragStart;
                }
                if (m_wndKneedownPosture.m_iSrcY + m_wndKneedownPosture.m_iSrcHeightOnDragStart >=
                    m_wndKneedownPosture.m_iBackgroundHeight)
                {
                    m_wndKneedownPosture.m_iSrcY = m_wndKneedownPosture.m_iBackgroundHeight -
                        m_wndKneedownPosture.m_iSrcHeightOnDragStart;
                }

                {
                    pictureBox_KneeDown.Invalidate();
                    pictureBox_standing.Invalidate();
                }


            }


        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {

            switch (MouseButtons)
            {
                case MouseButtons.Right:

                    if (m_wndStandingPosture.m_iMouseCaptureMode != 0)
                    {
                        return;
                    }
                    m_wndStandingPosture.m_iMouseCaptureMode = 2;
                    m_wndStandingPosture.m_iMousePointXOnDragStart = e.X;
                    m_wndStandingPosture.m_iMousePointYOnDragStart = e.Y;
                    m_wndStandingPosture.m_iScreenWidthOnDragStart = 423;//rcClient.Width;
                    m_wndStandingPosture.m_iScreenHeightOnDragStart = 470;//rcClient.Height;
                    m_wndStandingPosture.m_iSrcXOnDragStart = m_wndStandingPosture.m_iSrcX;
                    m_wndStandingPosture.m_iSrcYOnDragStart = m_wndStandingPosture.m_iSrcY;
                    m_wndStandingPosture.m_iSrcWidthOnDragStart = m_wndStandingPosture.m_iSrcWidth;
                    m_wndStandingPosture.m_iSrcHeightOnDragStart = m_wndStandingPosture.m_iSrcHeight;
                    m_wndStandingPosture.m_iEditPtID = -1;

                    m_wndKneedownPosture.m_iMouseCaptureMode = 2;
                    m_wndKneedownPosture.m_iMousePointXOnDragStart = e.X;
                    m_wndKneedownPosture.m_iMousePointYOnDragStart = e.Y;
                    m_wndKneedownPosture.m_iScreenWidthOnDragStart = 423;//rcClient.Width;
                    m_wndKneedownPosture.m_iScreenHeightOnDragStart = 470;//rcClient.Height;
                    m_wndKneedownPosture.m_iSrcXOnDragStart = m_wndKneedownPosture.m_iSrcX;
                    m_wndKneedownPosture.m_iSrcYOnDragStart = m_wndKneedownPosture.m_iSrcY;
                    m_wndKneedownPosture.m_iSrcWidthOnDragStart = m_wndKneedownPosture.m_iSrcWidth;
                    m_wndKneedownPosture.m_iSrcHeightOnDragStart = m_wndKneedownPosture.m_iSrcHeight;
                    m_wndKneedownPosture.m_iEditPtID = -1;

                    break;
                case MouseButtons.Left:
                    if (m_wndStandingPosture.m_iMouseCaptureMode != 0)
                    {
                        return;
                    }

                    m_wndStandingPosture.m_iEditPtID = m_wndStandingPosture.SearchEditPoint(e.Location, 15 * 15);
                    if (m_wndStandingPosture.m_iEditPtID == -1)
                    {
                        // OutputDebugString("EditPoint = -1 \n");
                        return;
                    }
                    else
                    {
                        char[] szMessage = new char[100];
                        //   wsprintf(&(szMessage[0]), "EditPoint = %d\n", m_iEditPtID);
                        //   OutputDebugString(&(szMessage[0]));
                    }

                    //  SetCapture();
                    m_wndStandingPosture.m_iMouseCaptureMode = 1;


                    // GetClientRect(&rcClient);

                    m_wndStandingPosture.m_iMousePointXOnDragStart = e.X;
                    m_wndStandingPosture.m_iMousePointYOnDragStart = e.Y;
                    m_wndStandingPosture.m_iScreenWidthOnDragStart = 423;//rcClient.Width;
                    m_wndStandingPosture.m_iScreenHeightOnDragStart = 470;//rcClient.Height;
                    m_wndStandingPosture.m_iSrcXOnDragStart = m_wndStandingPosture.m_iSrcX;
                    m_wndStandingPosture.m_iSrcYOnDragStart = m_wndStandingPosture.m_iSrcY;
                    m_wndStandingPosture.m_iSrcWidthOnDragStart = m_wndStandingPosture.m_iSrcWidth;
                    m_wndStandingPosture.m_iSrcHeightOnDragStart = m_wndStandingPosture.m_iSrcHeight;

                    break;
            }
            m_JointEditDoc.SetStandingMarkerTouchFlag(m_wndStandingPosture.GetEditPtID());// Newly added by Meena
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {

                if (m_wndStandingPosture.m_iMouseCaptureMode != 2)
                {
                    return;
                }
                //ReleaseCapture();
                m_wndStandingPosture.m_iMouseCaptureMode = 0;
                m_wndStandingPosture.m_iEditPtID = -1;

                m_wndKneedownPosture.m_iMouseCaptureMode = 0;
                m_wndKneedownPosture.m_iEditPtID = -1;

            }
            else
            {
                if (m_wndStandingPosture.m_iMouseCaptureMode != 1)
                {
                    return;
                }
                //ReleaseCapture();
                m_wndStandingPosture.ExchangeLRData();
                m_wndStandingPosture.m_iMouseCaptureMode = 0;
                m_wndStandingPosture.m_iEditPtID = -1;

            }
            SetEditPointMessage(m_wndStandingPosture.GetEditPtID());

        }

        public static byte[] converterDemo(Image x)
        {
            ImageConverter _imageConverter = new ImageConverter();
            byte[] xByte = (byte[])_imageConverter.ConvertTo(x, typeof(byte[]));
            return xByte;
        }

        void SetAnkleAndHipMode()
        {
            int iBackgroundWidth = m_wndStandingPosture.GetBackgroundWidth();
            int iBackgroundHeight = m_wndStandingPosture.GetBackgroundHeight();
            double dImgMag = 2;

            int iSrcWidth = iBackgroundWidth / 2;
            int iSrcHeight = iBackgroundHeight / 2;
            int iSrcX = iBackgroundWidth / 2 - iSrcWidth / 2;
            int iSrcY = 520;
            m_wndStandingPosture.SetSrcPos(iSrcX, iSrcY);
            m_wndStandingPosture.SetSrcSize(iSrcWidth, iSrcHeight);
            m_wndKneedownPosture.SetSrcPos(iSrcX, iSrcY);
            m_wndKneedownPosture.SetSrcSize(iSrcWidth, iSrcHeight);
            m_wndStandingPosture.SetAnkleAndHipMode();
            m_wndKneedownPosture.SetAnkleAndHipMode();
            IDC_CancelBtn.Visible = true;
            switch (m_JointEditDoc.GetInputMode())
            {
                case Constants.INPUTMODE_NEW:
                    /*m_btnBack.EnableWindow(TRUE);
                    m_btnBack.ShowWindow(SW_SHOW);*/

                    break;
                case Constants.INPUTMODE_MODIFY:
                    /* m_btnBack.EnableWindow(FALSE);
                     m_btnBack.ShowWindow(SW_HIDE);*/
                    //  IDC_CancelBtn.Visible = false; //commented by rohini for GSP-1179
                    backbutton = true; //added by rohini FOR GSP-1179
                    break;
                case Constants.INPUTMODE_NONE:
                default:
                    break;
            }
            /*
                        #if defined(TOYOTA)
                            SetDlgItemText(IDC_MESSAGE, "Drag icons to pelvis, ankles.");
                        #else
                                    SetDlgItemText(IDC_MESSAGE, "Drag icons to belt, ankles.");
                        #endif
                                    SetDlgItemText(IDC_MESSAGE2, "");
            */
            label1.Text = Yugamiru.Properties.Resources.BELT_ANKLE_MODE;//"Drag icons to belt, ankles.";
            m_JointEditDoc.SetJointEditViewMode(Constants.JOINTEDITVIEWMODE_ANKLE_AND_HIP);

            {
                /*      CRect rcClient;
                      m_wndStandingPosture.UpdateOffscreen();
                      m_wndStandingPosture.GetClientRect(&rcClient);
                      m_wndStandingPosture.InvalidateRect(&rcClient);
                      m_wndStandingPosture.UpdateWindow();

                      m_wndKneedownPosture.UpdateOffscreen();
                      m_wndKneedownPosture.GetClientRect(&rcClient);
                      m_wndKneedownPosture.InvalidateRect(&rcClient);
                      m_wndKneedownPosture.UpdateWindow();
                      */
                pictureBox_standing.Invalidate();
                pictureBox_KneeDown.Invalidate();

                // ƒXƒ‰ƒCƒ_[
                /*
                m_sliderScale.SetRange(int(IMG_SCALE_MIN * 100), int(IMG_SCALE_MAX * 100));
                m_sliderScale.SetPos(int(IMG_SCALE_MAX * 100 + IMG_SCALE_MIN * 100 - dImgMag * 100));
                m_sliderScale.SetTic(50);
                */
               IDC_SLIDER1.SetRange((int)(Constants.IMG_SCALE_MIN * 100), (int)(Constants.IMG_SCALE_MAX * 100));
                IDC_SLIDER1.TickFrequency = 50;
                  IDC_SLIDER1.Value = (int)(Constants.IMG_SCALE_MAX * 100 +
                     Constants.IMG_SCALE_MIN * 100 - dImgMag * 100) - 100;
                      
                //IDC_SLIDER1.Value = 200;

            }

        }

        private void JointEditView_Paint(object sender, PaintEventArgs e)
        {
            //m_wndStandingPosture.UpdateOffscreen(e.Graphics);
            //e.Graphics.DrawRectangle(new Pen(Color.Black), 1115, 300, 35, 193);
            //  e.Graphics.DrawImage(Yugamiru.Properties.Resources.Mainpic4, (this.Width - Yugamiru.Properties.Resources.Mainpic4.Width) / 2, 
            //      0, Yugamiru.Properties.Resources.Mainpic4.Width, Yugamiru.Properties.Resources.Mainpic4.Height);
        }

        public void JointEditView_SizeChanged(object sender, EventArgs e)
        {
            //--to centre the picture box while resizing the form
            m_MainPic.Left = (this.ClientSize.Width - m_MainPic.Width) / 2;
            m_MainPic.Top = (this.ClientSize.Height - m_MainPic.Height) / 2;
            
            pictureBox_standing.Size = new Size(423, 470);
            pictureBox_standing.Location = new Point(212, 152 + m_MainPic.Top);
            pictureBox_standing.Left = (this.Width - pictureBox_standing.Width) / 2 - 260;
            pictureBox_KneeDown.Size = new Size(423, 470);
            pictureBox_KneeDown.Location = new Point(670, 152 + m_MainPic.Top);
            pictureBox_KneeDown.Left = pictureBox_standing.Left + pictureBox_standing.Width + 36;

            label1.Top = 40 + m_MainPic.Top;
            label1.Left = m_MainPic.Left + 14;
            label2.Top = 80 + m_MainPic.Top;
            //MessageBox.Show(label1.Left.ToString());
            label2.Left = m_MainPic.Left + 14;
            IDC_Mag1Btn.Size = new Size(48, 48);
            IDC_Mag1Btn.Location = new Point(1110, 250 + m_MainPic.Top);
            IDC_Mag1Btn.Left = pictureBox_KneeDown.Left + pictureBox_KneeDown.Width + 16;

            panel1.Size = new Size(42, 192);
            panel1.Location = new Point(1110, 300 + m_MainPic.Top);
            panel1.Left = pictureBox_KneeDown.Left + pictureBox_KneeDown.Width + 16;


            IDC_SLIDER1.Size = new Size(36, 192);
            //IDC_SLIDER1.Location = new Point(1110, 300);



            IDC_Mag2Btn.Size = new Size(48, 48);
            IDC_Mag2Btn.Location = new Point(1110, 500 + m_MainPic.Top);
            IDC_Mag2Btn.Left = pictureBox_KneeDown.Left + pictureBox_KneeDown.Width + 16;

            IDC_ResetImgBtn.Size = new Size(48, 48);
            IDC_ResetImgBtn.Location = new Point(1110, 550 + m_MainPic.Top);
            IDC_ResetImgBtn.Left = pictureBox_KneeDown.Left + pictureBox_KneeDown.Width + 16;

            IDC_OkBtn.Size = new Size(112, 42);
            IDC_OkBtn.Location = new Point(980, 635 + m_MainPic.Top);
            IDC_OkBtn.Left = pictureBox_KneeDown.Left + pictureBox_KneeDown.Width - IDC_CancelBtn.Width;

            IDC_CancelBtn.Size = new Size(112, 42);
            IDC_CancelBtn.Location = new Point(250, 635 + m_MainPic.Top);
            IDC_CancelBtn.Left = pictureBox_standing.Left;
        }

        void OnPicture1(Graphics g)
        {
            int iSrcX = m_wndStandingPosture.GetSrcX();
            int iSrcY = m_wndStandingPosture.GetSrcY();
            int iSrcWidth = m_wndStandingPosture.GetSrcWidth();
            int iSrcHeight = m_wndStandingPosture.GetSrcHeight();
            m_wndKneedownPosture.SetSrcPos(iSrcX, iSrcY);
            m_wndKneedownPosture.SetSrcSize(iSrcWidth, iSrcHeight);
            m_wndKneedownPosture.UpdateOffscreen(g);
            /*
            CRect rcClient;
            m_wndKneedownPosture.GetClientRect(rcClient);
            m_wndKneedownPosture.InvalidateRect(rcClient);
            m_wndKneedownPosture.UpdateWindow();
            */
            int iScrollPos = (int)(Constants.IMG_SCALE_MAX * 100 +
                Constants.IMG_SCALE_MIN * 100 - m_wndStandingPosture.GetBackgroundWidth() * 100 / iSrcWidth);
            if (iScrollPos < Constants.IMG_SCALE_MIN * 100)
            {
                iScrollPos = (int)(Constants.IMG_SCALE_MIN * 100);
            }
            if (iScrollPos > Constants.IMG_SCALE_MAX * 100)
            {
                iScrollPos = (int)(Constants.IMG_SCALE_MAX * 100);
            }
            //m_sliderScale.SetPos(iScrollPos);


            SetEditPointMessage(m_wndStandingPosture.GetEditPtID());
            m_JointEditDoc.SetStandingMarkerTouchFlag(m_wndStandingPosture.GetEditPtID());

        }
        void SetEditPointMessage(int iEditPointID)
        {

            switch (iEditPointID)
            {
                case -1:
                    label2.Text = "";
                    break;
                case 0:
                    label2.Text = "This icon should be positioned on hip.";
                    break;
                case 1:
                    label2.Text = "This icon should be positioned on hip.";
                    break;
                case 2:     //@•G
                    label2.Text = Yugamiru.Properties.Resources.KNEE_POSITION;//"This icon should be positioned on knee ( center of Patella ).";
                    break;
                case 3:     //@•G
                    label2.Text = Yugamiru.Properties.Resources.KNEE_POSITION;//"This icon should be positioned on knee ( center of Patella ).";
                    break;
                case 4:     //@‘«Žñ
                    label2.Text = Yugamiru.Properties.Resources.ANKLE_POSITION;//"This icon should be positioned on ankle.";
                    break;
                case 5:     //@‘«Žñ
                    label2.Text = Yugamiru.Properties.Resources.ANKLE_POSITION;//"This icon should be positioned on ankle.";
                    break;
                case 6:     //@ƒxƒ‹ƒg
                            /*#if defined(TOYOTA)
                                    if ( m_nStage == 4 ){
                                        SetDlgItemText(IDC_MESSAGE2,  "This icon should be positioned on the back of pelvis.");
                                    }
                                    else{
                                        SetDlgItemText(IDC_MESSAGE2,  "This icon should be positioned on the process of pelvis." );
                                    }
                            #else*/
                    label2.Text = Yugamiru.Properties.Resources.BELT_POSITION;//"This icon should be positioned on end of belt.";
                                                                              //#endif
                    break;
                case 7:     //@ƒxƒ‹ƒg
                            /*#if defined(TOYOTA)
                                    if ( m_nStage == 4 ){
                                        SetDlgItemText(IDC_MESSAGE2,  "This icon should be positioned on the front of pelvis.");		
                                    }
                                    else{
                                        SetDlgItemText(IDC_MESSAGE2,  "This icon should be positioned on the process of pelvis.");		
                                    }
                            #else*/
                    label2.Text = Yugamiru.Properties.Resources.BELT_POSITION;//"This icon should be positioned on end of belt.";

                    break;
                case 8:     //@Œ¨
                            /*#if defined(TOYOTA)
                                    SetDlgItemText(IDC_MESSAGE2,  "This icon should be positioned on shoulder.");
                            #else*/
                    label2.Text = Yugamiru.Properties.Resources.SHOULDER_POSITION;//"This icon should be positioned on contour of shoulder.";
                                                                                  //#endif
                    break;
                case 9:     //@Œ¨
                            /*#if defined(TOYOTA)
                                    SetDlgItemText(IDC_MESSAGE2,  "This icon should be positioned on shoulder.");
                            #else*/
                    label2.Text = Yugamiru.Properties.Resources.SHOULDER_POSITION; //"This icon should be positioned on contour of shoulder.";
                                                                                   //#endif
                    break;
                case 10:    //@Ž¨
                    label2.Text = Yugamiru.Properties.Resources.EAR_POSITION;//"This icon should be positioned on contour of ear.";
                    break;
                case 11:    //@Ž¨
                    label2.Text = Yugamiru.Properties.Resources.EAR_POSITION;//"This icon should be positioned on contour of ear.";
                    break;
                case 12:    //@Š{
                    label2.Text = Yugamiru.Properties.Resources.CHIN_POSITION;//"This icon should be positioned on contour of chin.";
                    break;
                case 13:    //@”ûŠÔ
                    label2.Text = Yugamiru.Properties.Resources.GLABELLA_POSITION;//"This icon should be positioned on glabella.";
                    break;

                case 14:
                    label2.Text = "TALON.";
                    break;
                case 15:
                    label2.Text = "FOOT CURVE.";
                    break;

                default:
                    label2.Text = "";
                    break;
            }
        }

        private void pictureBox_KneeDown_Paint(object sender, PaintEventArgs e)
        {
            /*
            SolidBrush penred = new SolidBrush(Color.Red);
            e.Graphics.FillEllipse(penred, 275, 72,5,5);
            e.Graphics.FillEllipse(penred, 285, 350, 5, 5);
            e.Graphics.FillEllipse(penred, 402, 350, 5, 5);
            */
            //MessageBox.Show("picturebox2");
            m_wndKneedownPosture.m_pbyteBits = m_JointEditDoc.m_FrontKneedownImageBytes;
            m_wndKneedownPosture.UpdateOffscreen(e.Graphics);
            SetEditPointMessage(m_wndKneedownPosture.GetEditPtID());
            //m_JointEditDoc.SetStandingMarkerTouchFlag(m_wndKneedownPosture.GetEditPtID());
        }

        private void JointEditView_Load(object sender, EventArgs e)
        {

        }
        //added by rohini start GSP-1179
        public event EventHandler EventToChangeResultView; // creating event handler - step1
        public void FunctionToChangeResultView(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = EventToChangeResultView;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }

        //added by rohinni end GSP-1179

        private void IDC_CancelBtn_Click(object sender, EventArgs e)
        {
            m_Clickmode = "RETURN";
            IDC_CancelBtn.Image = Yugamiru.Properties.Resources.gobackred_down;
            t.Interval = 100;
            //MessageBox.Show("hi");
            t.Tick += new EventHandler(timer_tick);
            t.Start();

            ///added by rohini for GSP-1179 --- START
            if (backbutton == true)
            {
                t.Stop();
                this.Visible = false;

                FunctionToChangeResultView(EventArgs.Empty);

            }
            else
            {
                ///added by rohini for GSP-1179 --- END

                switch (m_JointEditDoc.GetJointEditViewMode())
                {
                    case Constants.JOINTEDITVIEWMODE_ANKLE_AND_HIP:
                        m_JointEditDoc.SetMeasurementStartViewMode(Constants.MEASUREMENTSTARTVIEWMODE_FRONT_KNEEDOWN);
                        m_JointEditDoc.SetJointEditViewMode(Constants.JOINTEDITVIEWMODE_NONE);
                        //m_JointEditDoc.SetInputMode(Constants.INPUTMODE_MODIFY);
                        //m_JointEditDoc.ChangeToMeasurementStartView();
                        /* Function_CloseBeltAnkleForm(EventArgs.Empty);
                         this.Close();
                         DisposeControls();*/
                        this.Visible = false;
                        m_JointEditDoc.GetMeasurementStart().Visible = true;
                        m_JointEditDoc.GetMeasurementStart().RefreshForm();
                        break;
                    case Constants.JOINTEDITVIEWMODE_KNEE:
                        SetAnkleAndHipMode();
                        break;
                    case Constants.JOINTEDITVIEWMODE_UPPERBODY:
                        SetKneeMode();
                        break;
                    case Constants.JOINTEDITVIEWMODE_SIDE:
                    case Constants.JOINTEDITVIEWMODE_NONE:
                    default:
                        break;
                }
                IDC_OkBtn.Image = Yugamiru.Properties.Resources.gonextgreen_up;
                IDC_CancelBtn.Image = Yugamiru.Properties.Resources.gobackred_on;
            }
        }
        public event EventHandler closeBeltAnkleForm; // creating event handler - step1
        public void Function_CloseBeltAnkleForm(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = closeBeltAnkleForm;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }

        public int GetUntouchedMarkerWarningAnkleAndHipMode(ref String strWarning)
        {
            if (m_JointEditDoc.GetInputMode() != Constants.INPUTMODE_NEW)
            {
                return 0;
            }
            string strPart = string.Empty;
            if ((!(m_JointEditDoc.GetStandingMarkerTouchFlag(Constants.BELT1))) ||
                (!(m_JointEditDoc.GetStandingMarkerTouchFlag(Constants.BELT2))))
            {
                strPart += Yugamiru.Properties.Resources.BELT_STANDING;//"belt(standing) ";
            }
            if (!(m_JointEditDoc.GetStandingMarkerTouchFlag(Constants.RIGHT_ANKLE)))
            {
                strPart += Yugamiru.Properties.Resources.RIGHTANKLE_STANDING;//"right ankle(standing) ";

            }
            if (!(m_JointEditDoc.GetStandingMarkerTouchFlag(Constants.LEFT_ANKLE)))
            {
                strPart += Yugamiru.Properties.Resources.LEFTANKLE_STANDING;//"left ankle(standing) ";

            }
            if ((!(m_JointEditDoc.GetKneedownMarkerTouchFlag(Constants.BELT1))) ||
                (!(m_JointEditDoc.GetKneedownMarkerTouchFlag(Constants.BELT2))))
            {
                strPart += Yugamiru.Properties.Resources.BELT_KNEEDOWN;//"belt(kneedown) ";

            }
            if (!(m_JointEditDoc.GetKneedownMarkerTouchFlag(Constants.RIGHT_ANKLE)))
            {
                strPart += Yugamiru.Properties.Resources.RIGHTANKLE_KNEEDOWN;//"right ankle(kneedown) ";

            }
            if (!(m_JointEditDoc.GetKneedownMarkerTouchFlag(Constants.LEFT_ANKLE)))
            {
                strPart += Yugamiru.Properties.Resources.LEFTANKLE_KNEEDOWN;//"left ankle(kneedown)";

            }
            if (strPart == string.Empty)
            {
                return 0;
            }
            strWarning = Yugamiru.Properties.Resources.WARNING4/*"There are unmoved icons. proceed anyway?*/+ "\n" + strPart;

            return 1;
        }
        static string m_Clickmode = "NONE";
        private void IDC_OkBtn_Click(object sender, EventArgs e)
        {
            backbutton = false; ///ADDED BY ROHINI FOR GSP-1179
            m_Clickmode = "NEXT";
            IDC_OkBtn.Image = Yugamiru.Properties.Resources.gonextred_down;
            
            t.Interval = 100;
            t.Tick += new EventHandler(timer_tick);
            t.Start();
            
            string strWarning = string.Empty;
            switch (m_JointEditDoc.GetJointEditViewMode())
            {
                case Constants.JOINTEDITVIEWMODE_ANKLE_AND_HIP:
                    {
                        if (GetUntouchedMarkerWarningAnkleAndHipMode(ref strWarning) == 1)
                        {
                            
                            DialogResult result = MessageBox.Show(strWarning, "JointEdit", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (result == DialogResult.No)
                            {
                                //t.Stop();
                                //IDC_OkBtn.Image = Yugamiru.Properties.Resources.gonextred_on;
                                return;
                            }
                            //CalcBelt2Hip(); // by meena on 4th April (to fix misplacement of markers)
                        }
                        CalcBelt2Hip();
                        SetKneeMode();
                        //m_JointEditDoc.m_FrontBodyPositionStanding = m_FrontBodyPositionStanding;

                    }
                    break;
                case Constants.JOINTEDITVIEWMODE_KNEE:
                    {
                        if (GetUntouchedMarkerWarningKneeMode(ref strWarning) == 1)
                        {
                            DialogResult result = MessageBox.Show(strWarning, "JointEdit", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (result == DialogResult.No)
                            {
                                t.Stop();
                                IDC_OkBtn.Image = Yugamiru.Properties.Resources.gonextred_on;
                                return;
                            }
                        }
                        SetUpperBodyMode();
                    }
                    break;
                case Constants.JOINTEDITVIEWMODE_UPPERBODY:
                    {
                        if (GetUntouchedMarkerWarningUpperBodyMode(ref strWarning) == 1)
                        {
                            DialogResult result = MessageBox.Show(strWarning, "JointEdit", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (result == DialogResult.No)
                            {
                                t.Stop();
                                IDC_OkBtn.Image = Yugamiru.Properties.Resources.gonextred_on;
                                return;
                            }
                        }
                        FrontBodyPosition FrontBodyPositionStanding = new FrontBodyPosition();
                        m_wndStandingPosture.GetFrontBodyPosition(ref FrontBodyPositionStanding);
                        FrontBodyPositionStanding.SetAllPositionDetected();
                        m_JointEditDoc.SetStandingFrontBodyPosition(FrontBodyPositionStanding);
                        m_JointEditDoc.SetStandingFrontBodyPositionObj(FrontBodyPositionStanding);


                        FrontBodyPosition FrontBodyPositionKneedown = new FrontBodyPosition();
                        m_wndKneedownPosture.GetFrontBodyPosition(ref FrontBodyPositionKneedown);
                        FrontBodyPositionKneedown.SetAllPositionDetected();
                        m_JointEditDoc.SetKneedownFrontBodyPosition(FrontBodyPositionKneedown);
                        m_JointEditDoc.SetKneedownFrontBodyPositionObj(FrontBodyPositionKneedown);

                        m_JointEditDoc.SetJointEditViewMode(Constants.JOINTEDITVIEWMODE_SIDE);
                        //m_JointEditDoc.ChangeToSideJointEditView();
                        /*Function_StartSideJointEditView(EventArgs.Empty);
                        this.Close();
                        DisposeControls();*/
                        this.Visible = false;
                        m_JointEditDoc.GetSideJointEditView().Visible = true;
                        m_JointEditDoc.GetSideJointEditView().RefreshForms();
                    }
                    break;
                case Constants.JOINTEDITVIEWMODE_SIDE:
                case Constants.JOINTEDITVIEWMODE_NONE:
                default:
                    break;
            }
            IDC_OkBtn.Image = Yugamiru.Properties.Resources.gonextred_on;
            IDC_CancelBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
        }
        public void SetUpperBodyMode()
        {
            int iBackgroundWidth = m_wndStandingPosture.GetBackgroundWidth();
            int iBackgroundHeight = m_wndStandingPosture.GetBackgroundHeight();
            double dImgMag = 3;
            int iSrcWidth = iBackgroundWidth / 3;
            int iSrcHeight = iBackgroundHeight / 3;
            int iSrcX = iBackgroundWidth / 2 - iSrcWidth / 2;
            int iSrcY = 130;
            m_wndStandingPosture.SetSrcPos(iSrcX, iSrcY);
            m_wndStandingPosture.SetSrcSize(iSrcWidth, iSrcHeight);
            m_wndKneedownPosture.SetSrcPos(iSrcX, iSrcY);
            m_wndKneedownPosture.SetSrcSize(iSrcWidth, iSrcHeight);
            m_wndStandingPosture.SetUpperBodyMode();
            m_wndKneedownPosture.SetUpperBodyMode();
            IDC_CancelBtn.Visible = true;
            switch (m_JointEditDoc.GetInputMode())
            {
                case Constants.INPUTMODE_NEW:
                    /*m_btnBack.EnableWindow(TRUE);
                    m_btnBack.ShowWindow(SW_SHOW);*/

                    break;
                case Constants.INPUTMODE_MODIFY:
                    /* m_btnBack.EnableWindow(FALSE);
                     m_btnBack.ShowWindow(SW_HIDE);*/
                    IDC_CancelBtn.Image = Properties.Resources.gobackgreen_up;
                    break;
                case Constants.INPUTMODE_NONE:
                default:
                    break;
            }
            label1.Text = Yugamiru.Properties.Resources.SHOULDER_EARS_MODE;//"Drag icons to shoulders, ears, etc.";
                                                                           /* m_btnBack.EnableWindow(TRUE);
                                                                            m_btnBack.ShowWindow(SW_SHOW);
                                                                            SetDlgItemText(IDC_MESSAGE, "Drag icons to shoulders, ears, etc.");
                                                                            SetDlgItemText(IDC_MESSAGE2, "");
                                                                            */

            m_JointEditDoc.SetJointEditViewMode(Constants.JOINTEDITVIEWMODE_UPPERBODY);

            {
                /*  CRect rcClient;
                  m_wndStandingPosture.UpdateOffscreen();
                  m_wndStandingPosture.GetClientRect(&rcClient);
                  m_wndStandingPosture.InvalidateRect(&rcClient);
                  m_wndStandingPosture.UpdateWindow();

                  m_wndKneedownPosture.UpdateOffscreen();
                  m_wndKneedownPosture.GetClientRect(&rcClient);
                  m_wndKneedownPosture.InvalidateRect(&rcClient);
                  m_wndKneedownPosture.UpdateWindow();
                  */
                pictureBox_standing.Invalidate();
                pictureBox_KneeDown.Invalidate();

                // ƒXƒ‰ƒCƒ_[
                IDC_SLIDER1.SetRange((int)(Constants.IMG_SCALE_MIN * 100), (int)(Constants.IMG_SCALE_MAX * 100));
                IDC_SLIDER1.TickFrequency = 50;
                IDC_SLIDER1.Value = (int)(Constants.IMG_SCALE_MAX * 100 +
                    Constants.IMG_SCALE_MIN * 100 - dImgMag * 100) + 100;
                /*
                m_sliderScale.SetRange(int(IMG_SCALE_MIN * 100), int(IMG_SCALE_MAX * 100));
                m_sliderScale.SetPos(int(IMG_SCALE_MAX * 100 + IMG_SCALE_MIN * 100 - dImgMag * 100));
                m_sliderScale.SetTic(50);
                */
            }
        }
        public void SetKneeMode()
        {
            int iBackgroundWidth = m_wndStandingPosture.GetBackgroundWidth();
            int iBackgroundHeight = m_wndStandingPosture.GetBackgroundHeight();
            double dImgMag = 3;
            int iSrcWidth = iBackgroundWidth / 3;
            int iSrcHeight = iBackgroundHeight / 3;
            int iSrcX = iBackgroundWidth / 2 - iSrcWidth / 2;
            int iSrcY = 750;
            m_wndStandingPosture.SetSrcPos(iSrcX, iSrcY);
            m_wndStandingPosture.SetSrcSize(iSrcWidth, iSrcHeight);
            m_wndKneedownPosture.SetSrcPos(iSrcX, iSrcY);
            m_wndKneedownPosture.SetSrcSize(iSrcWidth, iSrcHeight);
            m_wndStandingPosture.SetKneeMode();
            m_wndKneedownPosture.SetKneeMode();
            IDC_CancelBtn.Visible = true;
            switch (m_JointEditDoc.GetInputMode())
            {
                case Constants.INPUTMODE_NEW:
                    /*m_btnBack.EnableWindow(TRUE);
                    m_btnBack.ShowWindow(SW_SHOW);*/

                    break;
                case Constants.INPUTMODE_MODIFY:
                    /* m_btnBack.EnableWindow(FALSE);
                     m_btnBack.ShowWindow(SW_HIDE);*/
                    IDC_CancelBtn.Image = Properties.Resources.gobackgreen_up;
                    break;
                case Constants.INPUTMODE_NONE:
                default:
                    break;
            }
            label1.Text = Yugamiru.Properties.Resources.KNEES_MODE;//"Drag icons to knees.";
                                                                   /*
                                                                   m_btnBack.EnableWindow(TRUE);
                                                                   m_btnBack.ShowWindow(SW_SHOW);
                                                                   SetDlgItemText(IDC_MESSAGE, "Drag icons to knees.");
                                                                   SetDlgItemText(IDC_MESSAGE2, "");
                                                                   */
            m_JointEditDoc.SetJointEditViewMode(Constants.JOINTEDITVIEWMODE_KNEE);


            {
                /*      CRect rclient;
                      m_wndStandingPosture.UpdateOffscreen();
                      m_wndStandingPosture.GetClientRect(&rcClient);
                      m_wndStandingPosture.InvalidateRect(&rcClient);
                      m_wndStandingPosture.UpdateWindow();

                      m_wndKneedownPosture.UpdateOffscreen();
                      m_wndKneedownPosture.GetClientRect(&rcClient);
                      m_wndKneedownPosture.InvalidateRect(&rcClient);
                      m_wndKneedownPosture.UpdateWindow();*/

                pictureBox_standing.Invalidate();
                pictureBox_KneeDown.Invalidate();

                // ƒXƒ‰ƒCƒ_[
                IDC_SLIDER1.SetRange((int)(Constants.IMG_SCALE_MIN * 100), (int)(Constants.IMG_SCALE_MAX * 100));


                IDC_SLIDER1.TickFrequency = 50;
                IDC_SLIDER1.Value = (int)(Constants.IMG_SCALE_MAX * 100 +
                    Constants.IMG_SCALE_MIN * 100 - dImgMag * 100) + 100;

                /*
            m_sliderScale.SetRange(int(IMG_SCALE_MIN * 100), int(IMG_SCALE_MAX * 100));
            m_sliderScale.SetPos(int(IMG_SCALE_MAX * 100 + IMG_SCALE_MIN * 100 - dImgMag * 100));
            m_sliderScale.SetTic(50);
            */
            }
        }

        public int GetUntouchedMarkerWarningUpperBodyMode(ref String strWarning)
        {
            if (m_JointEditDoc.GetInputMode() != Constants.INPUTMODE_NEW)
            {
                return 0;
            }
            string strPart = string.Empty;
            if (!(m_JointEditDoc.GetStandingMarkerTouchFlag(Constants.RIGHT_SHOULDER)))
            {
                strPart += Yugamiru.Properties.Resources.RIGHTSHOULDER_STANDING;//"right shoulder(standing) ";

            }
            if (!(m_JointEditDoc.GetStandingMarkerTouchFlag(Constants.LEFT_SHOULDER)))
            {
                strPart += Yugamiru.Properties.Resources.LEFTSHOULDER_STANDING;//"left shoulder(standing) ";
            }
            if (!(m_JointEditDoc.GetStandingMarkerTouchFlag(Constants.RIGHT_EAR)))
            {
                strPart += Yugamiru.Properties.Resources.RIGHTEAR_STANDING; //"right ear(standing) ";
            }
            if (!(m_JointEditDoc.GetStandingMarkerTouchFlag(Constants.LEFT_EAR)))
            {
                strPart += Yugamiru.Properties.Resources.LEFTEAR_STANDING;//"left ear(standing) ";

            }
            if (!(m_JointEditDoc.GetStandingMarkerTouchFlag(Constants.CHIN)))
            {
                strPart += Yugamiru.Properties.Resources.CHIN_STANDING;//"chin(standing) ";

            }
            if (!(m_JointEditDoc.GetStandingMarkerTouchFlag(Constants.MIDDLE_EYEBROWS)))
            {
                strPart += Yugamiru.Properties.Resources.GLABELLA_STANDING;//"glabella(standing) ";

            }
            if (!(m_JointEditDoc.GetKneedownMarkerTouchFlag(Constants.RIGHT_SHOULDER)))
            {
                strPart += Yugamiru.Properties.Resources.RIGHTSHOULDER_KWN; //"right shoulder(kneedown) ";

            }
            if (!(m_JointEditDoc.GetKneedownMarkerTouchFlag(Constants.LEFT_SHOULDER)))
            {
                strPart += Yugamiru.Properties.Resources.LEFTSHOULDER_KWN;// "left shoulder(kneedown) ";
            }
            if (!(m_JointEditDoc.GetKneedownMarkerTouchFlag(Constants.RIGHT_EAR)))
            {
                strPart += Yugamiru.Properties.Resources.RIGHTEAR_KWN;// "right ear(kneedown) ";
            }
            if (!(m_JointEditDoc.GetKneedownMarkerTouchFlag(Constants.LEFT_EAR)))
            {
                strPart += Yugamiru.Properties.Resources.LEFTEAR_KWN;//"left ear(kneedown) ";

            }
            if (!(m_JointEditDoc.GetKneedownMarkerTouchFlag(Constants.CHIN)))
            {
                strPart += Yugamiru.Properties.Resources.CHIN_KWN;//"chin(kneedown) ";

            }
            if (!(m_JointEditDoc.GetKneedownMarkerTouchFlag(Constants.MIDDLE_EYEBROWS)))
            {
                strPart += Yugamiru.Properties.Resources.GLABELLA_KWN;//"glabella(kneedown) ";

            }
            if (strPart == string.Empty)
            {
                return 0;
            }
            strWarning = /*"There are unmoved icons. proceed anyway?*/ Yugamiru.Properties.Resources.WARNING4 + "\n" + strPart;
            return 1;
        }

        public int GetUntouchedMarkerWarningKneeMode(ref string strWarning)
        {
            if (m_JointEditDoc.GetInputMode() != Constants.INPUTMODE_NEW)
            {
                return 0;
            }
            string strPart = string.Empty;
            if (!(m_JointEditDoc.GetStandingMarkerTouchFlag(Constants.RIGHT_KNEE)))
            {
                strPart += Yugamiru.Properties.Resources.RIGHTKNEE_STANDING;//"right knee(standing) ";
            }
            if (!(m_JointEditDoc.GetStandingMarkerTouchFlag(Constants.LEFT_KNEE)))
            {
                strPart += Yugamiru.Properties.Resources.LEFTKNEE_STANDING;//"left knee(standing) ";

            }
            if (!(m_JointEditDoc.GetKneedownMarkerTouchFlag(Constants.RIGHT_KNEE)))
            {
                strPart += Yugamiru.Properties.Resources.RIGHTKNEE_KNEEDOWN;//"right knee(kneedown) ";
            }
            if (!(m_JointEditDoc.GetKneedownMarkerTouchFlag(Constants.LEFT_KNEE)))
            {
                strPart += Yugamiru.Properties.Resources.LEFTKNEE_KNEEDOWN;//"left knee(kneedown)";

            }
            if (strPart == string.Empty)
            {
                return 0;
            }
            strWarning = /*"There are unmoved icons. proceed anyway?*/ Yugamiru.Properties.Resources.WARNING4 + "\n" + strPart;
            return 1;
        }

        public void CalcBelt2Hip()
        {
            // ‚Ü‚¸AƒEƒBƒ“ƒhƒE“à‚Å•ÒW‚µ‚½ŠÖßÀ•W‚ÆAƒhƒLƒ…ƒƒ“ƒg‚ÉŠi”[‚³‚ê‚Ä‚¢‚éŠÖßÀ•W‚ðŽæ“¾‚·‚é.
            FrontBodyPosition FrontBodyPositionStandingDocument = new FrontBodyPosition();
            m_JointEditDoc.GetStandingFrontBodyPosition(ref FrontBodyPositionStandingDocument);
            FrontBodyPosition FrontBodyPositionStandingWnd = new FrontBodyPosition();
            m_wndStandingPosture.GetFrontBodyPosition(ref FrontBodyPositionStandingWnd);

            FrontBodyPosition FrontBodyPositionKneedownDocument = new FrontBodyPosition();
            m_JointEditDoc.GetKneedownFrontBodyPosition(ref FrontBodyPositionKneedownDocument);
            FrontBodyPosition FrontBodyPositionKneedownWnd = new FrontBodyPosition();
            m_wndKneedownPosture.GetFrontBodyPosition(ref FrontBodyPositionKneedownWnd);

            // —§ˆÊ‚É‚¨‚¯‚éƒxƒ‹ƒg‚ÌˆÊ’u‚ÉŠî‚Ã‚«A—§ˆÊ‚É‚¨‚¯‚éŒÒŠÖßˆÊ’u‚ðŒvŽZ‚·‚é.
            FrontBodyPositionStandingWnd.CalcBeltPrepare(
                m_wndStandingPosture.GetBackgroundWidth(),
                m_wndStandingPosture.GetBackgroundHeight(),
                m_JointEditDoc.GetHipMko(),
                m_JointEditDoc.GetBelt2HipRatio());

            // —§ˆÊ‚É‚¨‚¯‚éƒxƒ‹ƒgAæùAŒÒŠÖßˆÊ’u‚ðŠi”[—pƒf[ƒ^‚É”½‰f‚·‚é.
            FrontBodyPositionStandingDocument.CopYRightBeltPosition(FrontBodyPositionStandingWnd);
            FrontBodyPositionStandingDocument.CopYLeftBeltPosition(FrontBodyPositionStandingWnd);
            FrontBodyPositionStandingDocument.CopYRightHipPosition(FrontBodyPositionStandingWnd);
            FrontBodyPositionStandingDocument.CopYLeftHipPosition(FrontBodyPositionStandingWnd);
            FrontBodyPositionStandingDocument.CopYRightAnklePosition(FrontBodyPositionStandingWnd);
            FrontBodyPositionStandingDocument.CopYLeftAnklePosition(FrontBodyPositionStandingWnd);
            FrontBodyPositionStandingDocument.SetUnderBodYPositionDetected();

            // ‹üˆÊ‚É‚¨‚¯‚éƒxƒ‹ƒg‚ÌˆÊ’u‚ÉŠî‚Ã‚«A‹üˆÊ‚É‚¨‚¯‚éŒÒŠÖßˆÊ’u‚ðŒvŽZ‚·‚é.
            FrontBodyPositionKneedownWnd.CalcBeltPrepare(
                m_wndKneedownPosture.GetBackgroundWidth(),
                m_wndKneedownPosture.GetBackgroundHeight(),
                m_JointEditDoc.GetHipMko(),
                m_JointEditDoc.GetBelt2HipRatio());

            // ‹üˆÊ‚É‚¨‚¯‚éƒxƒ‹ƒgAæùAŒÒŠÖßˆÊ’u‚ðŠi”[—pƒf[ƒ^‚É”½‰f‚·‚é.
            FrontBodyPositionKneedownDocument.CopYRightBeltPosition(FrontBodyPositionKneedownWnd);
            FrontBodyPositionKneedownDocument.CopYLeftBeltPosition(FrontBodyPositionKneedownWnd);
            FrontBodyPositionKneedownDocument.CopYRightHipPosition(FrontBodyPositionKneedownWnd);
            FrontBodyPositionKneedownDocument.CopYLeftHipPosition(FrontBodyPositionKneedownWnd);
            FrontBodyPositionKneedownDocument.CopYRightAnklePosition(FrontBodyPositionKneedownWnd);
            FrontBodyPositionKneedownDocument.CopYLeftAnklePosition(FrontBodyPositionKneedownWnd);
            FrontBodyPositionKneedownDocument.SetUnderBodYPositionDetected();

            if (!(FrontBodyPositionStandingDocument.IsKneePositionDetected()))
            {
                // —§ˆÊ•G‚ÌˆÊ’u‚ª‚Ü‚¾Œˆ’è‚³‚ê‚Ä‚¢‚È‚¢ê‡‚ÍA.
                // ‚±‚ê‚Ü‚Å“ü—Í‚³‚ê‚½ŠÖß“_‚ÌÀ•W‚ð‚à‚Æ‚ÉŒvŽZ‚·‚é.
                FrontBodyPositionStandingDocument.CalcKneePosition(
                    m_wndStandingPosture.GetBackgroundWidth(),
                    m_wndStandingPosture.GetBackgroundHeight(),
                    m_JointEditDoc.GetBelt2KneeRatio(),
                    m_JointEditDoc.GetKneeRatioError());
                // ƒEƒBƒ“ƒhƒE‚É‚¨‚¯‚é•\Ž¦ˆÊ’u‚àXV‚·‚é.
                FrontBodyPositionStandingWnd.CopYRightKneePosition(FrontBodyPositionStandingDocument);
                FrontBodyPositionStandingWnd.CopYLeftKneePosition(FrontBodyPositionStandingDocument);
            }

            if (!(FrontBodyPositionKneedownDocument.IsKneePositionDetected()))
            {
                // ‹üˆÊ•G‚ÌˆÊ’u‚ª‚Ü‚¾Œˆ’è‚³‚ê‚Ä‚¢‚È‚¢ê‡‚ÍA.
                // ‚±‚ê‚Ü‚Å“ü—Í‚³‚ê‚½ŠÖß“_‚ÌÀ•W‚ð‚à‚Æ‚ÉŒvŽZ‚·‚é.
                FrontBodyPositionKneedownDocument.CalcKneePosition(
                    m_wndKneedownPosture.GetBackgroundWidth(),
                    m_wndKneedownPosture.GetBackgroundHeight(),
                    m_JointEditDoc.GetBelt2KneeRatio(),
                    m_JointEditDoc.GetKneeRatioError());
                // ƒEƒBƒ“ƒhƒE‚É‚¨‚¯‚é•\Ž¦ˆÊ’u‚àXV‚·‚é.
                FrontBodyPositionKneedownWnd.CopYRightKneePosition(FrontBodyPositionKneedownDocument);
                FrontBodyPositionKneedownWnd.CopYLeftKneePosition(FrontBodyPositionKneedownDocument);
            }

            if (!(FrontBodyPositionStandingDocument.IsUpperBodyPositionDetected()))
            {
                // —§ˆÊã”¼g‚ÌˆÊ’u‚ª‚Ü‚¾Œˆ’è‚³‚ê‚Ä‚¢‚È‚¢ê‡‚ÍA.
                // ‚±‚ê‚Ü‚Å“ü—Í‚³‚ê‚½ŠÖß“_‚ÌÀ•W‚ð‚à‚Æ‚ÉŒvŽZ‚·‚é.
                FrontBodyPositionStandingDocument.CalcUpperPosition(
                    m_wndStandingPosture.GetBackgroundWidth(),
                    m_wndStandingPosture.GetBackgroundHeight(),
                    0.85, 1.1, 1.0, 1.2);
                // ƒEƒBƒ“ƒhƒE‚É‚¨‚¯‚é•\Ž¦ˆÊ’u‚àXV‚·‚é.
                FrontBodyPositionStandingWnd.CopYRightShoulderPosition(FrontBodyPositionStandingDocument);
                FrontBodyPositionStandingWnd.CopYLeftShoulderPosition(FrontBodyPositionStandingDocument);
                FrontBodyPositionStandingWnd.CopYRightEarPosition(FrontBodyPositionStandingDocument);
                FrontBodyPositionStandingWnd.CopYLeftEarPosition(FrontBodyPositionStandingDocument);
                FrontBodyPositionStandingWnd.CopyChinPosition(FrontBodyPositionStandingDocument);
                FrontBodyPositionStandingWnd.CopYGlabellaPosition(FrontBodyPositionStandingDocument);
            }

            if (!(FrontBodyPositionKneedownDocument.IsUpperBodyPositionDetected()))
            {
                // ‹üˆÊã”¼g‚ÌˆÊ’u‚ª‚Ü‚¾Œˆ’è‚³‚ê‚Ä‚¢‚È‚¢ê‡‚ÍA
                // ‚±‚ê‚Ü‚Å“ü—Í‚³‚ê‚½ŠÖß“_‚ÌÀ•W‚ð‚à‚Æ‚ÉŒˆ’è‚·‚é.
                FrontBodyPositionKneedownDocument.CalcUpperPosition(
                    m_wndKneedownPosture.GetBackgroundWidth(),
                    m_wndKneedownPosture.GetBackgroundHeight(),
                    0.75, 1.0, 0.9, 1.1);
                // ƒEƒBƒ“ƒhƒE‚É‚¨‚¯‚é•\Ž¦ˆÊ’u‚àXV‚·‚é.
                FrontBodyPositionKneedownWnd.CopYRightShoulderPosition(FrontBodyPositionKneedownDocument);
                FrontBodyPositionKneedownWnd.CopYLeftShoulderPosition(FrontBodyPositionKneedownDocument);
                FrontBodyPositionKneedownWnd.CopYRightEarPosition(FrontBodyPositionKneedownDocument);
                FrontBodyPositionKneedownWnd.CopYLeftEarPosition(FrontBodyPositionKneedownDocument);
                FrontBodyPositionKneedownWnd.CopyChinPosition(FrontBodyPositionKneedownDocument);
                FrontBodyPositionKneedownWnd.CopYGlabellaPosition(FrontBodyPositionKneedownDocument);
            }
            FrontBodyPositionStandingDocument.DetectKneePos(
                m_wndStandingPosture.GetBackgroundWidth(),
                m_wndStandingPosture.GetBackgroundHeight(),
                m_JointEditDoc.GetBelt2KneeRatio() + 0.0);
            FrontBodyPositionKneedownDocument.DetectKneePos(
                m_wndKneedownPosture.GetBackgroundWidth(),
                m_wndKneedownPosture.GetBackgroundHeight(),
                m_JointEditDoc.GetBelt2KneeRatio() + m_JointEditDoc.GetKneeRatioError());

            m_JointEditDoc.SetStandingFrontBodyPosition(FrontBodyPositionStandingDocument);
            m_JointEditDoc.SetKneedownFrontBodyPosition(FrontBodyPositionKneedownDocument);
            m_wndStandingPosture.SetFrontBodyPosition(FrontBodyPositionStandingWnd);
            m_wndKneedownPosture.SetFrontBodyPosition(FrontBodyPositionKneedownWnd);
        }

        private void IDC_Mag2Btn_Click(object sender, EventArgs e)//ZoomIn button
        {
            /*  int scale = m_sliderScale.GetPos();
              m_sliderScale.SetPos(scale + 50);
              UpdatePictureScale(scale + 50);*/

            m_ZoomFlag = Constants.ZOOMIN;
            timer1.Interval = 100;
            timer1.Start();
            IDC_Mag2Btn.Image = Yugamiru.Properties.Resources.mag1_down;

            int scale = IDC_SLIDER1.Value;
            if (IDC_SLIDER1.Value <= 100)
                return;
            if ((scale - 50) < 100)
                IDC_SLIDER1.Value = 100;
            else
                IDC_SLIDER1.Value = scale - 50;
            UpdatePictureScale(scale - 50);


        }
        void UpdatePictureScale(int iSliderPos)
        {
            int iSrcX = m_wndStandingPosture.GetSrcX();
            int iSrcY = m_wndStandingPosture.GetSrcY();
            int iSrcWidth = m_wndStandingPosture.GetSrcWidth();
            int iSrcHeight = m_wndStandingPosture.GetSrcHeight();

            int iSrcCenterX = iSrcX + iSrcWidth / 2;
            int iSrcCenterY = iSrcY + iSrcHeight / 2;

            /* CRect rcClient;
             m_wndStandingPosture.GetClientRect(&rcClient);*/
            int Val = 400 - (iSliderPos - 100);
            double dImgMag = /*(Constants.IMG_SCALE_MAX + Constants.IMG_SCALE_MIN) - /*Val / 100.0;*/iSliderPos / 100.0;
            if (dImgMag < Constants.IMG_SCALE_MIN)
            {
                dImgMag = Constants.IMG_SCALE_MIN;
            }
            else if (dImgMag > Constants.IMG_SCALE_MAX)
            {
                dImgMag = Constants.IMG_SCALE_MAX;
            }
            int iNewSrcWidth = (int)(m_wndStandingPosture.GetBackgroundWidth() / dImgMag);
            int iNewSrcHeight = (int)(m_wndStandingPosture.GetBackgroundHeight() / dImgMag);

            int iNewSrcX = iSrcCenterX - iNewSrcWidth / 2;
            int iNewSrcY = iSrcCenterY - iNewSrcHeight / 2;
            if (iNewSrcX < 0)
            {
                iNewSrcX = 0;
            }
            if (iNewSrcY < 0)
            {
                iNewSrcY = 0;
            }
            if (iNewSrcX + iNewSrcWidth >= m_wndStandingPosture.GetBackgroundWidth())
            {
                iNewSrcX = m_wndStandingPosture.GetBackgroundWidth() - 1 - iNewSrcWidth;
            }
            if (iNewSrcY + iNewSrcHeight >= m_wndStandingPosture.GetBackgroundHeight())
            {
                iNewSrcY = m_wndStandingPosture.GetBackgroundHeight() - 1 - iNewSrcHeight;
            }
            m_wndStandingPosture.SetSrcPos(iNewSrcX, iNewSrcY);
            m_wndStandingPosture.SetSrcSize(iNewSrcWidth, iNewSrcHeight);
            m_wndKneedownPosture.SetSrcPos(iNewSrcX, iNewSrcY);
            m_wndKneedownPosture.SetSrcSize(iNewSrcWidth, iNewSrcHeight);

            pictureBox_standing.Invalidate();
            pictureBox_KneeDown.Invalidate();

            /* m_wndStandingPosture.UpdateOffscreen();
             m_wndStandingPosture.GetClientRect(&rcClient);
             m_wndStandingPosture.InvalidateRect(&rcClient);
             m_wndStandingPosture.UpdateWindow();

             m_wndKneedownPosture.UpdateOffscreen();
             m_wndKneedownPosture.GetClientRect(&rcClient);
             m_wndKneedownPosture.InvalidateRect(&rcClient);
             m_wndKneedownPosture.UpdateWindow();*/
        }

        private void IDC_SLIDER1_Scroll(object sender, EventArgs e)
        {
            int scale = IDC_SLIDER1.Value;
            UpdatePictureScale(scale);
        }

        private void IDC_Mag1Btn_Click(object sender, EventArgs e)// Zoom Out Button
        {
            m_ZoomFlag = Constants.ZOOMOUT;
            timer1.Interval = 100;
            timer1.Start();
            IDC_Mag1Btn.Image = Yugamiru.Properties.Resources.mag2_down;



            int scale = IDC_SLIDER1.Value;
            if (scale >= 400)
                return;
            if ((scale + 50) > 400)
                IDC_SLIDER1.Value = 400;
            else
                IDC_SLIDER1.Value = scale + 50;


            // int scale = m_sliderScale.GetPos();
            // m_sliderScale.SetPos(scale - 50);
            UpdatePictureScale(scale + 50);


        }

        private void IDC_ResetImgBtn_Click(object sender, EventArgs e)
        {
            m_ZoomFlag = Constants.RESET;
            timer1.Interval = 100;
            timer1.Start();
            IDC_ResetImgBtn.Image = Yugamiru.Properties.Resources.mag3_down;

            switch (m_JointEditDoc.GetJointEditViewMode())
            {
                case Constants.JOINTEDITVIEWMODE_ANKLE_AND_HIP:
                    SetAnkleAndHipMode();
                    break;
                case Constants.JOINTEDITVIEWMODE_KNEE:
                    SetKneeMode();
                    break;
                case Constants.JOINTEDITVIEWMODE_UPPERBODY:
                    SetUpperBodyMode();
                    break;
                case Constants.JOINTEDITVIEWMODE_SIDE:
                case Constants.JOINTEDITVIEWMODE_NONE:
                default:
                    break;
            }
            int scale = IDC_SLIDER1.Value;
            UpdatePictureScale(scale);
        }
        public event EventHandler EventToStartSideJointEditView; // creating event handler - step1
        public void Function_StartSideJointEditView(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = EventToStartSideJointEditView;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }

        private void pictureBox_KneeDown_MouseDown(object sender, MouseEventArgs e)
        {
            switch (MouseButtons)
            {
                case MouseButtons.Right:

                    if (m_wndKneedownPosture.m_iMouseCaptureMode != 0)
                    {
                        return;
                    }


                    m_wndKneedownPosture.m_iMouseCaptureMode = 2;
                    m_wndKneedownPosture.m_iMousePointXOnDragStart = e.X;
                    m_wndKneedownPosture.m_iMousePointYOnDragStart = e.Y;
                    m_wndKneedownPosture.m_iScreenWidthOnDragStart = 423;//rcClient.Width;
                    m_wndKneedownPosture.m_iScreenHeightOnDragStart = 470;//rcClient.Height;
                    m_wndKneedownPosture.m_iSrcXOnDragStart = m_wndKneedownPosture.m_iSrcX;
                    m_wndKneedownPosture.m_iSrcYOnDragStart = m_wndKneedownPosture.m_iSrcY;
                    m_wndKneedownPosture.m_iSrcWidthOnDragStart = m_wndKneedownPosture.m_iSrcWidth;
                    m_wndKneedownPosture.m_iSrcHeightOnDragStart = m_wndKneedownPosture.m_iSrcHeight;
                    m_wndKneedownPosture.m_iEditPtID = -1;

                    //pictureBox1_MouseDown(sender,e); //  by Meena

                    m_wndStandingPosture.m_iMouseCaptureMode = 2;
                    m_wndStandingPosture.m_iMousePointXOnDragStart = e.X;
                    m_wndStandingPosture.m_iMousePointYOnDragStart = e.Y;
                    m_wndStandingPosture.m_iScreenWidthOnDragStart = 423;//rcClient.Width;
                    m_wndStandingPosture.m_iScreenHeightOnDragStart = 470;//rcClient.Height;
                    m_wndStandingPosture.m_iSrcXOnDragStart = m_wndStandingPosture.m_iSrcX;
                    m_wndStandingPosture.m_iSrcYOnDragStart = m_wndStandingPosture.m_iSrcY;
                    m_wndStandingPosture.m_iSrcWidthOnDragStart = m_wndStandingPosture.m_iSrcWidth;
                    m_wndStandingPosture.m_iSrcHeightOnDragStart = m_wndStandingPosture.m_iSrcHeight;
                    m_wndStandingPosture.m_iEditPtID = -1;


                    break;
                case MouseButtons.Left:
                    if (m_wndKneedownPosture.m_iMouseCaptureMode != 0)
                    {
                        return;
                    }

                    m_wndKneedownPosture.m_iEditPtID = m_wndKneedownPosture.SearchEditPoint(e.Location, 15 * 15);
                    if (m_wndKneedownPosture.m_iEditPtID == -1)
                    {
                        // OutputDebugString("EditPoint = -1 \n");
                        return;
                    }
                    else
                    {
                        char[] szMessage = new char[100];
                        //   wsprintf(&(szMessage[0]), "EditPoint = %d\n", m_iEditPtID);
                        //   OutputDebugString(&(szMessage[0]));
                    }

                    //  SetCapture();
                    m_wndKneedownPosture.m_iMouseCaptureMode = 1;


                    // GetClientRect(&rcClient);

                    m_wndKneedownPosture.m_iMousePointXOnDragStart = e.X;
                    m_wndKneedownPosture.m_iMousePointYOnDragStart = e.Y;
                    m_wndKneedownPosture.m_iScreenWidthOnDragStart = 423;//rcClient.Width;
                    m_wndKneedownPosture.m_iScreenHeightOnDragStart = 470;//rcClient.Height;
                    m_wndKneedownPosture.m_iSrcXOnDragStart = m_wndStandingPosture.m_iSrcX;
                    m_wndKneedownPosture.m_iSrcYOnDragStart = m_wndStandingPosture.m_iSrcY;
                    m_wndKneedownPosture.m_iSrcWidthOnDragStart = m_wndStandingPosture.m_iSrcWidth;
                    m_wndKneedownPosture.m_iSrcHeightOnDragStart = m_wndStandingPosture.m_iSrcHeight;
                    /*
                        if (GetParent() != NULL)
                        {
                            GetParent()->SendMessage(WM_COMMAND, (WPARAM)GetDlgCtrlID(), (LPARAM)GetSafeHwnd());
                        }
                        */

                    //MessageBox.Show("Left Mouse button clicked");
                    break;
            }
            m_JointEditDoc.SetKneedownMarkerTouchFlag(m_wndKneedownPosture.GetEditPtID());// Newly added by Meena

        }

        private void pictureBox_KneeDown_MouseMove(object sender, MouseEventArgs e)
        {
            //Graphics temp_Graphics = this.CreateGraphics();
            if ((m_wndKneedownPosture.m_iMouseCaptureMode == 1) && (m_wndKneedownPosture.m_iEditPtID != -1) &&
                (m_wndKneedownPosture.m_isValidEditPoint[m_wndKneedownPosture.m_iEditPtID]))
            {
                // ŠÖßˆÊ’uC³
                m_wndKneedownPosture.m_aptEdit[m_wndKneedownPosture.m_iEditPtID].X =
                    e.X * m_wndKneedownPosture.m_iSrcWidth / m_wndKneedownPosture.m_iOffscreenWidth + m_wndKneedownPosture.m_iSrcX;
                m_wndKneedownPosture.m_aptEdit[m_wndKneedownPosture.m_iEditPtID].Y =
                    e.Y * m_wndKneedownPosture.m_iSrcHeight / m_wndKneedownPosture.m_iOffscreenHeight + m_wndKneedownPosture.m_iSrcY;

                if (m_wndKneedownPosture.m_aptEdit[m_wndKneedownPosture.m_iEditPtID].X < 0)
                {
                    m_wndKneedownPosture.m_aptEdit[m_wndKneedownPosture.m_iEditPtID].X = 0;
                }
                if (m_wndKneedownPosture.m_aptEdit[m_wndKneedownPosture.m_iEditPtID].Y < 0)
                {
                    m_wndKneedownPosture.m_aptEdit[m_wndKneedownPosture.m_iEditPtID].Y = 0;
                }
                if (m_wndKneedownPosture.m_aptEdit[m_wndKneedownPosture.m_iEditPtID].X >=
                    m_wndKneedownPosture.m_iBackgroundWidth)
                {
                    m_wndKneedownPosture.m_aptEdit[m_wndKneedownPosture.m_iEditPtID].X =
                        m_wndKneedownPosture.m_iBackgroundWidth - 1;
                }
                if (m_wndKneedownPosture.m_aptEdit[m_wndKneedownPosture.m_iEditPtID].Y >= m_wndKneedownPosture.m_iBackgroundHeight)
                {
                    m_wndKneedownPosture.m_aptEdit[m_wndKneedownPosture.m_iEditPtID].Y =
                        m_wndKneedownPosture.m_iBackgroundHeight - 1;
                }

                //m_wndStandingPosture.UpdateOffscreen(temp_Graphics);
                /*       if (GetParent() != NULL)
                       {
                           GetParent()->SendMessage(WM_COMMAND, (WPARAM)GetDlgCtrlID(), (LPARAM)GetSafeHwnd());
                       }
                       UpdateOffscreen();
                       CRect rcClient;
                       GetClientRect(&rcClient);
                       InvalidateRect(&rcClient);
                       UpdateWindow();
                       */

                pictureBox_KneeDown.Invalidate();
            }
            else if (m_wndKneedownPosture.m_iMouseCaptureMode == 2)
            {
                int iDiffX = e.X - m_wndKneedownPosture.m_iMousePointXOnDragStart;
                int iDiffY = e.Y - m_wndKneedownPosture.m_iMousePointYOnDragStart;
                m_wndKneedownPosture.m_iSrcX = m_wndKneedownPosture.m_iSrcXOnDragStart - iDiffX * m_wndKneedownPosture.m_iSrcWidthOnDragStart / m_wndKneedownPosture.m_iScreenWidthOnDragStart;
                m_wndKneedownPosture.m_iSrcY = m_wndKneedownPosture.m_iSrcYOnDragStart - iDiffY * m_wndKneedownPosture.m_iSrcHeightOnDragStart / m_wndKneedownPosture.m_iScreenHeightOnDragStart;
                if (m_wndKneedownPosture.m_iSrcX < 0)
                {
                    m_wndKneedownPosture.m_iSrcX = 0;
                }
                if (m_wndKneedownPosture.m_iSrcY < 0)
                {
                    m_wndKneedownPosture.m_iSrcY = 0;
                }
                if (m_wndKneedownPosture.m_iSrcX + m_wndKneedownPosture.m_iSrcWidthOnDragStart >=
                    m_wndKneedownPosture.m_iBackgroundWidth)
                {
                    m_wndKneedownPosture.m_iSrcX = m_wndKneedownPosture.m_iBackgroundWidth -
                        m_wndKneedownPosture.m_iSrcWidthOnDragStart;
                }
                if (m_wndKneedownPosture.m_iSrcY + m_wndKneedownPosture.m_iSrcHeightOnDragStart >=
                    m_wndKneedownPosture.m_iBackgroundHeight)
                {
                    m_wndKneedownPosture.m_iSrcY = m_wndKneedownPosture.m_iBackgroundHeight -
                        m_wndKneedownPosture.m_iSrcHeightOnDragStart;
                }

                //m_wndStandingPosture.UpdateOffscreen(temp_Graphics);
                /*        if (GetParent() != NULL)
                        {
                            GetParent()->SendMessage(WM_COMMAND, (WPARAM)GetDlgCtrlID(), (LPARAM)GetSafeHwnd());
                        }
                        UpdateOffscreen();
                        CRect rcClient;
                        GetClientRect(&rcClient);
                        InvalidateRect(&rcClient);
                        UpdateWindow();*/

                m_wndStandingPosture.m_iSrcX = m_wndStandingPosture.m_iSrcXOnDragStart - iDiffX * m_wndStandingPosture.m_iSrcWidthOnDragStart / m_wndStandingPosture.m_iScreenWidthOnDragStart;
                m_wndStandingPosture.m_iSrcY = m_wndStandingPosture.m_iSrcYOnDragStart - iDiffY * m_wndStandingPosture.m_iSrcHeightOnDragStart / m_wndStandingPosture.m_iScreenHeightOnDragStart;
                if (m_wndStandingPosture.m_iSrcX < 0)
                {
                    m_wndStandingPosture.m_iSrcX = 0;
                }
                if (m_wndStandingPosture.m_iSrcY < 0)
                {
                    m_wndStandingPosture.m_iSrcY = 0;
                }
                if (m_wndStandingPosture.m_iSrcX + m_wndStandingPosture.m_iSrcWidthOnDragStart >=
                    m_wndStandingPosture.m_iBackgroundWidth)
                {
                    m_wndStandingPosture.m_iSrcX = m_wndStandingPosture.m_iBackgroundWidth -
                        m_wndStandingPosture.m_iSrcWidthOnDragStart;
                }
                if (m_wndStandingPosture.m_iSrcY + m_wndStandingPosture.m_iSrcHeightOnDragStart >=
                    m_wndStandingPosture.m_iBackgroundHeight)
                {
                    m_wndStandingPosture.m_iSrcY = m_wndStandingPosture.m_iBackgroundHeight -
                        m_wndStandingPosture.m_iSrcHeightOnDragStart;
                }

                pictureBox_KneeDown.Invalidate();
                pictureBox_standing.Invalidate();

            }
        }

        private void pictureBox_KneeDown_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {

                if (m_wndKneedownPosture.m_iMouseCaptureMode != 2)
                {
                    return;
                }
                //ReleaseCapture();
                m_wndKneedownPosture.m_iMouseCaptureMode = 0;
                m_wndKneedownPosture.m_iEditPtID = -1;

                m_wndStandingPosture.m_iMouseCaptureMode = 0;
                m_wndStandingPosture.m_iEditPtID = -1;
                //pictureBox1_MouseUp(sender, e);
            }
            else
            {
                if (m_wndKneedownPosture.m_iMouseCaptureMode != 1)
                {
                    return;
                }
                //ReleaseCapture();
                m_wndKneedownPosture.ExchangeLRData();
                m_wndKneedownPosture.m_iMouseCaptureMode = 0;
                m_wndKneedownPosture.m_iEditPtID = -1;

            }
            SetEditPointMessage(m_wndKneedownPosture.GetEditPtID());
        }
        Timer t = new Timer();
        private void timer1_Tick(object sender, EventArgs e)
        {
            switch (m_ZoomFlag)
            {
                case Constants.ZOOMOUT:
                    IDC_Mag1Btn.Image = Yugamiru.Properties.Resources.mag2_up;
                    break;
                case Constants.ZOOMIN:
                    IDC_Mag2Btn.Image = Yugamiru.Properties.Resources.mag1_up;
                    break;
                case Constants.RESET:
                    IDC_ResetImgBtn.Image = Yugamiru.Properties.Resources.mag3_up;
                    break;
            }
        }
        void timer_tick(object sender, EventArgs e)
        { 
            switch(m_Clickmode)
            {
                case "NEXT":
                    IDC_OkBtn.Image = Yugamiru.Properties.Resources.gonextred_on;
                    break;
                case "RETURN":
                    IDC_CancelBtn.Image = Yugamiru.Properties.Resources.gobackred_on;
                    break;
            }
            t.Stop();
        }
        public void RefreshForm()
        {
            m_MainPic.Image = Yugamiru.Properties.Resources.Mainpic4;

            IDC_Mag1Btn.Image = Yugamiru.Properties.Resources.mag2_up;
            IDC_Mag2Btn.Image = Yugamiru.Properties.Resources.mag1_up;
            IDC_ResetImgBtn.Image = Yugamiru.Properties.Resources.mag3_up;

            IDC_OkBtn.Image = Yugamiru.Properties.Resources.gonextred_on;
            IDC_CancelBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
            m_wndStandingPosture.SetMarkerSizeBase(m_JointEditDoc.GetMarkerSize());
            m_wndKneedownPosture.SetMarkerSizeBase(m_JointEditDoc.GetMarkerSize());
            switch (m_JointEditDoc.GetInputMode())
            {
                case Constants.INPUTMODE_NEW:

                    m_JointEditDoc.GetStandingFrontBodyPosition(ref m_FrontBodyPositionStanding);
                    m_wndStandingPosture.SetFrontBodyPosition(m_FrontBodyPositionStanding);

                    m_JointEditDoc.GetKneedownFrontBodyPosition(ref m_FrontBodyPositionKneedown);
                    m_wndKneedownPosture.SetFrontBodyPosition(m_FrontBodyPositionKneedown);

                    
                    /*m_btnBack.EnableWindow(TRUE);
                    m_btnBack.ShowWindow(SW_SHOW);*/

                    break;
                case Constants.INPUTMODE_MODIFY:
                    m_JointEditDoc.GetStandingFrontBodyPosition(ref m_FrontBodyPositionStanding);
                    m_wndStandingPosture.SetFrontBodyPosition(m_FrontBodyPositionStanding);

                    m_JointEditDoc.GetKneedownFrontBodyPosition(ref m_FrontBodyPositionKneedown);
                    m_wndKneedownPosture.SetFrontBodyPosition(m_FrontBodyPositionKneedown);

                    /* m_btnBack.EnableWindow(FALSE);
                     m_btnBack.ShowWindow(SW_HIDE);*/
                    IDC_CancelBtn.Image = Properties.Resources.gobackgreen_up;
                    
                    break;
                case Constants.INPUTMODE_NONE:
                default:
                    break;
            }
            switch (m_JointEditDoc.GetJointEditViewMode())
            {
                case Constants.JOINTEDITVIEWMODE_ANKLE_AND_HIP:
                    SetAnkleAndHipMode();
                    break;
                case Constants.JOINTEDITVIEWMODE_KNEE:
                    SetKneeMode();
                    break;
                case Constants.JOINTEDITVIEWMODE_UPPERBODY:
                    SetUpperBodyMode();
                    break;
            }
            m_JointEditDoc.GetMainScreen().RefreshMenuStrip(false);

        }

        private void JointEditView_MouseUp(object sender, MouseEventArgs e)
        {
            //Event Added by Sumit GSP-1146
            //Added by Sumit GSP-1146-----START
            CloudManager.SynchronizationScheduler.lastActivityTime = DateTime.Now;
            //Added by Sumit GSP-1146-----START
        }
    }
}
