﻿namespace Yugamiru
{
    partial class IDD_BALANCELABO_DIALOG
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IDD_BALANCELABO_DIALOG));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.sETTINGSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.qRCODEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pORTSETTINGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lANGUAGESETTINGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cLOUDSETTINGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cONSOLESETTINGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pCSETTINGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bETATESTINGToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iMPORTYGAFILEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eXPORTYGAFILEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_License = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_View_License = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem_Release_License = new System.Windows.Forms.ToolStripMenuItem();
            this.hELPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.versionInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.trialExpiryBox = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnRefreshCloud = new System.Windows.Forms.Button();
            this.lblBottomAlert = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sETTINGSToolStripMenuItem,
            this.bETATESTINGToolStripMenuItem,
            this.hELPToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.ShowItemToolTips = true;
            this.menuStrip1.Size = new System.Drawing.Size(1217, 28);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // sETTINGSToolStripMenuItem
            // 
            this.sETTINGSToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.qRCODEToolStripMenuItem,
            this.pORTSETTINGToolStripMenuItem,
            this.lANGUAGESETTINGToolStripMenuItem,
            this.cLOUDSETTINGToolStripMenuItem});
            this.sETTINGSToolStripMenuItem.Name = "sETTINGSToolStripMenuItem";
            this.sETTINGSToolStripMenuItem.Size = new System.Drawing.Size(86, 24);
            this.sETTINGSToolStripMenuItem.Text = "SETTINGS";
            // 
            // qRCODEToolStripMenuItem
            // 
            this.qRCODEToolStripMenuItem.CheckOnClick = true;
            this.qRCODEToolStripMenuItem.Name = "qRCODEToolStripMenuItem";
            this.qRCODEToolStripMenuItem.Size = new System.Drawing.Size(221, 26);
            this.qRCODEToolStripMenuItem.Text = "QR CODE";
            this.qRCODEToolStripMenuItem.Visible = false;
            this.qRCODEToolStripMenuItem.CheckStateChanged += new System.EventHandler(this.qRCODEToolStripMenuItem_CheckStateChanged);
            this.qRCODEToolStripMenuItem.Click += new System.EventHandler(this.qRCODEToolStripMenuItem_Click);
            // 
            // pORTSETTINGToolStripMenuItem
            // 
            this.pORTSETTINGToolStripMenuItem.Name = "pORTSETTINGToolStripMenuItem";
            this.pORTSETTINGToolStripMenuItem.Size = new System.Drawing.Size(221, 26);
            this.pORTSETTINGToolStripMenuItem.Text = "PORT SETTING";
            this.pORTSETTINGToolStripMenuItem.Visible = false;
            this.pORTSETTINGToolStripMenuItem.Click += new System.EventHandler(this.pORTSETTINGToolStripMenuItem_Click);
            // 
            // lANGUAGESETTINGToolStripMenuItem
            // 
            this.lANGUAGESETTINGToolStripMenuItem.Name = "lANGUAGESETTINGToolStripMenuItem";
            this.lANGUAGESETTINGToolStripMenuItem.Size = new System.Drawing.Size(221, 26);
            this.lANGUAGESETTINGToolStripMenuItem.Text = "LANGUAGE SETTING";
            this.lANGUAGESETTINGToolStripMenuItem.Visible = false;
            this.lANGUAGESETTINGToolStripMenuItem.Click += new System.EventHandler(this.lANGUAGESETTINGToolStripMenuItem_Click);
            // 
            // cLOUDSETTINGToolStripMenuItem
            // 
            this.cLOUDSETTINGToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cONSOLESETTINGToolStripMenuItem,
            this.pCSETTINGToolStripMenuItem});
            this.cLOUDSETTINGToolStripMenuItem.Name = "cLOUDSETTINGToolStripMenuItem";
            this.cLOUDSETTINGToolStripMenuItem.Size = new System.Drawing.Size(221, 26);
            this.cLOUDSETTINGToolStripMenuItem.Text = "CLOUD SETTINGS";
            this.cLOUDSETTINGToolStripMenuItem.Click += new System.EventHandler(this.cLOUDSETTINGToolStripMenuItem_Click);
            // 
            // cONSOLESETTINGToolStripMenuItem
            // 
            this.cONSOLESETTINGToolStripMenuItem.Name = "cONSOLESETTINGToolStripMenuItem";
            this.cONSOLESETTINGToolStripMenuItem.Size = new System.Drawing.Size(207, 26);
            this.cONSOLESETTINGToolStripMenuItem.Text = "UPDATE SETTINGS";
            this.cONSOLESETTINGToolStripMenuItem.Click += new System.EventHandler(this.cONSOLESETTINGToolStripMenuItem_Click);
            // 
            // pCSETTINGToolStripMenuItem
            // 
            this.pCSETTINGToolStripMenuItem.Name = "pCSETTINGToolStripMenuItem";
            this.pCSETTINGToolStripMenuItem.Size = new System.Drawing.Size(207, 26);
            this.pCSETTINGToolStripMenuItem.Text = "PC SETTINGS";
            this.pCSETTINGToolStripMenuItem.Click += new System.EventHandler(this.pCSETTINGToolStripMenuItem_Click);
            // 
            // bETATESTINGToolStripMenuItem
            // 
            this.bETATESTINGToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iMPORTYGAFILEToolStripMenuItem,
            this.eXPORTYGAFILEToolStripMenuItem,
            this.toolStripMenuItem_License});
            this.bETATESTINGToolStripMenuItem.Name = "bETATESTINGToolStripMenuItem";
            this.bETATESTINGToolStripMenuItem.Size = new System.Drawing.Size(65, 24);
            this.bETATESTINGToolStripMenuItem.Text = "TOOLS";
            // 
            // iMPORTYGAFILEToolStripMenuItem
            // 
            this.iMPORTYGAFILEToolStripMenuItem.Name = "iMPORTYGAFILEToolStripMenuItem";
            this.iMPORTYGAFILEToolStripMenuItem.Size = new System.Drawing.Size(197, 26);
            this.iMPORTYGAFILEToolStripMenuItem.Text = "IMPORT YGA FILE";
            this.iMPORTYGAFILEToolStripMenuItem.Click += new System.EventHandler(this.iMPORTYGAFILEToolStripMenuItem_Click);
            // 
            // eXPORTYGAFILEToolStripMenuItem
            // 
            this.eXPORTYGAFILEToolStripMenuItem.Name = "eXPORTYGAFILEToolStripMenuItem";
            this.eXPORTYGAFILEToolStripMenuItem.Size = new System.Drawing.Size(197, 26);
            this.eXPORTYGAFILEToolStripMenuItem.Text = "EXPORT YGA FILE";
            this.eXPORTYGAFILEToolStripMenuItem.Visible = false;
            this.eXPORTYGAFILEToolStripMenuItem.Click += new System.EventHandler(this.eXPORTYGAFILEToolStripMenuItem_Click);
            // 
            // toolStripMenuItem_License
            // 
            this.toolStripMenuItem_License.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem_View_License,
            this.toolStripMenuItem_Release_License});
            this.toolStripMenuItem_License.Name = "toolStripMenuItem_License";
            this.toolStripMenuItem_License.Size = new System.Drawing.Size(197, 26);
            this.toolStripMenuItem_License.Text = "LICENSE";
            // 
            // toolStripMenuItem_View_License
            // 
            this.toolStripMenuItem_View_License.Name = "toolStripMenuItem_View_License";
            this.toolStripMenuItem_View_License.Size = new System.Drawing.Size(201, 26);
            this.toolStripMenuItem_View_License.Text = "VIEW LICENSE";
            this.toolStripMenuItem_View_License.Click += new System.EventHandler(this.toolStripMenuItem_View_License_Click);
            // 
            // toolStripMenuItem_Release_License
            // 
            this.toolStripMenuItem_Release_License.Name = "toolStripMenuItem_Release_License";
            this.toolStripMenuItem_Release_License.Size = new System.Drawing.Size(201, 26);
            this.toolStripMenuItem_Release_License.Text = "RELEASE LICENSE";
            this.toolStripMenuItem_Release_License.Click += new System.EventHandler(this.toolStripMenuItem_Release_License_Click);
            // 
            // hELPToolStripMenuItem
            // 
            this.hELPToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.versionInfoToolStripMenuItem});
            this.hELPToolStripMenuItem.Name = "hELPToolStripMenuItem";
            this.hELPToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.H)));
            this.hELPToolStripMenuItem.Size = new System.Drawing.Size(55, 24);
            this.hELPToolStripMenuItem.Text = "HELP";
            this.hELPToolStripMenuItem.ToolTipText = "lfghlf";
            this.hELPToolStripMenuItem.Visible = false;
            // 
            // versionInfoToolStripMenuItem
            // 
            this.versionInfoToolStripMenuItem.Name = "versionInfoToolStripMenuItem";
            this.versionInfoToolStripMenuItem.Size = new System.Drawing.Size(299, 26);
            this.versionInfoToolStripMenuItem.Text = "Version Info Yugamiru cloud(A)...";
            this.versionInfoToolStripMenuItem.Click += new System.EventHandler(this.versionInfoToolStripMenuItem_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.AutoScroll = true;
            this.panel2.Location = new System.Drawing.Point(0, 33);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(284, 64);
            this.panel2.TabIndex = 5;
            this.panel2.SizeChanged += new System.EventHandler(this.panel2_SizeChanged);
            // 
            // trialExpiryBox
            // 
            this.trialExpiryBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.trialExpiryBox.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trialExpiryBox.ForeColor = System.Drawing.Color.Red;
            this.trialExpiryBox.Location = new System.Drawing.Point(1065, 2);
            this.trialExpiryBox.Margin = new System.Windows.Forms.Padding(4);
            this.trialExpiryBox.Name = "trialExpiryBox";
            this.trialExpiryBox.Size = new System.Drawing.Size(147, 30);
            this.trialExpiryBox.TabIndex = 6;
            this.trialExpiryBox.UseVisualStyleBackColor = true;
            this.trialExpiryBox.Click += new System.EventHandler(this.trialExpiryBox_Click);
            // 
            // button1
            // 
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(257, 1);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(39, 25);
            this.button1.TabIndex = 7;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnRefreshCloud
            // 
            this.btnRefreshCloud.Location = new System.Drawing.Point(304, 2);
            this.btnRefreshCloud.Margin = new System.Windows.Forms.Padding(4);
            this.btnRefreshCloud.Name = "btnRefreshCloud";
            this.btnRefreshCloud.Size = new System.Drawing.Size(143, 25);
            this.btnRefreshCloud.TabIndex = 8;
            this.btnRefreshCloud.Text = "Refresh Cloud";
            this.btnRefreshCloud.UseVisualStyleBackColor = true;
            this.btnRefreshCloud.Click += new System.EventHandler(this.btnRefreshCloud_Click);
            // 
            // lblBottomAlert
            // 
            this.lblBottomAlert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBottomAlert.AutoSize = true;
            this.lblBottomAlert.BackColor = System.Drawing.Color.White;
            this.lblBottomAlert.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBottomAlert.ForeColor = System.Drawing.Color.Green;
            this.lblBottomAlert.Location = new System.Drawing.Point(821, 1);
            this.lblBottomAlert.Name = "lblBottomAlert";
            this.lblBottomAlert.Size = new System.Drawing.Size(57, 24);
            this.lblBottomAlert.TabIndex = 9;
            this.lblBottomAlert.Text = "Alerts";
            this.lblBottomAlert.Visible = false;
            // 
            // IDD_BALANCELABO_DIALOG
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1217, 599);
            this.Controls.Add(this.lblBottomAlert);
            this.Controls.Add(this.btnRefreshCloud);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.trialExpiryBox);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(1209, 573);
            this.Name = "IDD_BALANCELABO_DIALOG";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "YUGAMIRU";
            this.MaximumSizeChanged += new System.EventHandler(this.IDD_BALANCELABO_DIALOG_MaximumSizeChanged);
            this.MinimumSizeChanged += new System.EventHandler(this.IDD_BALANCELABO_DIALOG_MinimumSizeChanged);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.IDD_BALANCELABO_DIALOG_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.IDD_BALANCELABO_DIALOG_FormClosed);
            this.Load += new System.EventHandler(this.IDD_BALANCELABO_DIALOG_Load);
            this.Shown += new System.EventHandler(this.IDD_BALANCELABO_DIALOG_Shown);
            this.SizeChanged += new System.EventHandler(this.IDD_BALANCELABO_DIALOG_SizeChanged);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.IDD_BALANCELABO_DIALOG_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.IDD_BALANCELABO_DIALOG_KeyDown);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.IDD_BALANCELABO_DIALOG_MouseUp);
            this.Resize += new System.EventHandler(this.IDD_BALANCELABO_DIALOG_Resize);
            this.ParentChanged += new System.EventHandler(this.IDD_BALANCELABO_DIALOG_ParentChanged);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem sETTINGSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem qRCODEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pORTSETTINGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hELPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem versionInfoToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStripMenuItem lANGUAGESETTINGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bETATESTINGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem iMPORTYGAFILEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eXPORTYGAFILEToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_License;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_View_License;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem_Release_License;
        public System.Windows.Forms.Button trialExpiryBox;
        public System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem cLOUDSETTINGToolStripMenuItem;
        public System.Windows.Forms.Button btnRefreshCloud;
        private System.Windows.Forms.ToolStripMenuItem cONSOLESETTINGToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pCSETTINGToolStripMenuItem;
        public System.Windows.Forms.Label lblBottomAlert;
    }
}

