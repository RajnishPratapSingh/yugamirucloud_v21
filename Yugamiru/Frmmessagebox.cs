using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yugamiru
{
    public partial class Frmmessagebox : Form
    { 
        public Frmmessagebox()
        {
            InitializeComponent();

        }
       
        public void setMessage(string messageText)
        {
            lblMessageText.Font = SystemFonts.MessageBoxFont;
            int number = Math.Abs(messageText.Length);
            if (number >= 40)
            {
                this.Width = 480;
                lblMessageText.Width = 400;
                lblMessageText.Height = 40;

            }
            else if (number <= 25)
            {
                pnlShowMessage.Width = 265;
                this.Width = 250;
                this.Height = 130;
            }
            this.lblMessageText.Text = messageText;
          
        }

        public void addIconImage(enumMessageIcon MessageIcon)
        {
            switch (MessageIcon)
            {
                case enumMessageIcon.Error:
                    pictureBox1.Image = imageList1.Images["Error"];  
                    break;
                case enumMessageIcon.Information:
                    pictureBox1.Image = imageList1.Images["Information"];
                    break;
                case enumMessageIcon.Question:
                    pictureBox1.Image = imageList1.Images["Question"];
                    break;
                case enumMessageIcon.Warning:
                    pictureBox1.Image = imageList1.Images["Warning"];
                    break;
            }
        }
        public void addButton(enumMessageButton MessageButton)
        {
            switch (MessageButton)
            {
                case enumMessageButton.OK:
                    {
                        Button btnOk = new Button();
                        btnOk.Text = Properties.Resources.BUTTON_OK;
                        btnOk.DialogResult = DialogResult.OK;
                        btnOk.Font = SystemFonts.MessageBoxFont;
                        btnOk.FlatStyle = FlatStyle.Popup;
                        btnOk.FlatAppearance.BorderSize = 0;
                        btnOk.SetBounds(pnlShowMessage.ClientSize.Width - 80, 5, 75, 25);
                        pnlShowMessage.Controls.Add(btnOk);

                    }
                    break;
                case enumMessageButton.OKCancel:
                    {
                       
                        Button btnOk = new Button();
                        btnOk.Text = Properties.Resources.BUTTON_OK;
                        btnOk.DialogResult = DialogResult.OK;
                        btnOk.Font = SystemFonts.MessageBoxFont;
                        btnOk.FlatStyle = FlatStyle.Popup;
                        btnOk.FlatAppearance.BorderSize = 0;
                        btnOk.SetBounds((pnlShowMessage.ClientSize.Width - (btnOk.ClientSize.Width + 5 + 80)), 5, 75, 25);
                        pnlShowMessage.Controls.Add(btnOk);

                        Button btnCancel = new Button();
                        btnCancel.Text = Properties.Resources.CANCEL;
                        btnCancel.DialogResult = DialogResult.Cancel;
                        btnCancel.Font = SystemFonts.MessageBoxFont;
                        btnCancel.FlatStyle = FlatStyle.Popup;
                        btnCancel.FlatAppearance.BorderSize = 0;
                        btnCancel.SetBounds((pnlShowMessage.ClientSize.Width - 70), 5, 65, 25);
                        pnlShowMessage.Controls.Add(btnCancel);

                    }
                    break;
                case enumMessageButton.YesNo:
                    {
                        Button btnNo = new Button();
                        btnNo.Text = Properties.Resources.BUTTON_NO;
                        btnNo.DialogResult = DialogResult.No;
                        btnNo.Font = SystemFonts.MessageBoxFont;                       
                        btnNo.FlatStyle = FlatStyle.Popup;
                        btnNo.FlatAppearance.BorderSize = 0;                        
                        btnNo.SetBounds((pnlShowMessage.ClientSize.Width - 70), 5, 65, 25);
                        pnlShowMessage.Controls.Add(btnNo);

                        Button btnYes = new Button();
                        btnYes.Text = Properties.Resources.BUTTON_YES;
                        btnYes.DialogResult = DialogResult.Yes;
                        btnYes.Font = SystemFonts.MessageBoxFont;
                        btnYes.FlatStyle = FlatStyle.Popup;
                        btnYes.FlatAppearance.BorderSize = 0;
                        btnYes.SetBounds((pnlShowMessage.ClientSize.Width -(btnNo.ClientSize.Width + 5 + 80)), 5, 75, 25);
                        pnlShowMessage.Controls.Add(btnYes);                   

                    }
                    break;
                case enumMessageButton.YesNoCancel:
                    {
                        Button btnCancel = new Button();
                        btnCancel.Text = Properties.Resources.BUTTON_CANCEL;
                        btnCancel.DialogResult = DialogResult.Cancel;
                        btnCancel.Font = SystemFonts.MessageBoxFont;
                        btnCancel.FlatStyle = FlatStyle.Popup;
                        btnCancel.FlatAppearance.BorderSize = 0;
                        btnCancel.SetBounds((pnlShowMessage.ClientSize.Width - 70), 5, 65, 25);
                        pnlShowMessage.Controls.Add(btnCancel);

                        Button btnNo = new Button();
                        btnNo.Text = Properties.Resources.BUTTON_NO;
                        btnNo.DialogResult = DialogResult.No;
                        btnNo.Font = SystemFonts.MessageBoxFont;
                        btnNo.FlatStyle = FlatStyle.Popup;
                        btnNo.FlatAppearance.BorderSize = 0;
                        btnNo.SetBounds((pnlShowMessage.ClientSize.Width -(btnCancel.ClientSize.Width + 5 + 80)), 5, 75, 25);
                        pnlShowMessage.Controls.Add(btnNo);

                        Button btnYes = new Button();
                        btnYes.Text = Properties.Resources.BUTTON_YES;
                        btnYes.DialogResult = DialogResult.Yes;
                        btnYes.Font = SystemFonts.MessageBoxFont;
                        btnYes.FlatStyle = FlatStyle.Popup;
                        btnYes.FlatAppearance.BorderSize = 0;
                        btnYes.SetBounds((pnlShowMessage.ClientSize.Width -(btnCancel.ClientSize.Width + btnNo.ClientSize.Width + 10 + 80)), 5, 75, 25);
                        pnlShowMessage.Controls.Add(btnYes);

                    }
                    break;

            }

        }

        public static DialogResult Show(string messageText, enumMessageIcon messageIcon, enumMessageButton messageButton)
        {
            Frmmessagebox frmMessage = new Frmmessagebox();
            frmMessage.setMessage(messageText);
           // frmMessage.lblMessageText.TextAlign = ContentAlignment.MiddleCenter;
            frmMessage.addButton(messageButton);
            frmMessage.addIconImage(messageIcon);
            frmMessage.ShowDialog();
            return frmMessage.DialogResult;
        }

        public static DialogResult Show1(string messageText)
        {
            Frmmessagebox frmMessage = new Frmmessagebox();
            frmMessage.pictureBox1.Visible = false;          
            frmMessage.setMessage(messageText);
            frmMessage.lblMessageText.Location = new System.Drawing.Point(47, 25);
            frmMessage.ShowDialog();
          
            return frmMessage.DialogResult;
          


        }
        public static DialogResult Show2(string messageText, enumMessageButton messageButton)
        {
            Frmmessagebox frmMessage = new Frmmessagebox();
            frmMessage.pictureBox1.Visible = false;
            frmMessage.setMessage(messageText);
            frmMessage.lblMessageText.Location = new System.Drawing.Point(45, 25);
            frmMessage.addButton(messageButton);
            frmMessage.ShowDialog();
            return frmMessage.DialogResult;
        }

        public enum enumMessageButton
        {
            OK,
            YesNo,
            YesNoCancel,
            OKCancel
        }
        public enum enumMessageIcon
        {
            Error,
            Warning,
            Information,
            Question,
        }

     
    }
}
