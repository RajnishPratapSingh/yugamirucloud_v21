﻿namespace Yugamiru
{
    partial class SettingView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.IDOK = new System.Windows.Forms.PictureBox();
            this.IDCANCEL = new System.Windows.Forms.PictureBox();
            this.IDC_RADIO_ARROW_ON = new System.Windows.Forms.PictureBox();
            this.IDC_RADIO_ARROW_OFF = new System.Windows.Forms.PictureBox();
            this.IDC_RADIO_CENTERLINE_ON = new System.Windows.Forms.PictureBox();
            this.IDC_RADIO_CENTERLINE_OFF = new System.Windows.Forms.PictureBox();
            this.IDC_RADIO_LABEL_ON = new System.Windows.Forms.PictureBox();
            this.IDC_RADIO_LABEL_OFF = new System.Windows.Forms.PictureBox();
            this.IDC_RADIO_SCORE_ON = new System.Windows.Forms.PictureBox();
            this.IDC_RADIO_SCORE_OFF = new System.Windows.Forms.PictureBox();
            this.IDC_RADIO_MUSCLEREPORT_ON = new System.Windows.Forms.PictureBox();
            this.IDC_RADIO_MUSCLEREPORT_OFF = new System.Windows.Forms.PictureBox();
            this.IDC_RADIO_CENTROID_ON = new System.Windows.Forms.PictureBox();
            this.IDC_RADIO_CENTROID_OFF = new System.Windows.Forms.PictureBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDCANCEL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RADIO_ARROW_ON)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RADIO_ARROW_OFF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RADIO_CENTERLINE_ON)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RADIO_CENTERLINE_OFF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RADIO_LABEL_ON)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RADIO_LABEL_OFF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RADIO_SCORE_ON)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RADIO_SCORE_OFF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RADIO_MUSCLEREPORT_ON)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RADIO_MUSCLEREPORT_OFF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RADIO_CENTROID_ON)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RADIO_CENTROID_OFF)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(69, 46);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(133, 62);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // IDOK
            // 
            this.IDOK.Location = new System.Drawing.Point(16, 268);
            this.IDOK.Margin = new System.Windows.Forms.Padding(4);
            this.IDOK.Name = "IDOK";
            this.IDOK.Size = new System.Drawing.Size(133, 62);
            this.IDOK.TabIndex = 1;
            this.IDOK.TabStop = false;
            this.IDOK.Click += new System.EventHandler(this.IDOK_Click);
            // 
            // IDCANCEL
            // 
            this.IDCANCEL.Location = new System.Drawing.Point(208, 268);
            this.IDCANCEL.Margin = new System.Windows.Forms.Padding(4);
            this.IDCANCEL.Name = "IDCANCEL";
            this.IDCANCEL.Size = new System.Drawing.Size(133, 62);
            this.IDCANCEL.TabIndex = 2;
            this.IDCANCEL.TabStop = false;
            this.IDCANCEL.Click += new System.EventHandler(this.IDCANCEL_Click);
            // 
            // IDC_RADIO_ARROW_ON
            // 
            this.IDC_RADIO_ARROW_ON.Location = new System.Drawing.Point(16, 137);
            this.IDC_RADIO_ARROW_ON.Margin = new System.Windows.Forms.Padding(4);
            this.IDC_RADIO_ARROW_ON.Name = "IDC_RADIO_ARROW_ON";
            this.IDC_RADIO_ARROW_ON.Size = new System.Drawing.Size(25, 23);
            this.IDC_RADIO_ARROW_ON.TabIndex = 15;
            this.IDC_RADIO_ARROW_ON.TabStop = false;
            this.IDC_RADIO_ARROW_ON.Click += new System.EventHandler(this.IDC_RADIO_ARROW_ON_Click);
            // 
            // IDC_RADIO_ARROW_OFF
            // 
            this.IDC_RADIO_ARROW_OFF.Location = new System.Drawing.Point(91, 137);
            this.IDC_RADIO_ARROW_OFF.Margin = new System.Windows.Forms.Padding(4);
            this.IDC_RADIO_ARROW_OFF.Name = "IDC_RADIO_ARROW_OFF";
            this.IDC_RADIO_ARROW_OFF.Size = new System.Drawing.Size(13, 14);
            this.IDC_RADIO_ARROW_OFF.TabIndex = 16;
            this.IDC_RADIO_ARROW_OFF.TabStop = false;
            this.IDC_RADIO_ARROW_OFF.Click += new System.EventHandler(this.IDC_RADIO_ARROW_OFF_Click);
            // 
            // IDC_RADIO_CENTERLINE_ON
            // 
            this.IDC_RADIO_CENTERLINE_ON.Location = new System.Drawing.Point(16, 182);
            this.IDC_RADIO_CENTERLINE_ON.Margin = new System.Windows.Forms.Padding(4);
            this.IDC_RADIO_CENTERLINE_ON.Name = "IDC_RADIO_CENTERLINE_ON";
            this.IDC_RADIO_CENTERLINE_ON.Size = new System.Drawing.Size(13, 14);
            this.IDC_RADIO_CENTERLINE_ON.TabIndex = 17;
            this.IDC_RADIO_CENTERLINE_ON.TabStop = false;
            this.IDC_RADIO_CENTERLINE_ON.Click += new System.EventHandler(this.IDC_RADIO_CENTERLINE_ON_Click);
            // 
            // IDC_RADIO_CENTERLINE_OFF
            // 
            this.IDC_RADIO_CENTERLINE_OFF.Location = new System.Drawing.Point(91, 180);
            this.IDC_RADIO_CENTERLINE_OFF.Margin = new System.Windows.Forms.Padding(4);
            this.IDC_RADIO_CENTERLINE_OFF.Name = "IDC_RADIO_CENTERLINE_OFF";
            this.IDC_RADIO_CENTERLINE_OFF.Size = new System.Drawing.Size(13, 14);
            this.IDC_RADIO_CENTERLINE_OFF.TabIndex = 18;
            this.IDC_RADIO_CENTERLINE_OFF.TabStop = false;
            this.IDC_RADIO_CENTERLINE_OFF.Click += new System.EventHandler(this.IDC_RADIO_CENTERLINE_OFF_Click);
            // 
            // IDC_RADIO_LABEL_ON
            // 
            this.IDC_RADIO_LABEL_ON.Location = new System.Drawing.Point(16, 229);
            this.IDC_RADIO_LABEL_ON.Margin = new System.Windows.Forms.Padding(4);
            this.IDC_RADIO_LABEL_ON.Name = "IDC_RADIO_LABEL_ON";
            this.IDC_RADIO_LABEL_ON.Size = new System.Drawing.Size(13, 14);
            this.IDC_RADIO_LABEL_ON.TabIndex = 19;
            this.IDC_RADIO_LABEL_ON.TabStop = false;
            this.IDC_RADIO_LABEL_ON.Click += new System.EventHandler(this.IDC_RADIO_LABEL_ON_Click);
            // 
            // IDC_RADIO_LABEL_OFF
            // 
            this.IDC_RADIO_LABEL_OFF.Location = new System.Drawing.Point(91, 229);
            this.IDC_RADIO_LABEL_OFF.Margin = new System.Windows.Forms.Padding(4);
            this.IDC_RADIO_LABEL_OFF.Name = "IDC_RADIO_LABEL_OFF";
            this.IDC_RADIO_LABEL_OFF.Size = new System.Drawing.Size(13, 14);
            this.IDC_RADIO_LABEL_OFF.TabIndex = 20;
            this.IDC_RADIO_LABEL_OFF.TabStop = false;
            this.IDC_RADIO_LABEL_OFF.Click += new System.EventHandler(this.IDC_RADIO_LABEL_OFF_Click);
            // 
            // IDC_RADIO_SCORE_ON
            // 
            this.IDC_RADIO_SCORE_ON.Location = new System.Drawing.Point(227, 137);
            this.IDC_RADIO_SCORE_ON.Margin = new System.Windows.Forms.Padding(4);
            this.IDC_RADIO_SCORE_ON.Name = "IDC_RADIO_SCORE_ON";
            this.IDC_RADIO_SCORE_ON.Size = new System.Drawing.Size(13, 14);
            this.IDC_RADIO_SCORE_ON.TabIndex = 21;
            this.IDC_RADIO_SCORE_ON.TabStop = false;
            this.IDC_RADIO_SCORE_ON.Click += new System.EventHandler(this.IDC_RADIO_SCORE_ON_Click);
            // 
            // IDC_RADIO_SCORE_OFF
            // 
            this.IDC_RADIO_SCORE_OFF.Location = new System.Drawing.Point(328, 137);
            this.IDC_RADIO_SCORE_OFF.Margin = new System.Windows.Forms.Padding(4);
            this.IDC_RADIO_SCORE_OFF.Name = "IDC_RADIO_SCORE_OFF";
            this.IDC_RADIO_SCORE_OFF.Size = new System.Drawing.Size(13, 14);
            this.IDC_RADIO_SCORE_OFF.TabIndex = 22;
            this.IDC_RADIO_SCORE_OFF.TabStop = false;
            this.IDC_RADIO_SCORE_OFF.Click += new System.EventHandler(this.IDC_RADIO_SCORE_OFF_Click);
            // 
            // IDC_RADIO_MUSCLEREPORT_ON
            // 
            this.IDC_RADIO_MUSCLEREPORT_ON.Location = new System.Drawing.Point(227, 182);
            this.IDC_RADIO_MUSCLEREPORT_ON.Margin = new System.Windows.Forms.Padding(4);
            this.IDC_RADIO_MUSCLEREPORT_ON.Name = "IDC_RADIO_MUSCLEREPORT_ON";
            this.IDC_RADIO_MUSCLEREPORT_ON.Size = new System.Drawing.Size(13, 14);
            this.IDC_RADIO_MUSCLEREPORT_ON.TabIndex = 23;
            this.IDC_RADIO_MUSCLEREPORT_ON.TabStop = false;
            this.IDC_RADIO_MUSCLEREPORT_ON.Click += new System.EventHandler(this.IDC_RADIO_MUSCLEREPORT_ON_Click);
            // 
            // IDC_RADIO_MUSCLEREPORT_OFF
            // 
            this.IDC_RADIO_MUSCLEREPORT_OFF.Location = new System.Drawing.Point(333, 180);
            this.IDC_RADIO_MUSCLEREPORT_OFF.Margin = new System.Windows.Forms.Padding(4);
            this.IDC_RADIO_MUSCLEREPORT_OFF.Name = "IDC_RADIO_MUSCLEREPORT_OFF";
            this.IDC_RADIO_MUSCLEREPORT_OFF.Size = new System.Drawing.Size(13, 14);
            this.IDC_RADIO_MUSCLEREPORT_OFF.TabIndex = 24;
            this.IDC_RADIO_MUSCLEREPORT_OFF.TabStop = false;
            this.IDC_RADIO_MUSCLEREPORT_OFF.Click += new System.EventHandler(this.IDC_RADIO_MUSCLEREPORT_OFF_Click);
            // 
            // IDC_RADIO_CENTROID_ON
            // 
            this.IDC_RADIO_CENTROID_ON.Location = new System.Drawing.Point(227, 229);
            this.IDC_RADIO_CENTROID_ON.Margin = new System.Windows.Forms.Padding(4);
            this.IDC_RADIO_CENTROID_ON.Name = "IDC_RADIO_CENTROID_ON";
            this.IDC_RADIO_CENTROID_ON.Size = new System.Drawing.Size(13, 14);
            this.IDC_RADIO_CENTROID_ON.TabIndex = 25;
            this.IDC_RADIO_CENTROID_ON.TabStop = false;
            this.IDC_RADIO_CENTROID_ON.Click += new System.EventHandler(this.IDC_RADIO_CENTROID_ON_Click);
            // 
            // IDC_RADIO_CENTROID_OFF
            // 
            this.IDC_RADIO_CENTROID_OFF.Location = new System.Drawing.Point(333, 229);
            this.IDC_RADIO_CENTROID_OFF.Margin = new System.Windows.Forms.Padding(4);
            this.IDC_RADIO_CENTROID_OFF.Name = "IDC_RADIO_CENTROID_OFF";
            this.IDC_RADIO_CENTROID_OFF.Size = new System.Drawing.Size(13, 14);
            this.IDC_RADIO_CENTROID_OFF.TabIndex = 26;
            this.IDC_RADIO_CENTROID_OFF.TabStop = false;
            this.IDC_RADIO_CENTROID_OFF.Click += new System.EventHandler(this.IDC_RADIO_CENTROID_OFF_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            global::Yugamiru.Properties.Resources.SMALL,
            Yugamiru.Properties.Resources.MIDDLE,//GSP-1291
            global::Yugamiru.Properties.Resources.LARGE});
            this.comboBox1.Location = new System.Drawing.Point(333, 50);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(4);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(160, 24);
            this.comboBox1.TabIndex = 27;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // SettingView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(681, 373);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.IDC_RADIO_CENTROID_OFF);
            this.Controls.Add(this.IDC_RADIO_CENTROID_ON);
            this.Controls.Add(this.IDC_RADIO_MUSCLEREPORT_OFF);
            this.Controls.Add(this.IDC_RADIO_MUSCLEREPORT_ON);
            this.Controls.Add(this.IDC_RADIO_SCORE_OFF);
            this.Controls.Add(this.IDC_RADIO_SCORE_ON);
            this.Controls.Add(this.IDC_RADIO_LABEL_OFF);
            this.Controls.Add(this.IDC_RADIO_LABEL_ON);
            this.Controls.Add(this.IDC_RADIO_CENTERLINE_OFF);
            this.Controls.Add(this.IDC_RADIO_CENTERLINE_ON);
            this.Controls.Add(this.IDC_RADIO_ARROW_OFF);
            this.Controls.Add(this.IDC_RADIO_ARROW_ON);
            this.Controls.Add(this.IDCANCEL);
            this.Controls.Add(this.IDOK);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "SettingView";
            this.Text = "SettingView";
            this.Load += new System.EventHandler(this.SettingView_Load);
            this.SizeChanged += new System.EventHandler(this.SettingView_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDCANCEL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RADIO_ARROW_ON)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RADIO_ARROW_OFF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RADIO_CENTERLINE_ON)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RADIO_CENTERLINE_OFF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RADIO_LABEL_ON)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RADIO_LABEL_OFF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RADIO_SCORE_ON)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RADIO_SCORE_OFF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RADIO_MUSCLEREPORT_ON)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RADIO_MUSCLEREPORT_OFF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RADIO_CENTROID_ON)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RADIO_CENTROID_OFF)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox IDOK;
        private System.Windows.Forms.PictureBox IDCANCEL;
        private System.Windows.Forms.PictureBox IDC_RADIO_ARROW_ON;
        private System.Windows.Forms.PictureBox IDC_RADIO_ARROW_OFF;
        private System.Windows.Forms.PictureBox IDC_RADIO_CENTERLINE_ON;
        private System.Windows.Forms.PictureBox IDC_RADIO_CENTERLINE_OFF;
        private System.Windows.Forms.PictureBox IDC_RADIO_LABEL_ON;
        private System.Windows.Forms.PictureBox IDC_RADIO_LABEL_OFF;
        private System.Windows.Forms.PictureBox IDC_RADIO_SCORE_ON;
        private System.Windows.Forms.PictureBox IDC_RADIO_SCORE_OFF;
        private System.Windows.Forms.PictureBox IDC_RADIO_MUSCLEREPORT_ON;
        private System.Windows.Forms.PictureBox IDC_RADIO_MUSCLEREPORT_OFF;
        private System.Windows.Forms.PictureBox IDC_RADIO_CENTROID_ON;
        private System.Windows.Forms.PictureBox IDC_RADIO_CENTROID_OFF;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}