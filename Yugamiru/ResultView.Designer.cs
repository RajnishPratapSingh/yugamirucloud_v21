﻿namespace Yugamiru
{
    partial class ResultView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox_Standing = new System.Windows.Forms.PictureBox();
            this.pictureBox_KneeDown = new System.Windows.Forms.PictureBox();
            this.IDC_BTN_NAMECHANGE = new System.Windows.Forms.PictureBox();
            this.IDC_RemeasurementBtn = new System.Windows.Forms.PictureBox();
            this.IDC_EditBtn = new System.Windows.Forms.PictureBox();
            this.IDC_ScoresheetBtn = new System.Windows.Forms.PictureBox();
            this.IDC_PrintBtn = new System.Windows.Forms.PictureBox();
            this.IDC_BTN_DATASAVE = new System.Windows.Forms.PictureBox();
            this.IDC_MeasurementEndBtn = new System.Windows.Forms.PictureBox();
            this.IDC_BTN_RETURNTOPMENU = new System.Windows.Forms.PictureBox();
            this.IDC_ResetImgBtn = new System.Windows.Forms.PictureBox();
            this.IDC_Mag2Btn = new System.Windows.Forms.PictureBox();
            this.IDC_Mag1Btn = new System.Windows.Forms.PictureBox();
            this.IDC_SLIDER1 = new System.Windows.Forms.TrackBar();
            this.IDC_CommentField = new System.Windows.Forms.RichTextBox();
            this.IDC_ID = new System.Windows.Forms.Label();
            this.IDC_Name = new System.Windows.Forms.Label();
            this.IDC_Gender = new System.Windows.Forms.Label();
            this.IDC_DoB = new System.Windows.Forms.Label();
            this.IDC_Height = new System.Windows.Forms.Label();
            this.IDC_EDIT1 = new System.Windows.Forms.RichTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.IDC_BACKImgBtn = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Standing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_KneeDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_BTN_NAMECHANGE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RemeasurementBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_EditBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_ScoresheetBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_PrintBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_BTN_DATASAVE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_MeasurementEndBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_BTN_RETURNTOPMENU)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_ResetImgBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_Mag2Btn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_Mag1Btn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_SLIDER1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_BACKImgBtn)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox_Standing
            // 
            this.pictureBox_Standing.Location = new System.Drawing.Point(12, 12);
            this.pictureBox_Standing.Name = "pictureBox_Standing";
            this.pictureBox_Standing.Size = new System.Drawing.Size(100, 50);
            this.pictureBox_Standing.TabIndex = 0;
            this.pictureBox_Standing.TabStop = false;
            this.pictureBox_Standing.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox_Standing_Paint);
            this.pictureBox_Standing.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox_Standing_MouseDown);
            this.pictureBox_Standing.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox_Standing_MouseMove);
            this.pictureBox_Standing.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox_Standing_MouseUp);
            // 
            // pictureBox_KneeDown
            // 
            this.pictureBox_KneeDown.Location = new System.Drawing.Point(12, 95);
            this.pictureBox_KneeDown.Name = "pictureBox_KneeDown";
            this.pictureBox_KneeDown.Size = new System.Drawing.Size(100, 50);
            this.pictureBox_KneeDown.TabIndex = 1;
            this.pictureBox_KneeDown.TabStop = false;
            this.pictureBox_KneeDown.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox_KneeDown_Paint);
            this.pictureBox_KneeDown.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox_KneeDown_MouseDown);
            this.pictureBox_KneeDown.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox_KneeDown_MouseMove);
            this.pictureBox_KneeDown.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox_KneeDown_MouseUp);
            // 
            // IDC_BTN_NAMECHANGE
            // 
            this.IDC_BTN_NAMECHANGE.Location = new System.Drawing.Point(12, 308);
            this.IDC_BTN_NAMECHANGE.Name = "IDC_BTN_NAMECHANGE";
            this.IDC_BTN_NAMECHANGE.Size = new System.Drawing.Size(86, 38);
            this.IDC_BTN_NAMECHANGE.TabIndex = 2;
            this.IDC_BTN_NAMECHANGE.TabStop = false;
            this.IDC_BTN_NAMECHANGE.Click += new System.EventHandler(this.IDC_BTN_NAMECHANGE_Click);
            // 
            // IDC_RemeasurementBtn
            // 
            this.IDC_RemeasurementBtn.Location = new System.Drawing.Point(114, 308);
            this.IDC_RemeasurementBtn.Name = "IDC_RemeasurementBtn";
            this.IDC_RemeasurementBtn.Size = new System.Drawing.Size(86, 38);
            this.IDC_RemeasurementBtn.TabIndex = 3;
            this.IDC_RemeasurementBtn.TabStop = false;
            this.IDC_RemeasurementBtn.Click += new System.EventHandler(this.IDC_RemeasurementBtn_Click);
            // 
            // IDC_EditBtn
            // 
            this.IDC_EditBtn.Location = new System.Drawing.Point(206, 308);
            this.IDC_EditBtn.Name = "IDC_EditBtn";
            this.IDC_EditBtn.Size = new System.Drawing.Size(86, 38);
            this.IDC_EditBtn.TabIndex = 4;
            this.IDC_EditBtn.TabStop = false;
            this.IDC_EditBtn.Click += new System.EventHandler(this.IDC_EditBtn_Click);
            // 
            // IDC_ScoresheetBtn
            // 
            this.IDC_ScoresheetBtn.Location = new System.Drawing.Point(298, 308);
            this.IDC_ScoresheetBtn.Name = "IDC_ScoresheetBtn";
            this.IDC_ScoresheetBtn.Size = new System.Drawing.Size(86, 38);
            this.IDC_ScoresheetBtn.TabIndex = 5;
            this.IDC_ScoresheetBtn.TabStop = false;
            this.IDC_ScoresheetBtn.Click += new System.EventHandler(this.IDC_ScoresheetBtn_Click);
            // 
            // IDC_PrintBtn
            // 
            this.IDC_PrintBtn.Location = new System.Drawing.Point(390, 308);
            this.IDC_PrintBtn.Name = "IDC_PrintBtn";
            this.IDC_PrintBtn.Size = new System.Drawing.Size(86, 38);
            this.IDC_PrintBtn.TabIndex = 6;
            this.IDC_PrintBtn.TabStop = false;
            this.IDC_PrintBtn.Click += new System.EventHandler(this.IDC_PrintBtn_Click);
            // 
            // IDC_BTN_DATASAVE
            // 
            this.IDC_BTN_DATASAVE.Location = new System.Drawing.Point(482, 308);
            this.IDC_BTN_DATASAVE.Name = "IDC_BTN_DATASAVE";
            this.IDC_BTN_DATASAVE.Size = new System.Drawing.Size(86, 38);
            this.IDC_BTN_DATASAVE.TabIndex = 7;
            this.IDC_BTN_DATASAVE.TabStop = false;
            this.IDC_BTN_DATASAVE.Click += new System.EventHandler(this.IDC_BTN_DATASAVE_Click);
            // 
            // IDC_MeasurementEndBtn
            // 
            this.IDC_MeasurementEndBtn.Location = new System.Drawing.Point(574, 308);
            this.IDC_MeasurementEndBtn.Name = "IDC_MeasurementEndBtn";
            this.IDC_MeasurementEndBtn.Size = new System.Drawing.Size(86, 38);
            this.IDC_MeasurementEndBtn.TabIndex = 8;
            this.IDC_MeasurementEndBtn.TabStop = false;
            this.IDC_MeasurementEndBtn.Click += new System.EventHandler(this.IDC_MeasurementEndBtn_Click);
            // 
            // IDC_BTN_RETURNTOPMENU
            // 
            this.IDC_BTN_RETURNTOPMENU.Location = new System.Drawing.Point(675, 308);
            this.IDC_BTN_RETURNTOPMENU.Name = "IDC_BTN_RETURNTOPMENU";
            this.IDC_BTN_RETURNTOPMENU.Size = new System.Drawing.Size(86, 38);
            this.IDC_BTN_RETURNTOPMENU.TabIndex = 9;
            this.IDC_BTN_RETURNTOPMENU.TabStop = false;
            this.IDC_BTN_RETURNTOPMENU.Click += new System.EventHandler(this.IDC_BTN_RETURNTOPMENU_Click);
            // 
            // IDC_ResetImgBtn
            // 
            this.IDC_ResetImgBtn.Location = new System.Drawing.Point(783, 308);
            this.IDC_ResetImgBtn.Name = "IDC_ResetImgBtn";
            this.IDC_ResetImgBtn.Size = new System.Drawing.Size(86, 38);
            this.IDC_ResetImgBtn.TabIndex = 10;
            this.IDC_ResetImgBtn.TabStop = false;
            this.IDC_ResetImgBtn.Click += new System.EventHandler(this.IDC_ResetImgBtn_Click);
            // 
            // IDC_Mag2Btn
            // 
            this.IDC_Mag2Btn.Location = new System.Drawing.Point(888, 24);
            this.IDC_Mag2Btn.Name = "IDC_Mag2Btn";
            this.IDC_Mag2Btn.Size = new System.Drawing.Size(43, 38);
            this.IDC_Mag2Btn.TabIndex = 11;
            this.IDC_Mag2Btn.TabStop = false;
            this.IDC_Mag2Btn.Click += new System.EventHandler(this.IDC_Mag2Btn_Click);
            // 
            // IDC_Mag1Btn
            // 
            this.IDC_Mag1Btn.Location = new System.Drawing.Point(888, 245);
            this.IDC_Mag1Btn.Name = "IDC_Mag1Btn";
            this.IDC_Mag1Btn.Size = new System.Drawing.Size(43, 38);
            this.IDC_Mag1Btn.TabIndex = 12;
            this.IDC_Mag1Btn.TabStop = false;
            this.IDC_Mag1Btn.Click += new System.EventHandler(this.IDC_Mag1Btn_Click);
            // 
            // IDC_SLIDER1
            // 
            this.IDC_SLIDER1.Location = new System.Drawing.Point(3, 3);
            this.IDC_SLIDER1.Name = "IDC_SLIDER1";
            this.IDC_SLIDER1.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.IDC_SLIDER1.Size = new System.Drawing.Size(45, 104);
            this.IDC_SLIDER1.TabIndex = 13;
            this.IDC_SLIDER1.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.IDC_SLIDER1.Scroll += new System.EventHandler(this.IDC_SLIDER1_Scroll);
            // 
            // IDC_CommentField
            // 
            this.IDC_CommentField.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDC_CommentField.Location = new System.Drawing.Point(148, 12);
            this.IDC_CommentField.MaxLength = 300;
            this.IDC_CommentField.Name = "IDC_CommentField";
            this.IDC_CommentField.Size = new System.Drawing.Size(100, 96);
            this.IDC_CommentField.TabIndex = 14;
            this.IDC_CommentField.Text = "";
            this.IDC_CommentField.Click += new System.EventHandler(this.IDC_CommentField_Click);
            this.IDC_CommentField.TextChanged += new System.EventHandler(this.IDC_CommentField_TextChanged);
            this.IDC_CommentField.Enter += new System.EventHandler(this.IDC_CommentField_Enter);
            this.IDC_CommentField.KeyDown += new System.Windows.Forms.KeyEventHandler(this.IDC_CommentField_KeyDown);
            this.IDC_CommentField.KeyUp += new System.Windows.Forms.KeyEventHandler(this.IDC_CommentField_KeyUp);
            this.IDC_CommentField.Leave += new System.EventHandler(this.IDC_CommentField_Leave);
            // 
            // IDC_ID
            // 
            this.IDC_ID.AutoSize = true;
            this.IDC_ID.BackColor = System.Drawing.Color.Transparent;
            this.IDC_ID.Location = new System.Drawing.Point(36, 214);
            this.IDC_ID.Name = "IDC_ID";
            this.IDC_ID.Size = new System.Drawing.Size(35, 13);
            this.IDC_ID.TabIndex = 16;
            this.IDC_ID.Text = "label1";
            // 
            // IDC_Name
            // 
            this.IDC_Name.AutoSize = true;
            this.IDC_Name.BackColor = System.Drawing.Color.Transparent;
            this.IDC_Name.Location = new System.Drawing.Point(113, 222);
            this.IDC_Name.Name = "IDC_Name";
            this.IDC_Name.Size = new System.Drawing.Size(35, 13);
            this.IDC_Name.TabIndex = 17;
            this.IDC_Name.Text = "label1";
            // 
            // IDC_Gender
            // 
            this.IDC_Gender.AutoSize = true;
            this.IDC_Gender.BackColor = System.Drawing.Color.Transparent;
            this.IDC_Gender.Location = new System.Drawing.Point(217, 222);
            this.IDC_Gender.Name = "IDC_Gender";
            this.IDC_Gender.Size = new System.Drawing.Size(35, 13);
            this.IDC_Gender.TabIndex = 23;
            this.IDC_Gender.Text = "label1";
            // 
            // IDC_DoB
            // 
            this.IDC_DoB.AutoSize = true;
            this.IDC_DoB.BackColor = System.Drawing.Color.Transparent;
            this.IDC_DoB.Location = new System.Drawing.Point(303, 221);
            this.IDC_DoB.Name = "IDC_DoB";
            this.IDC_DoB.Size = new System.Drawing.Size(35, 13);
            this.IDC_DoB.TabIndex = 19;
            this.IDC_DoB.Text = "label1";
            // 
            // IDC_Height
            // 
            this.IDC_Height.AutoSize = true;
            this.IDC_Height.BackColor = System.Drawing.Color.Transparent;
            this.IDC_Height.Location = new System.Drawing.Point(399, 222);
            this.IDC_Height.Name = "IDC_Height";
            this.IDC_Height.Size = new System.Drawing.Size(35, 13);
            this.IDC_Height.TabIndex = 20;
            this.IDC_Height.Text = "label1";
            // 
            // IDC_EDIT1
            // 
            this.IDC_EDIT1.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IDC_EDIT1.Location = new System.Drawing.Point(335, 45);
            this.IDC_EDIT1.Name = "IDC_EDIT1";
            this.IDC_EDIT1.ReadOnly = true;
            this.IDC_EDIT1.Size = new System.Drawing.Size(100, 96);
            this.IDC_EDIT1.TabIndex = 21;
            this.IDC_EDIT1.Text = "";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.IDC_SLIDER1);
            this.panel1.Location = new System.Drawing.Point(757, 95);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(54, 108);
            this.panel1.TabIndex = 22;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // IDC_BACKImgBtn
            // 
            this.IDC_BACKImgBtn.Location = new System.Drawing.Point(733, 245);
            this.IDC_BACKImgBtn.Name = "IDC_BACKImgBtn";
            this.IDC_BACKImgBtn.Size = new System.Drawing.Size(100, 38);
            this.IDC_BACKImgBtn.TabIndex = 24;
            this.IDC_BACKImgBtn.TabStop = false;
            this.IDC_BACKImgBtn.Click += new System.EventHandler(this.IDC_BACKImgBtn_Click);
            // 
            // ResultView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(924, 355);
            this.Controls.Add(this.IDC_BACKImgBtn);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.IDC_EDIT1);
            this.Controls.Add(this.IDC_Height);
            this.Controls.Add(this.IDC_DoB);
            this.Controls.Add(this.IDC_Gender);
            this.Controls.Add(this.IDC_Name);
            this.Controls.Add(this.IDC_ID);
            this.Controls.Add(this.IDC_CommentField);
            this.Controls.Add(this.IDC_Mag1Btn);
            this.Controls.Add(this.IDC_Mag2Btn);
            this.Controls.Add(this.IDC_ResetImgBtn);
            this.Controls.Add(this.IDC_BTN_RETURNTOPMENU);
            this.Controls.Add(this.IDC_MeasurementEndBtn);
            this.Controls.Add(this.IDC_BTN_DATASAVE);
            this.Controls.Add(this.IDC_PrintBtn);
            this.Controls.Add(this.IDC_ScoresheetBtn);
            this.Controls.Add(this.IDC_EditBtn);
            this.Controls.Add(this.IDC_RemeasurementBtn);
            this.Controls.Add(this.IDC_BTN_NAMECHANGE);
            this.Controls.Add(this.pictureBox_KneeDown);
            this.Controls.Add(this.pictureBox_Standing);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "ResultView";
            this.Text = "ResultView";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ResultView_FormClosed);
            this.Load += new System.EventHandler(this.ResultView_Load);
            this.SizeChanged += new System.EventHandler(this.ResultView_SizeChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ResultView_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.ResultView_KeyUp);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ResultView_MouseUp);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Standing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_KneeDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_BTN_NAMECHANGE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RemeasurementBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_EditBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_ScoresheetBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_PrintBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_BTN_DATASAVE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_MeasurementEndBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_BTN_RETURNTOPMENU)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_ResetImgBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_Mag2Btn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_Mag1Btn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_SLIDER1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_BACKImgBtn)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox_Standing;
        private System.Windows.Forms.PictureBox pictureBox_KneeDown;
        private System.Windows.Forms.PictureBox IDC_BTN_NAMECHANGE;
        private System.Windows.Forms.PictureBox IDC_RemeasurementBtn;
        private System.Windows.Forms.PictureBox IDC_EditBtn;
        private System.Windows.Forms.PictureBox IDC_ScoresheetBtn;
        private System.Windows.Forms.PictureBox IDC_PrintBtn;
        private System.Windows.Forms.PictureBox IDC_BTN_DATASAVE;
        private System.Windows.Forms.PictureBox IDC_MeasurementEndBtn;
        private System.Windows.Forms.PictureBox IDC_BTN_RETURNTOPMENU;
        private System.Windows.Forms.PictureBox IDC_ResetImgBtn;
        private System.Windows.Forms.PictureBox IDC_Mag2Btn;
        private System.Windows.Forms.PictureBox IDC_Mag1Btn;
        private System.Windows.Forms.TrackBar IDC_SLIDER1;
        private System.Windows.Forms.RichTextBox IDC_CommentField;
        private System.Windows.Forms.Label IDC_ID;
        private System.Windows.Forms.Label IDC_Name;
        private System.Windows.Forms.Label IDC_Gender;
        private System.Windows.Forms.Label IDC_DoB;
        private System.Windows.Forms.Label IDC_Height;
        private System.Windows.Forms.RichTextBox IDC_EDIT1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox IDC_BACKImgBtn;
    }
}