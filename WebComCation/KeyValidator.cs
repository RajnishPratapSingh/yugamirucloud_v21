﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using System.Diagnostics;

namespace WebComCation
{
    public partial class KeyValidator : Form
    {
        public static bool IsClose;
        Registry_Handler reg = new Registry_Handler();
        ServerCommunicator web = new ServerCommunicator();
        ServerOutInMsg regmsg = new ServerOutInMsg();
        ServerOutInMsg srvrmsg = new ServerOutInMsg();
        
        string jsonQuery = "";
        keyRequest req;
        public KeyValidator()
        {
            InitializeComponent();
            //

            // regmsg = reg.GetStoredRegistryMsg();
            // srvrmsg = web.SendAndGetResponse();
            jsonQuery = reg.RegToJsonRawMsgToSend(regmsg);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string text = "";
            if (txtLicKey.Text.Trim().Length == 0)
            {
                //   MessageBox.Show("Please provide Activation key", "GSPORT");
                //Edited By Suhana For GSP-450
                MessageBox.Show(Properties.Resources.REQUEST_ACTIVATION_KEY, "Yugamiru");
                //Edited By Suhana For GSP-450
                return;
            }
            textBox1.Clear();

            if (!Utility.IsInternetConnected())
            {
                MessageBox.Show(Properties.Resources.PC_NOT_CONNECTED, "Yugamiru");
                return;
            }
            else
            {
                //Check PC date first, if valid then only allow activation.
                if (!DateTemperingHandler.IsPCDateValidWithNet())
                {
                    MessageBox.Show(Properties.Resources.CANNOT_ACTIVATE_INCORRECT_PC_DATE, "Yugamiru");
                    return;
                }
            }


            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string jsonQuery = "[{\"Computer_id\":\"" + keyRequest.GetComputerID() +
                                    "\",\"Computer_name\":\"" + keyRequest.GetComputerName() +
                                    "\",\"Activation_key\":\"" + txtLicKey.Text + "\",\"Language\":\"" + LicenseValidator.sCurrent_Language.ToLower().Replace("japanese", "japan") + "\"}]"; //Added by Rajnish for GSP - 880 and GSP - 881
 
                 String queryString = jsonQuery;//jsonRawTemplate;//jsonQuery;
                var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(queryString);
                //3.
                JsonSerializer jsonSerializer = new JsonSerializer();
                //4.
                MemoryStream objBsonMemoryStream = new MemoryStream();
                //5.
                Newtonsoft.Json.Bson.BsonWriter bsonWriterObject = new Newtonsoft.Json.Bson.BsonWriter(objBsonMemoryStream);
                //6.
                jsonSerializer.Serialize(bsonWriterObject, studentObject);
                text = queryString;
                byte[] requestByte = objBsonMemoryStream.ToArray();//= Encoding.Default.GetBytes(queryString);



                #region WebRequest

                //Connect to our Yugamiru Web Server
                //WebRequest webRequest = WebRequest.Create("http://gs-demo.jma.website/apioauthdata/index.php/home/activatekeydata");
                WebRequest webRequest = WebRequest.Create(StarterLicense.activateURL);


                webRequest.Method = "POST";
                webRequest.ContentType = "application/json";
                webRequest.ContentLength = requestByte.Length;
                Stream webDataStream = null;
                try
                {
                    webDataStream = webRequest.GetRequestStream();
                    webDataStream.Write(requestByte, 0, requestByte.Length);
                }
                catch (Exception ex)
                {
                    using (var client = new WebClient())
                    {
                        try
                        {
                            using (client.OpenRead("http://clients3.google.com/generate_204"))
                            {
                                //System.Windows.Forms.MessageBox.Show("You are not connected to internet");
                            }
                        }
                        catch (Exception ex1)
                        {
                            System.Windows.Forms.MessageBox.Show(Properties.Resources.PC_NOT_CONNECTED, "Yugamiru");
                            //Added Sumit GSP-775 on 28-Aug-18 START
                            WebComCation.FaultManager.LogIt(ex);
                            //Added Sumit GSP-775 on 28-Aug-18 END

                            //Added Sumit GSP-775 on 28-Aug-18 START
                            WebComCation.FaultManager.LogIt(ex1);
                            //Added Sumit GSP-775 on 28-Aug-18 END
                            return;
                        }
                    }
                }
                string ed = webDataStream.ToString();

                // get the response from our stream

                WebResponse webResponse = webRequest.GetResponse();
                webDataStream = webResponse.GetResponseStream();

                // convert the result into a String
                StreamReader webResponseSReader = new StreamReader(webDataStream);

                //Try Catch added by sumit for GSP-1078-----------START
                //String responseFromServer = webResponseSReader.ReadToEnd();
                String responseFromServer = "";

                try
                {
                    responseFromServer = webResponseSReader.ReadToEnd();
                }
                catch
                {
                    if (!Utility.IsInternetConnected())
                        System.Windows.Forms.MessageBox.Show(Properties.Resources.PC_NOT_CONNECTED, "Yugamiru");

                    return;
                }
                //Try Catch added by sumit for GSP-1078-----------END

                #region License_Expired_Case
                if (responseFromServer.Contains("Trial Expired"))//Trial Expired
                {
                    MessageBox.Show(Properties.Resources.ALREADY_REGISTERED_TRIAL, "Yugamiru");
                    textBox1.Text = Properties.Resources.ALREADY_REGISTERED_TRIAL;
                    return;
                }
                else if (responseFromServer.Contains("invalid") && responseFromServer.Contains("expired"))
                {
                    int n1 = responseFromServer.IndexOf("Message");
                    string p1 = responseFromServer.Substring(n1 + 10);
                    p1 = p1.Replace("\0", "");
                    p1 = p1.Replace(@"\u", "");
                    p1 = p1.Replace(@"�", "");//                    
                    p1 = p1.Replace(@":", "");
                    p1 = p1.Substring(0, 112);

                    //--Added By Rajnish For GSP-768--//
                    LicenseValidator.objfrmCustomMessageBox.lblMessage.Text = Properties.Resources.ACTIVATION_KEY_EXPIRED;
                    LicenseValidator.objfrmCustomMessageBox.tbDetails.Text = Properties.Resources.ACTIVATION_KEY + ": " + WebComCation.LicenseValidator.sActivationKey + Environment.NewLine + Properties.Resources.EXPIRY_DATE + ": " + WebComCation.LicenseValidator.sExpiryDate;
                    LicenseValidator.objfrmCustomMessageBox.btnOK.Focus();
                    LicenseValidator.objfrmCustomMessageBox.TopMost = true;
                    LicenseValidator.objfrmCustomMessageBox.ShowDialog();
                    //----------------------//

                   // MessageBox.Show("Activation Key Expired");//+ p1);//--Commented by Rajnish for GSP-768//
                    textBox1.Text = Properties.Resources.ACTIVATION_KEY_EXPIRED;//+ p1;
                    return;
                    //Application.Exit();
                }
                #endregion


                #region Invalid_License
                else if (responseFromServer.Contains("invalid") || responseFromServer.Contains("Language Invalid"))
                {

                    string holdrenewMsg = LicenseValidator.GetRenewedComputerKey("", "");
                    //Edited By Suhana for GSP-450
                    MessageBox.Show(Properties.Resources.INVALID_LICENSE_KEY, "Yugamiru");
                    //Edited By Suhana For GSP-450
                    // MessageBox.Show("Invalid Activation Key");
                    //Application.Exit();
                    textBox1.Text = Properties.Resources.INVALID_LICENSE_KEY;
                    return;
                }
                #endregion


                #region Valid_License

                string sw = responseFromServer.Replace("?", "");
                sw = sw.Replace("\0", "");
                sw = sw.Replace(@"\u", "");
                sw = sw.Replace(@"�", "");//
                //sw = sw.Replace(sw[7].ToString(), "");//
                sw = sw.Replace("\u0002", "-");
                sw = sw.Replace("\u0016", "");
                sw = sw.Replace("\u0006", "");
                sw = sw.Replace("\u0010", "");

                string CPK = sw;
                string strrm = sw;
                //Edited by Sumit on 16-Nov-18 GSP-918----------START
                //CPK = CPK.Replace("Status-ComputerKey ", "");
                //CPK = CPK.Replace("-LicenseType-S-MessageLicense Key Activated-Keyvalid-Languagevalid", "");//Added by Rajnish for GSP - 880 and GSP - 881
                CPK = CPK.Replace("Status-ComputerKey ", "").Split('-')[0];
                //Edited by Sumit on 16-Nov-18 GSP-918----------START

                #region Store_Computer_Key_Locally

                //Keys.ReadKey();

                //QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();
                //Keys.ActivationKey = txtLicKey.Text.Trim();
                //Keys.ComputerKey = CPK;
                //=========================
                LicenseStatus st = LicenseStatus.Unknown;
                //Fetch locally stored values first
                string LicenseKey = txtLicKey.Text; // Registry_Handler.GetActivationKeyFromReg();
                string ComputerKey = CPK;//Registry_Handler.GetComputerKeyFromReg();
                string computerID = keyRequest.GetComputerID();
                QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();
                //store keys first.
                string ak = LicenseKey; //Validation Key
                string ck = ComputerKey; // Computer Key

                //Added by Sumit GSP-775 on 04-Sep-18 1322 ---START
                ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                    , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
                ql.PublicKey = ProductInfo.ProductEncryptionKey;
                string IsKeyDeleted = string.Empty;
                ql.DeleteKeys(out IsKeyDeleted);
                //Added by Sumit GSP-775 on 04-Sep-18 1322 ---END

                ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                    , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
                ql.PublicKey = ProductInfo.ProductEncryptionKey;
                ql.StoreKeysOptions = QlmLicenseLib.EStoreKeysOptions.EStoreKeysPerMachine;//sumit testing july
                ql.EvaluationPerUser = false;//Sumit testing July
                ql.FavorMachineLevelLicenseKey = true;//Sumit testing July
                ql.DefaultWebServiceUrl = Environment.UserName;

                //Added by Sumit for GSP-821 on 24-Sep-2018-----------START
                //ql.ProxyUser = Environment.UserName;
                ql.ProxyUser = DateTime.Now.ToString("yyyy MM dd HH:mm:ss").Replace(" ", "").Replace(":", "");
                //Added by Sumit for GSP-821 on 24-Sep-2018-----------END

                ql.StoreKeys(ak, ck);
                ///Added by Sumit GSP-705 ----START
                SecureAccessManager.AccessRightsHandler.SetLicenseReleaseableForAll();
                //Added by Sumit GSP-705 ----END
                #endregion


                #region Not_In_Use
                //if (strrm.Contains("invalid"))
                //{
                //    //keyStatus = LicenseStatus.Invalid;
                //    strrm = strrm.Replace("invalid", "");
                //    System.Windows.Forms.MessageBox.Show("Key is Invalid");
                //    textBox1.Text = "Key is Invalid";
                //    return;
                //}
                //else if (strrm.Contains("valid"))
                //{
                //keyStatus = LicenseStatus.Valid;
                //msg.Status = LicenseStatus.Valid.ToString();
                //msg.keyStatus = LicenseStatus.Valid.ToString();
                //msg.ServerMessage = "License Key is valid";
                //strrm = strrm.Replace("valid", "");
                //                }
                //                //System.Windows.Forms.MessageBox.Show(strrm);
                //                //ServerOutInMsg msg = new ServerOutInMsg();

                //                try
                //                {
                //                    //msg.ActivationDate = actdt;//dcValuePairs["ActivationDate"];
                //                    //msg.ComputerID = CPK;//dcValuePairs["ComputerID"];
                //                    //msg.ComputerName = cpname;//dcValuePairs["ComputerName"];
                //                    //msg.EndDate = eddt; //dcValuePairs["EndDate"];
                //                    //msg.LicenseKey = txtLicKey.Text;// dcValuePairs["LicenseKey"];
                //                    //msg.ServerMessage = strrm;//dcValuePairs["Message"];
                //                    //msg.StartDate = Stdt;//dcValuePairs["StartDate"];
                //                    //msg.Status = keyStatus.ToString();//dcValuePairs["Status"];
                //                    //msg.keyStatus = keyStatus.ToString();//dcValuePairs["keyStatus"];
                //                    //msg.LastRunDate = Utility.DateTimeToString(DateTime.Now); //dcValuePairs["LastRunDate"];
                //                    //Registry_Handler.SaveToRegistry(msg);
                //                }
                #endregion
                //                catch (Exception ex)
                //                {
                //                    throw new Exception("Message from server is not in correct Format" + Environment.NewLine + ex.Message);

                //                }
                #endregion

                textBox1.Text = Properties.Resources.KEY_ACTIVATED;//commented by Sumit Exception At Activation after this line change
                //Edited By Suhana For GSP-450
                System.Windows.Forms.MessageBox.Show(Properties.Resources.KEY_ACTIVATED, "Yugamiru");
                //Added by Sumit GSP-705 ----START
                SecureAccessManager.AccessRightsHandler.SetLicenseReleaseableForAll();
                //Added by Sumit GSP-705 ----END

                //System.Windows.Forms.MessageBox.Show("Application activated successfully", "GSPORT");
                //Edited By Suhana For GSP-450

                //Added by Sumit GSP-821 on 25-Sep-2018-----START
                DateTemperingHandler.SavePresentTimeStamp();
                //Added by Sumit GSP-821 on 25-Sep-2018-----END
                Utility.SavePresentTimeStamp(); //Added By Suhana

                //Commenting progressbar GSP-1174-----START
                //if (LicenseValidator.IsKVFromLaunch == true)//--Added by Rajnish For GSP-782--//
                //{
                //    Process myp = new Process();
                //    myp.StartInfo.FileName = Application.StartupPath + "\\yugamiruPGB.exe";
                //    myp.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
                //    myp.Start();
                //}
                //Commenting progressbar GSP-1174-----END
                //// close everything
                webResponseSReader.Close();
                webResponse.Close();
                webDataStream.Close();
                this.Close();
                #endregion

            }
            catch (WebException we) //Any how key is invalid
            {
                #region Commented_Out
                //using (WebResponse response = we.Response)
                //{
                //    HttpWebResponse httpResponse = (HttpWebResponse)response;
                //    Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                //    string messageFromServer = string.Empty;
                //    using (Stream data = response.GetResponseStream())
                //    using (var reader = new StreamReader(data))
                //    {
                //        // text is the response body
                //        messageFromServer = reader.ReadToEnd();
                //    }

                //    // You now have the JSON text in the responseFromServer variable, use it :)
                //    string[] responseFromServer_1 = messageFromServer.Split(','); //.Replace(',', '\\');
                //    string sl = "";
                //    string servermsg = string.Empty;
                //    foreach (string s in responseFromServer_1)
                //    {
                //        sl += s + Environment.NewLine;

                //        if (s.Contains("\"errormessage\":"))
                //        {
                //            servermsg = s.Replace("\"errormessage\":", "");
                //        }

                //    }
                //    if (servermsg.Trim().Length > 0)
                //        MessageBox.Show(servermsg.Replace("\"", ""));
                //    sl = sl.Replace("\"", " ");
                //    sl = sl.Replace("{", " ");
                //    sl = sl.Replace("}", " ");
                //    sl = sl.Replace("[", " ");
                //    sl = sl.Replace("]", " ");

                //Message to show to user.
                //string msgResponse = responseFromServer_1[5];                   
                #endregion
                textBox1.Text = textBox1.Text + Environment.NewLine + we.Message + Environment.NewLine + we.StackTrace;
                //Added Sumit GSP-775 on 28-Aug-18 START
                WebComCation.FaultManager.LogIt(we);
                //Added Sumit GSP-775 on 28-Aug-18 END

            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void KeyValidator_Load(object sender, EventArgs e)
        {
            IsClose = false;
        }

        private void txtLicKey_TextChanged(object sender, EventArgs e)
        {
            textBox1.Clear();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            //IsClose = true;
            //this.Close();
            if (textBox1.Text.Contains(Properties.Resources.KEY_ACTIVATED))
            {
                //let the application launch
            }
            else
            {
                try
                {
                    Cleaner.ScrosheetCleaner(); // Sumit GSP-1299
                }
                catch (FileLoadException elf)
                {
                    FaultManager.LogIt(elf);
                }

                catch (FieldAccessException efa)
                {
                    FaultManager.LogIt(efa);
                }
                catch (IOException eio)
                {
                    FaultManager.LogIt(eio);
                } // Sumit GSP-1299
                Environment.Exit(1);
            }
        }

        private void KeyValidator_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (textBox1.Text.Contains(Properties.Resources.KEY_ACTIVATED))
            {
                //let the application launch
            }
            else
            {
                try
                {
                    Cleaner.ScrosheetCleaner(); // Sumit GSP-1299
                }
                catch (FileLoadException elf)
                {
                    FaultManager.LogIt(elf);
                }               
                
                catch (FieldAccessException efa)
                {
                    FaultManager.LogIt(efa);
                }
                catch (IOException eio)
                {
                    FaultManager.LogIt(eio);
                }
                Cleaner.ScrosheetCleaner(); // Sumit GSP-1299
                Environment.Exit(1);
            }
        }
    }
}
