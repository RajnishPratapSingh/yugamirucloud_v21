﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Diagnostics;
using Newtonsoft.Json.Bson;
using Microsoft.Win32;

namespace WebComCation
{
    public partial class frmReleaseLicense : Form
    {
        ServerOutInMsg msgSrvr;
        Registry_Handler reg;
        QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();
        public frmReleaseLicense()
        {
            InitializeComponent();
        }

        private void frmReleaseLicense_Load(object sender, EventArgs e)
        {
            try
            {
                //reg = new Registry_Handler();
                //msgSrvr = reg.GetStoredRegistryMsg();
                string LicenseKey = string.Empty;
                string ComputerKey = string.Empty;
                ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
              , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
                ql.PublicKey = ProductInfo.ProductEncryptionKey;
                ql.StoreKeysOptions = QlmLicenseLib.EStoreKeysOptions.EStoreKeysPerMachine;
                ql.EvaluationPerUser = false;//Sumit testing July
                ql.FavorMachineLevelLicenseKey = true;//Sumit testing July
                ql.DefaultWebServiceUrl = Environment.UserName;
                //ql.ProxyUser = Environment.UserName;//Sumit testing July
                ql.ReadKeys(ref LicenseKey, ref ComputerKey);
                
                txtLicense.Text = LicenseKey;//msgSrvr.LicenseKey;
                if (LicenseKey.Trim() == "")
                    btnRelease.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,"GSPORT");
                //Added Sumit GSP-775 on 28-Aug-18 START
                WebComCation.FaultManager.LogIt(ex);
                //Added Sumit GSP-775 on 28-Aug-18 END
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public string msgReleaseLicense= "License released successfully From this PC.";
        public string msgReleaseNetProblem = Properties.Resources.PC_NOT_CONNECTED + Environment.NewLine + Properties.Resources.KEY_NOT_RELEASED;
        private string ReleaseLicense()
        {
            // check if offline
            using (var client = new WebClient())
            {
                try
                {
                    using (client.OpenRead("http://clients3.google.com/generate_204"))
                    {
                        //System.Windows.Forms.MessageBox.Show("You are not connected to internet");
                    }
                }
                catch (Exception ex1)
                {
                    //Added Sumit GSP-775 on 28-Aug-18 START
                    WebComCation.FaultManager.LogIt(ex1);
                    //Added Sumit GSP-775 on 28-Aug-18 END
                    MessageBox.Show(msgReleaseNetProblem,"GSPORT");
                    //---------For Debug Only System.Windows.Forms.MessageBox.Show(msgReleaseNetProblem);//("You are not connected to internet." + Environment.NewLine + "For releasing a license internet connectivity is required.");
                    return "NOINTERNET";//"Connection Error";

                }
            }
            //connectivity test done





            string msgDel = string.Empty;
            string LK = "";
            string CK = "";
            ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
               , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
            ql.PublicKey = ProductInfo.ProductEncryptionKey;
            ql.StoreKeysOptions = QlmLicenseLib.EStoreKeysOptions.EStoreKeysPerMachine;
            ql.EvaluationPerUser = false;//Sumit testing July
            ql.FavorMachineLevelLicenseKey = true;//Sumit testing July
            ql.DefaultWebServiceUrl = Environment.UserName;
            //ql.ProxyUser = Environment.UserName;//Sumit testing July
            ql.ReadKeys(ref LK, ref CK);
            txtLicense.Text = LK;
            ql.DeleteKeys();
            ql.DeleteKeys(out msgDel);
            ql.Dispose();


            //Section Added by sumit for GSP-1059-----------START
            if (LicenseReleased().Trim().Length > 0)
            {
                return "action denied";
            }
            //GSP-1059-----------END


            if (msgDel.Length == 0)
                msgDel = msgReleaseLicense;
            //MessageBox.Show(msgDel);
            //return msgDel;
            string retMsg = msgReleaseLicense;// "License released successfully From this system.";
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string jsonQuery = "[{\"Computer_id\":\"" + keyRequest.GetComputerID() +
                                    "\",\"Computer_name\":\"" + keyRequest.GetComputerName() +
                                    "\",\"Activation_key\":\"" + txtLicense.Text + "\"}]";
                String queryString = jsonQuery;//jsonRawTemplate;//jsonQuery;
                var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(queryString);
                //3.
                JsonSerializer jsonSerializer = new JsonSerializer();
                //4.
                MemoryStream objBsonMemoryStream = new MemoryStream();
                //5.
                Newtonsoft.Json.Bson.BsonWriter bsonWriterObject = new Newtonsoft.Json.Bson.BsonWriter(objBsonMemoryStream);
                //6.
                jsonSerializer.Serialize(bsonWriterObject, studentObject);
                // text = queryString;
                byte[] requestByte = objBsonMemoryStream.ToArray();//= Encoding.Default.GetBytes(queryString);



                #region WebRequest


                //WebRequest webRequest = WebRequest.Create("http://gs-demo.jma.website/apioauthdata/index.php/home/releasedata");
                WebRequest webRequest = WebRequest.Create(StarterLicense.releaseURL);
                webRequest.Method = "POST";
                webRequest.ContentType = "application/json";
                webRequest.ContentLength = requestByte.Length;
                Stream webDataStream = webRequest.GetRequestStream();
                webDataStream.Write(requestByte, 0, requestByte.Length);

                string ed = webDataStream.ToString();

                // get the response from our stream

                WebResponse webResponse = webRequest.GetResponse();
                webDataStream = webResponse.GetResponseStream();

                // convert the result into a String
                StreamReader webResponseSReader = new StreamReader(webDataStream);
                String responseFromServer = webResponseSReader.ReadToEnd();
                #endregion                    
                btnRelease.Enabled = false;
                //btnCancel.Text = "Close";
            }
            catch(Exception ex)
            {

                //retMsg = "Cannot release this key"+Environment.NewLine+ ex.Message; required only for debug
                //btnRelease.Text = "Cancel";
                //MessageBox.Show(retMsg);

                //Added Sumit GSP-775 on 28-Aug-18 START
                WebComCation.FaultManager.LogIt(ex);
                //Added Sumit GSP-775 on 28-Aug-18 END

            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
            return retMsg;
        }

        private void btnRelease_Click(object sender, EventArgs e)
        {
            string msgDlg = ReleaseLicense();


            //added by sumit GSP-1059------START
            if (msgDlg == "action denied")
            {
                MessageBox.Show(Properties.Resources.KEY_NOT_RELEASED + Environment.NewLine + Properties.Resources.UNAUTHORISED_RELEASED, "Action denied", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            //added by sumit GSP-1059------END



            if (msgDlg != "NOINTERNET")
            {
                QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();
                ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                    , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
                ql.PublicKey = ProductInfo.ProductEncryptionKey;
                ql.StoreKeysOptions = QlmLicenseLib.EStoreKeysOptions.EStoreKeysPerMachine;//testing sumit july
                ql.EvaluationPerUser = false;//Sumit testing July
                ql.FavorMachineLevelLicenseKey = true;//Sumit testing July
                ql.DefaultWebServiceUrl = Environment.UserName;
                //ql.ProxyUser = Environment.UserName;//Sumit testing July
                //ql.StoreKeys("X", "Y");
                //bool one;bool two;string thr = string.Empty; //Sumit Testing

                string recheck = ReleaseLicense();
                string released = LicenseReleased();
                if (released.Trim().Length == 0)
                {
                    if (recheck.Trim().ToUpper() == "NOINTERNET")
                    {
                       MessageBox.Show(Properties.Resources.PC_NOT_CONNECTED + Environment.NewLine + Properties.Resources.KEY_NOT_RELEASED, "GSPORT");
                    }
                    else
                    {
                        MessageBox.Show(recheck, "GSPORT");
                    }
                }
                else
                {

                    MessageBox.Show(Properties.Resources.KEY_NOT_RELEASED + Environment.NewLine + Properties.Resources.UNAUTHORISED_RELEASED, "Action denied", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //    this.Close();
                    //Added by sumit for GSP-699 on 31-July-18----------START

                    //try
                    //{
                    //    //ProcessStartInfo ykr = new ProcessStartInfo();
                    //    //ykr.Verb = "runas";
                    //    //ykr.FileName = Application.StartupPath + "\\ActivationKeyReleaseHelper.exe";
                    //    //Process.Start(ykr);
                    //    //MessageBox.Show("Activation key released successfully from this PC");
                    //    //Environment.Exit(1);
                    //    //Application.Exit();
                    //}
                    //catch (Exception ex)

                    //{
                    //    MessageBox.Show("Key not released! " + Environment.NewLine + "Loggedin user is not authorised to release license key from this PC. Contact your system admin", "Action denied", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //    this.Close();
                    //}
                    //Added by sumit for GSP-699 on 31-July-18----------END



                    return;
                }

                string a = string.Empty;
                string b = string.Empty;
                ql.ReadKeys(ref a, ref b);

                ql.ProxyUser = "RELEASED";
                
                ql.DeleteKeys();

            }

            if (msgDlg != "NOINTERNET")
            {
                //this.Close();
                //Edited by sumit GSP-1194 GSP-1212------START
                //Application.Exit();// 
                Cleaner.ScrosheetCleaner(); // Sumit GSP-1299
                Environment.Exit(1);
                //Edited by sumit GSP-1194 GSP-1212------END
            }
            //Environment.Exit(1);
        }
        public string LicenseReleased()
        {
            QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();
            ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
            ql.PublicKey = ProductInfo.ProductEncryptionKey;
            string ak = string.Empty;
            string ck = string.Empty;

            ql.ReadKeys(ref ak, ref ck);
            return ak;
        }
        //Added Function by sumit for GSP-1082
        public static string DeleteKey()
        {
            QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();
            ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
            ql.PublicKey = ProductInfo.ProductEncryptionKey;
            string ak = string.Empty;
            string ck = string.Empty;

            //ql.ReadKeys(ref ak, ref ck);
            ql.DeleteKeys();


            //if(ak.Trim().Length>0)
            //{
            //    LicenseValidator.ActiavteLicense("", ak);
            //}

            return ak;
        }
    }
}
