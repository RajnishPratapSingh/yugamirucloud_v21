﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Security.Principal;
using System.Security.AccessControl;


namespace SecureAccessManager
{
    public static class AccessRightsHandler
    {
        /// <summary>
        /// Function Implemented By Sumit GSP-705
        /// This function will make license releaseable for all OS users except guest account.
        /// </summary>
        /// <returns></returns>
        public static bool SetLicenseReleaseableForAll()
        {
            bool done = false;
            //First we need to find the license file full path.
            //Universal Fixed License File Name is '{1d683c4c-a831-40f0-8301-726f010f5ee9}'
            IEnumerable<string> files = Directory.EnumerateFiles(
            //Directory.GetCurrentDirectory(),
            Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData)+@"\IsolatedStorage\",
           "{1d683c4c-a831-40f0-8301-726f010f5ee9}",
           SearchOption.AllDirectories);

            var myFileInfo = new FileInfo(files.First());
            List<string> licFilePath = new List<string>();
            licFilePath.Add(myFileInfo.FullName);
            try
            {
                SetUserPreviliges(licFilePath);
                done = true;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.StackTrace);
                done = false;
            }
            return done;
        }



        /// <summary>
        /// Idealy this function should be called only once. 
        /// In furture may be chnged if new user added on OS after yugamiru installation
        /// </summary>
        /// <param name="filesFullPath">List of all the files with full path. These files will be granted with elevated permission covers windows7 to most latest (windows10)</param>
        private static void SetUserPreviliges(List<string> filesFullPath)
        {
            try
            {
                foreach (string file in filesFullPath)
                {
                    AddFileSecurity(file, "", FileSystemRights.FullControl, AccessControlType.Allow);
                }
            }
            catch (SecurityException e)
            {
                throw e;
            }
        }

        // Adds an ACL entry on the specified file for the specified account.
        private static void AddFileSecurity(/*SecurityIdentifier fileName*/string fileName, string account,
            FileSystemRights rights, AccessControlType controlType)
        {
            // Get a FileSecurity object that represents the
            // current security settings.
            FileSecurity fSecurity = File.GetAccessControl(fileName);
            fSecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null),
                rights, controlType));
            // Set the new access settings.
            try
            {
                File.SetAccessControl(fileName, fSecurity);
            }
            catch (SecurityException es)
            {
                throw es;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
