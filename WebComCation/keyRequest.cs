﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using System.Management.Instrumentation;
using System.Diagnostics;

namespace WebComCation
{
    public class keyRequest
    {
        string computer_ID { get; set; }// = "C1A5448A50577243E4FB379B5BEBEA26C239F3C0";// System.Environment.MachineName;// " ";// string.Empty;
        string computer_Name { get; set; }//= "NewSumit-PC";// string.Empty;
        string activation_key { get; set; }// = "B3H20Z0E009GA11Q8K893S1SVWTQQZ3";// string.Empty;
        static string globalCompID = "";
        static string CompName = "";

        public keyRequest(string activationKey)
        {
           
        }
        static keyRequest()
        {
            try
            {
                CompName = Environment.MachineName;
                ManagementClass mc = new ManagementClass("win32_processor");
                ManagementObjectCollection moc = mc.GetInstances();
                String Id = String.Empty;

                //Added Later START

                string drive = "C";
                ManagementObject dsk = new ManagementObject(
                    @"win32_logicaldisk.deviceid=""" + drive + @":""");
                dsk.Get();
                string volumeSerial = dsk["VolumeSerialNumber"].ToString();

                //--Commented By Rajnish on 15 june 18--//
                //string mbInfo = String.Empty;
                //ManagementScope scope = new ManagementScope("\\\\" + Environment.MachineName + "\\root\\cimv2");
                //scope.Connect();
                //ManagementObject wmiClass = new ManagementObject(scope, new ManagementPath("Win32_BaseBoard.Tag=\"Base Board\""), new ObjectGetOptions());

                //foreach (PropertyData propData in wmiClass.Properties)
                //{
                //    if (propData.Name == "SerialNumber")
                //    {
                //        //mbInfo = String.Format("{0,-25}{1}", propData.Name, Convert.ToString(propData.Value).Replace(@"/", "").Replace(@"\", "");
                //        mbInfo = Convert.ToString(propData.Value).Replace(@"/", "").Replace(@"\", "");
                //        break;
                //    }
                //}
                //--------------------------------------//

                //Added Later END


                //--Added by Rajnish On 30-05-2018--GSP-643--//
                string uuid = string.Empty;
                ManagementClass mgc = new ManagementClass("Win32_ComputerSystemProduct");
                ManagementObjectCollection mgoc = mgc.GetInstances();

                foreach (ManagementObject mgo in mgoc)
                {
                    uuid = mgo.Properties["UUID"].Value.ToString().Replace("-", "");
                    break;
                }


                foreach (ManagementObject mo in moc)
                {
                    //--commeted by Rajnish on 30-0502018--//
                    //globalCompID = mo.Properties["processorID"].Value.ToString() + volumeSerial.Trim() + mbInfo;
                    //-------------------------------------//
                    globalCompID = mo.Properties["processorID"].Value.ToString() + volumeSerial.Trim() + uuid;
                    break;
                }
                //-------------------------------//
            }
            catch(UnauthorizedAccessException aex)
            {
                //Added by sumit for GSP-709 ----------START
                System.Windows.Forms.MessageBox.Show(Properties.Resources.USER_NOT_ALLOWED +
                    Environment.NewLine +
                    Properties.Resources.CONTACT_ADMIN, "Yugamiru Cloud: Access Denied", 
                    System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                //Close ProcessBar if running
                Process[] ps = Process.GetProcesses();
                foreach (Process kp in ps)
                {
                    if (kp.ProcessName.ToUpper().Contains("YUGAMIRUPGB"))
                    {
                        kp.CloseMainWindow();
                    }
                }
                //


                //Added Sumit GSP-775 on 28-Aug-18 START
                WebComCation.FaultManager.LogIt(aex);
                //Added Sumit GSP-775 on 28-Aug-18 END

                //System.Windows.Forms.Application.Exit();
                Cleaner.ScrosheetCleaner(); // Sumit GSP-1299
                System.Environment.Exit(0);
                //GSP-709 -----END

            }
            catch(Exception ex)
            {
                //Added by sumit for GSP-709 ----------START
                System.Windows.Forms.MessageBox.Show(Properties.Resources.USER_NOT_ALLOWED +
                    Environment.NewLine +
                    Properties.Resources.CONTACT_ADMIN, "Yugamiru Cloud: Access Denied",
                    System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                //Close ProcessBar if running
                Process[] ps = Process.GetProcesses();
                foreach (Process kp in ps)
                {
                    if (kp.ProcessName.ToUpper().Contains("YUGAMIRUPGB"))
                    {
                        kp.CloseMainWindow();
                    }
                }
                //


                //Added Sumit GSP-775 on 28-Aug-18 START
                WebComCation.FaultManager.LogIt(ex);
                //Added Sumit GSP-775 on 28-Aug-18 END


                //System.Windows.Forms.Application.Exit();
                Cleaner.ScrosheetCleaner(); // Sumit GSP-1299
                System.Environment.Exit(0);
                //GSP-709 -------------END
            }
        }
        public static string GetComputerID()
        {
            return globalCompID;
        }
        public static string GetComputerName()
        {
            return CompName;
        }
    }
   
}
