﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;
using QlmLicenseLib;
using System.Diagnostics;
using System.Runtime.CompilerServices;


namespace WebComCation
{
    public static class LicenseValidator
    {
        //Testing the bitbucket sync
        public static bool IsCallFromBC = false;//--Added by Rajnish For Background License check--//
        public static bool IsFirstTimeCheck = false;//--Added by Rajnish To Handle GSP-771--//existing
        public static bool IsKVFromLaunch = false;//--Added by Rajnish For GSP-782--//
        public static bool IsFirstTimeLanguageInvalid = true;//Added by Rajnish for GSP - 880 and GSP - 881        
        static Registry_Handler rh = new Registry_Handler();
        static ServerOutInMsg valueSetReg = new ServerOutInMsg();
        static ServerOutInMsg valueSetServer = new ServerOutInMsg();
        static ServerCommunicator sc = new ServerCommunicator();
        public static string showRenewDate; // Added By Suhana

        //--Added by Rajnish For GSP-768--//
        public static frmCustomMessageBox objfrmCustomMessageBox = new frmCustomMessageBox();
        public static string sActivationKey { get; set; }
        public static string sActivationDate { get; set; }
        public static string sExpiryDate { get; set; }

        //--------------------------------//
        public static string sCurrent_Language { get; set; } //--Added by Rajnish For GSP-880 & GSP-881

        #region Added by Sumit on 25-September-2018------GSP-821
        public static string pcTimeStamp = "";
        public static string pcValue = "";
        public static string SavedTimeStamp = "";
        public static string SavedValue = "";
        #endregion

        #region Sumit on 10-Oct-18 GSP-858 (Cloud DB)
        public static string ActivationKey = string.Empty;
        #endregion



        public static LicenseStatus IsResponseValid(ServerOutInMsg registryMSG, ServerOutInMsg srverMSG)
        {
            LicenseStatus lt = LicenseStatus.Unknown;
            //if(registryMSG.ActivationDate==srverMSG.ActivationDate &&
            //    registryMSG.ComputerID == srverMSG.ActivationDate &&
            //    registryMSG.ComputerName == srverMSG.ActivationDate &&
            //        registryMSG.EndDate == srverMSG.ActivationDate &&
            //    registryMSG.LicenseKey == srverMSG.ActivationDate &&
            //    registryMSG.ServerMessage == srverMSG.ActivationDate &&
            //    registryMSG.StartDate == srverMSG.ActivationDate &&
            //    registryMSG.Status  == srverMSG.ActivationDate)
            if (srverMSG.Status.ToUpper().Contains("invalid".ToUpper()))
            {
                lt = LicenseStatus.Invalid;
            }
            else
            {
                lt = LicenseStatus.Valid;
            }


            return lt;
        }
        public static LicenseStatus IsDateValid(ServerOutInMsg registryMSG)
        {
            LicenseStatus lstt = LicenseStatus.Unknown;
            if (registryMSG.EndDate == null || registryMSG.EndDate.Length == 0)
            {
                registryMSG.EndDate = Utility.DateTimeToString(DateTime.Now.AddHours(1));
            }
            DateTime dtEndCheck = Utility.StringToDateTime(registryMSG.EndDate);
            DateTime dtLastRun = (Registry_Handler.GetLastRunDateFromReg());
            DateTime dt = DateTime.Now;
            bool isDateCheckOK = false;
            if (Utility.IsFirstDateGreaterOrEqual(dtEndCheck, dt))
            {
                //lstt = LicenseStatus.Valid;
                isDateCheckOK = true;
            }
            else
            {
                //lstt = LicenseStatus.Invalid;
                isDateCheckOK = false;
            }
            if (Utility.IsFirstDateGreaterOrEqual(dtLastRun, DateTime.Now.AddDays(0)))
            {
                isDateCheckOK = false;
                //Edired By Suhana for GSP-450
                System.Windows.Forms.MessageBox.Show(Properties.Resources.PC_INVALID_DATE, "GSPORT");
                // System.Windows.Forms.MessageBox.Show("PC Date is Invalid", "GSPORT");
                //Edired By Suhana for GSP-450
                Cleaner.ScrosheetCleaner(); // Sumit GSP-1299
                //Application.Exit();
                System.Environment.Exit(1);
                //throw new Exception(" System Date is Invalid");
            }





            //Check system date
            if (isDateCheckOK)
            {
                return LicenseStatus.Valid;
            }
            else
            {
                return LicenseStatus.Invalid;
            }
        }
        public static LicenseValidationResult IsPresentStoredLicenseValid()
        {

            LicenseValidationResult result = new LicenseValidationResult();
            result.status = LicenseStatus.Valid;
            valueSetReg = rh.GetStoredRegistryMsg();
            LicenseStatus lst = LicenseStatus.Valid;
            //If App running first time.
            if (valueSetReg.ActivationDate == "FirstTime")
            {
                result.Message = "FirstTime";
                result.status = LicenseStatus.Valid;
                return result;
            }



            //Check last run basis if system date is valid



            try
            {
                if (IsDateValid(valueSetReg) != LicenseStatus.Valid && lst == LicenseStatus.Invalid)
                {
                    //throw new Exception("system date is not valid");
                    result.Message = "PC date is not valid";
                    result.status = LicenseStatus.Invalid;
                    return result;
                }
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message + Environment.NewLine + ex.StackTrace);
                //Added Sumit GSP-775 on 28-Aug-18 START
                WebComCation.FaultManager.LogIt(ex);
                //Added Sumit GSP-775 on 28-Aug-18 END
                Application.Exit();
            }
            if (valueSetReg.LicenseKey == null || valueSetReg.LicenseKey.Length == 0)
            {
                //License not found It may be due to the application on the  is running first time.
                //Launch License Wizard and ask user for License Key
                //System.Windows.Forms.MessageBox.Show("No valid License found for the application");

                result.Message = "No valid License found for the application.";// + Environment.NewLine + "Do you want to provide a valid Activation key?";
                result.status = LicenseStatus.Invalid;
                //return LicenseStatus.Invalid;
                return result;
            }



            valueSetServer = sc.SendAndGetResponse(valueSetReg.LicenseKey);


            if (/*result.status!=LicenseStatus.Valid && */valueSetServer.keyStatus.ToUpper().Trim().Contains("INVALID"))
            {
                //lst = LicenseStatus.Invalid;
                result.Message = "Activation key is Invalid";
                result.status = LicenseStatus.Invalid;

                //throw new Exception("License key is not valid");
            }

            if (!(valueSetServer.LicenseKey != null || valueSetServer.LicenseKey.Trim().ToUpper() != valueSetReg.LicenseKey.Trim().ToUpper() || lst != LicenseStatus.Valid))
            {
                //throw new Exception("Application's License is not valid");
                result.Message = "Activation key is not valid";
                result.status = LicenseStatus.Invalid;
            }

            if (result.Message == null)
            {
                result.Message = "";
            }

            // return lst;
            return result;
        }

        public static void HandleInvalidLicenseEvent(LicenseValidationResult rs)
        {
            if (rs.status != LicenseStatus.Valid)
            {
                //DialogResult dr = System.Windows.Forms.MessageBox.Show(rs.Message + Environment.NewLine + "Do you want to provide a Valid Activation Key?", "GSPORT",
                //    MessageBoxButtons.YesNo);
                //if (dr == DialogResult.No)
                //{
                //    System.Windows.Forms.Application.Exit();
                //}
                //else if (dr == DialogResult.Yes)
                //{
                //    KeyValidator kv = new KeyValidator();
                //    kv.ShowDialog();
                //}
                KeyValidator kv = new KeyValidator();
                kv.ShowDialog();

                //Added by Sumit GSP-782 For closing the animation progress bar------------START
                if (IsCallFromBC)
                {
                    System.Diagnostics.Process[] allp = System.Diagnostics.Process.GetProcesses();
                    foreach (System.Diagnostics.Process p in allp)
                    {
                        if (p.ProcessName.ToUpper().Contains("yugamiruPGB".ToUpper()))
                        {
                            p.CloseMainWindow();
                        }
                    }
                }
                //Added by Sumit GSP-782 For closing the animation progress bar------------END





            }
        }

        #region Not Needed Function
        //public static LicenseStatus ValidateLicenseAtLogonQlm()
        //{
        //    LicenseStatus ret = LicenseStatus.Unknown;
        //    //===============DateValidation
        //    ServerOutInMsg dateMessage = new ServerOutInMsg();
        //    //=================now read here the end date and from stored CK=======VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV






        //    //=============================




        //    //fetch CK CN and AK from system
        //    Keys.ReadKey();
        //    string CK = Keys.ComputerKey;
        //    string AK = Keys.ActivationKey;
        //    string endDate = Keys.EndDate;

        //    //=================
        //    Cursor.Current = Cursors.WaitCursor;

        //    string jsonQuery = "[{\"Computer_id\":\"" + keyRequest.GetComputerID() +
        //                        "\",\"Computer_name\":\"" + keyRequest.GetComputerName() +
        //                        "\",\"Activation_key\":\"" + AK + "\"}]";
        //    String queryString = jsonQuery;//jsonRawTemplate;//jsonQuery;
        //    var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(queryString);
        //    //3.
        //    JsonSerializer jsonSerializer = new JsonSerializer();
        //    //4.
        //    MemoryStream objBsonMemoryStream = new MemoryStream();
        //    //5.
        //    Newtonsoft.Json.Bson.BsonWriter bsonWriterObject = new Newtonsoft.Json.Bson.BsonWriter(objBsonMemoryStream);
        //    //6.
        //    jsonSerializer.Serialize(bsonWriterObject, studentObject);
        //    //text = queryString;
        //    byte[] requestByte = objBsonMemoryStream.ToArray();//= Encoding.Default.GetBytes(queryString);



        //    #region WebRequest

        //    //Connect to our Yugamiru Web Server
        //    //WebRequest webRequest = WebRequest.Create("http://gs-demo.jma.website/apioauthdata/index.php/home/activatekeydata");
        //    WebRequest webRequest = WebRequest.Create(StarterLicense.activateURL);
        //    webRequest.Method = "POST";
        //    webRequest.ContentType = "application/json";
        //    webRequest.ContentLength = requestByte.Length;
        //    Stream webDataStream = null;
        //    try
        //    {
        //        webDataStream = webRequest.GetRequestStream();
        //        webDataStream.Write(requestByte, 0, requestByte.Length);
        //    }
        //    catch (Exception ex)
        //    {
        //        //Added Sumit GSP-775 on 28-Aug-18 START
        //        WebComCation.FaultManager.LogIt(ex);
        //        //Added Sumit GSP-775 on 28-Aug-18 END
        //        using (var client = new WebClient())
        //        {
        //            try
        //            {
        //                using (client.OpenRead("http://clients3.google.com/generate_204"))
        //                {
        //                    //System.Windows.Forms.MessageBox.Show("You are not connected to internet");
        //                }
        //            }
        //            catch (Exception ex1)
        //            {
        //                // Edited By Suhana For GSP-450
        //                System.Windows.Forms.MessageBox.Show(Properties.Resources.PC_NOT_CONNECTED);
        //                //Added Sumit GSP-775 on 28-Aug-18 START
        //                WebComCation.FaultManager.LogIt(ex1);
        //                //Added Sumit GSP-775 on 28-Aug-18 END
        //                // Edited By Suhana For GSP-450
        //                // System.Windows.Forms.MessageBox.Show("You are not connected to internet");
        //                return LicenseStatus.Unknown;
        //            }
        //        }

        //    }
        //    string ed = webDataStream.ToString();

        //    // get the response from our stream

        //    WebResponse webResponse = webRequest.GetResponse();
        //    webDataStream = webResponse.GetResponseStream();

        //    // convert the result into a String
        //    StreamReader webResponseSReader = new StreamReader(webDataStream);
        //    String responseFromServer = webResponseSReader.ReadToEnd();
        //    #endregion

        //    #region License_Expired_Case
        //    if (responseFromServer.Contains("invalid") && responseFromServer.Contains("expired"))
        //    {
        //        int n1 = responseFromServer.IndexOf("Message");
        //        string p1 = responseFromServer.Substring(n1 + 10);
        //        p1 = p1.Replace("\0", "");
        //        p1 = p1.Replace(@"\u", "");
        //        p1 = p1.Replace(@"�", "");//                    
        //        p1 = p1.Replace(@":", "");
        //        p1 = p1.Substring(0, 112);
        //        // Edited By Suhana For GSP-450
        //        MessageBox.Show(Properties.Resources.ACTIVATION_KEY_EXPIRED);//+ p1);
        //                                                                     // Edited By Suhana For GSP-450
        //                                                                     // MessageBox.Show("Activation Key Expired");//+ p1);
        //                                                                     //textBox1.Text = "License Key Expired";//+ p1;
        //        return LicenseStatus.Expired;
        //        //Application.Exit();
        //    }
        //    #endregion


        //    #region Invalid_License
        //    else if (responseFromServer.Contains("invalid"))
        //    {
        //        // Edited By Suhana For GSP-450
        //        //  MessageBox.Show("Invalid Activation Key");
        //        MessageBox.Show(Properties.Resources.INVALID_LICENSE_KEY);
        //        // Edited By Suhana For GSP-450
        //        //Application.Exit();
        //        //textBox1.Text = "Invalid License Key";
        //        return LicenseStatus.Invalid;
        //    }
        //    #endregion


        //    #region Valid_License
        //    string sw = responseFromServer.Replace("?", "");
        //    sw = sw.Replace("\0", "");
        //    sw = sw.Replace(@"\u", "");
        //    sw = sw.Replace(@"�", "");//
        //                              //sw = sw.Replace(sw[7].ToString(), "");//
        //    sw = sw.Replace("\u0002", "-");
        //    sw = sw.Replace("\u0016", "");
        //    sw = sw.Replace("\u0006", "");
        //    sw = sw.Replace("\u0010", "");

        //    string CPK = sw;
        //    string strrm = sw;
        //    CPK = CPK.Replace("Status-ComputerKey ", "");
        //    CPK = CPK.Replace("-LicenseType-S-MessageLicense Key Activated-Keyvalid", "");
        //    #endregion
        //    #region Store_Computer_Key_Locally

        //    Keys.ReadKey();

        //    //QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();
        //    //Keys.ActivationKey = txtLicKey.Text.Trim();
        //    //Keys.ComputerKey = CPK;
        //    //Keys.StoreAKey();
        //    //Registry_Handler.SaveCurrentRunDateToReg();
        //    #endregion

        //    //=================

        //    return ret;
        //}
        #endregion



        /// <summary>
        /// Call this method after activation on before each launch
        /// </summary>
        /// <returns></returns>
        public static LicenseStatus VerifyPreActivated()
        {
            //Added by Sumit for GSP-821 on 24-Sep-18--------------START

            //Validate PC date time Online
            if (Utility.IsInternetConnected())
            {
                if (!DateTemperingHandler.IsPCDateValidNow())
                {
                    Process[] allp = Process.GetProcesses();
                    foreach (Process p in allp)
                    {
                        if (p.ProcessName.ToUpper().Contains("yugamiruPGB".ToUpper()))
                        {
                            p.CloseMainWindow();
                        }
                    }
                    //MessageBox.Show("Cannot continue application, PC date time is not correct");
                    foreach (Form frm in Application.OpenForms)
                    {

                        frm.Invoke((MethodInvoker)delegate
                        {
                            MessageBox.Show(frm, Properties.Resources.CANNOT_ACTIVATE_INCORRECT_PC_DATE, "gsport");
                        });
                        break;
                    }
                    Cleaner.ScrosheetCleaner(); // Sumit GSP-1299
                    Environment.Exit(0);
                }
                //else
                //{
                //    //Reset stored timestamp if net is connected and PC date is correct.
                //    //Added by Sumit GSP-821 on 25-Sep-2018-----START
                //    DateTemperingHandler.SavePresentTimeStamp();
                //    //Added by Sumit GSP-821 on 25-Sep-2018-----END

                //    #region commented
                //    //QlmLicenseLib.QlmLicense qlreset = new QlmLicenseLib.QlmLicense();

                //    //qlreset.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                //    //    , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
                //    //qlreset.PublicKey = ProductInfo.ProductEncryptionKey;
                //    //string x = "", y = "";
                //    //qlreset.ReadKeys(ref x, ref y);

                //    //qlreset.ProxyUser = DateTemperingHandler.GetTimeStampFromInternet();
                //    //qlreset.CultureName = qlreset.ProxyUser;
                //    //qlreset.DefaultWebServiceUrl = qlreset.ProxyUser;

                //    //QlmCustomerInfo sysdate = new QlmCustomerInfo();
                //    //sysdate.Address1 = "checkValueFix";

                //    //QlmLicense.QlmLogFile = "LogFileFix";

                //    //QlmLicenseLib.WebServiceCredentials wsvs = new WebServiceCredentials();
                //    //wsvs.Domain = "testfix";
                //    #endregion

                //}
            }
            else
            {
                if (!DateTemperingHandler.IsPCDateValidWithoutNet())
                {
                    Process[] allp = Process.GetProcesses();
                    foreach (Process p in allp)
                    {
                        if (p.ProcessName.ToUpper().Contains("yugamiruPGB".ToUpper()))
                        {
                            p.CloseMainWindow();
                        }
                    }
                    //MessageBox.Show("Cannot continue application, PC date time is not correct");
                    foreach (Form frm in Application.OpenForms)
                    {

                        frm.Invoke((MethodInvoker)delegate
                        {
                            MessageBox.Show(frm, Properties.Resources.CANNOT_ACTIVATE_INCORRECT_PC_DATE, "gsport");

                            //testing sumit
                            //System.Windows.Forms.MessageBox.Show("pcValue= " + pcValue + "(PCstamp = " + pcTimeStamp + ")" + Environment.NewLine + "SavedValue= " + SavedValue + "(SavedStamp = " + SavedTimeStamp + ")");
                            //testing sumit

                        });
                        //frm.Enabled = false;
                        //frm.Visible = false;
                        //frm.Invoke(new Action(() => { MessageBox.Show(frm, "Cannot continue application, PC date time is not correct", "gsport"); }));
                        break;
                    }
                    Cleaner.ScrosheetCleaner(); // Sumit GSP-1299
                    Environment.Exit(0);
                }
            }

            //Validate PC date offline           
            QlmLicenseLib.QlmLicense dtql = new QlmLicenseLib.QlmLicense();

            dtql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
            dtql.PublicKey = ProductInfo.ProductEncryptionKey;
            string a = "", b = "";
            dtql.ReadKeys(ref a, ref b);
            QlmCustomerInfo qlmCustomerInfo = new QlmCustomerInfo();
            qlmCustomerInfo.Address1 = "checkValueFix";
            QlmLicenseLib.WebServiceCredentials webService = new WebServiceCredentials();
            webService.Domain = "testfix";
            string readVallog = QlmLicense.QlmLogFile;

            string stp = dtql.DefaultWebServiceUrl;
            //if (dtql.ProxyUser != null && dtql.ProxyUser.Trim().Length > 0)
            {
                if (!DateTemperingHandler.IsPCDateValidNow())
                {
                    Process[] allp = Process.GetProcesses();
                    foreach (Process p in allp)
                    {
                        if (p.ProcessName.ToUpper().Contains("yugamiruPGB".ToUpper()))
                        {
                            p.CloseMainWindow();
                        }
                    }
                    //MessageBox.Show("Cannot continue application, PC date time is not correct");
                    foreach (Form frm in Application.OpenForms)
                    {
                        frm.Invoke((MethodInvoker)delegate
                        {
                            MessageBox.Show(frm, Properties.Resources.CANNOT_ACTIVATE_INCORRECT_PC_DATE, "gsport");
                        });
                        break;
                    }
                    Cleaner.ScrosheetCleaner(); // Sumit GSP-1299
                    Environment.Exit(0);
                }
            }

            //Added by Sumit for GSP-821 on 24-Sep-18--------------END


            int runcount = 0;
            ReCheck:
            runcount++;
            LicenseStatus st = LicenseStatus.Unknown;
            LicenseValidator.GetRenewedComputerKey("", "");
            //Fetch locally stored values first
            string LicenseKey = "";// Registry_Handler.GetActivationKeyFromReg();
            string ComputerKey = "";//Registry_Handler.GetComputerKeyFromReg();
            string computerID = keyRequest.GetComputerID();
            QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();
            //store keys first.
            string ak = LicenseKey; //Validation Key
            string ck = ComputerKey; // Computer Key
            //ql.DeleteKeys();

            ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
            ql.PublicKey = ProductInfo.ProductEncryptionKey;
            ql.StoreKeysOptions = QlmLicenseLib.EStoreKeysOptions.EStoreKeysPerMachine;




            //ql.StoreKeys(ak, ck);//uncommented Sumit GSP-775 on 03092018 1555

            //EndDate = Utility.DateTimeToString(ql.ExpiryDate);

            //DaysLeft = ql.DaysLeft;
            //========================================================================

            ql.ValidateLicense(LicenseKey);
            ql.ReadKeys(ref LicenseKey, ref ComputerKey);
            #region Added by Sumit on 10-Oct-18 GSP-858
            ActivationKey = LicenseKey;
            #endregion
            //Added by Sumit for GSP-804,GSP-805,GSP-806,GSP-789------------------START
            if (LicenseKey.Trim().Length != 0)
            {
                ql.ValidateLicenseEx(LicenseKey, keyRequest.GetComputerID());                            //========================================================================
                int DaysLeft = ql.DaysLeft; // testing sumit
                bool validity = ql.IsValid(); // testing sumit
                if (!ql.IsValid() && ql.DaysLeft <= 0)
                {
                    if (!Utility.IsInternetConnected())
                    {
                        System.Diagnostics.Process[] allp = System.Diagnostics.Process.GetProcesses();
                        foreach (System.Diagnostics.Process p in allp)
                        {
                            if (p.ProcessName.ToUpper().Contains("yugamiruPGB".ToUpper()))
                            {
                                p.CloseMainWindow();
                            }
                        }
                        //MessageBox.Show(Properties.Resources.CANNOT_LAUNCH_APP + Environment.NewLine + Properties.Resources.SOMETHING_WRONG_WITH_APP, "gsport");
                        //--Added By Rajnish for GSP-783--//
                        foreach (Form frm in Application.OpenForms)
                        {
                            frm.Invoke(new Action(() => { MessageBox.Show(frm, Properties.Resources.CANNOT_LAUNCH_APP + Environment.NewLine + Properties.Resources.SOMETHING_WRONG_WITH_APP, "gsport"); }));
                            break;
                        }
                        //--------------------------------//
                        Cleaner.ScrosheetCleaner(); // Sumit GSP-1299
                        Environment.Exit(0);
                    }
                }
            }
            //Added by Sumit for GSP-804,GSP-805,GSP-806,GSP-789------------------END

            //Added on 06-Sep-18 1244 by Sumit GSP-789------START
            ELicenseStatus statusOne = ql.GetStatus();
            //If system date tempered
            if (statusOne == ELicenseStatus.EKeyTampered)
            {
                //Close ProcessBar if running
                Process[] ps = Process.GetProcesses();
                foreach (Process kp in ps)
                {
                    if (kp.ProcessName.ToUpper().Contains("YUGAMIRUPGB"))
                    {
                        kp.CloseMainWindow();
                    }
                }
                //

                //--Commented by Rajnish For GSP-783--//
                //MessageBox.Show(Properties.Resources.CANNOT_LAUNCH_APP + Environment.NewLine + Properties.Resources.SOMETHING_WRONG_WITH_APP, "gsport");
                // MessageBox.Show("Cannot launch Application" + Environment.NewLine + "Something is wrong with this system. Most probably system date & time is not correct. ", "gsport");
                //--Added By Rajnish for GSP-783--//
                foreach (Form frm in Application.OpenForms)
                {
                    frm.Invoke(new Action(() => { MessageBox.Show(frm, Properties.Resources.CANNOT_LAUNCH_APP + Environment.NewLine + Properties.Resources.SOMETHING_WRONG_WITH_APP, "gsport"); }));
                    break;
                }
                //--------------------------------//
                Cleaner.ScrosheetCleaner(); // Sumit GSP-1299
                Environment.Exit(0);
            }
            //Added on 06-Sep-18 1244 by Sumit GSP-789------END

            #region GSP-788
            //Added by Sumit GSP-788 on 07-Sep-2018 1103--------START
            //if (LicenseKey.Trim() == "" || ComputerKey.Trim() != "")
            //{
            //    //redirect to License Activation Screen
            //    st = LicenseStatus.Unknown;


            //    ql.ReadKeys(ref LicenseKey, ref ComputerKey);
            //    if (LicenseKey.Trim().Length == 0 && ComputerKey.Trim().Length == 0)
            //    {

            //        Process[] allp = Process.GetProcesses();
            //        foreach (Process p in allp)
            //        {
            //            if (p.ProcessName.ToUpper().Contains("yugamiruPGB".ToUpper()))
            //            {
            //                p.CloseMainWindow();
            //            }
            //        }
            //        //Edited By Suhana for GSP-450
            //        MessageBox.Show(Properties.Resources.NO_LICENSE_FOUND, "gsport", MessageBoxButtons.OK);
            //        // MessageBox.Show("No License found on this PC", "gsport", MessageBoxButtons.OK);
            //        //Edited By Suhana for GSP-450
            //        st = LicenseStatus.Unknown;
            //        //remove the following after 1.0 testing
            //        //MessageBox.Show("AK={" + LicenseKey + Environment.NewLine + "},  CK={" + ComputerKey + Environment.NewLine + "},  status={" + st.ToString() + Environment.NewLine + "}, ComputerID=" + keyRequest.GetComputerID()+Environment.NewLine+ShowDebugDetails(), "Sumit Testing");
            //        return st;
            //    }
            //    else if (LicenseKey.Trim().Length > 0 && ComputerKey.Trim() == "")
            //    {
            //        //Edited By Suhana for GSP-450
            //        #region GSP-771
            //        //--If case Added by Rajnish For Background License check--//
            //        if (!IsCallFromBC && !IsFirstTimeCheck)//--!IsFirstTimeCheck Added by Rajnish To Handle GSP-771--//
            //        {
            //            // MessageBox.Show(Properties.Resources.ACTIVATION_KEY_EXPIRED, "gsport", MessageBoxButtons.OK);
            //            MessageBox.Show(Properties.Resources.EXISTING_LICENSE_EXPIRED, "gsport", MessageBoxButtons.OK);
            //        }
            //        #endregion                   


            //        //  MessageBox.Show("Application Activation Key has been expired.", "gsport", MessageBoxButtons.OK);
            //        //Edited By Suhana for GSP-450
            //    }
            //    else if (LicenseKey.Trim().Length > 0 && ComputerKey.Trim().Length > 0)
            //    {
            //        //MessageBox.Show("No License found on this PC", "gsport", MessageBoxButtons.OK);
            //        //Edited By Suhana for GSP-450
            //        MessageBox.Show(Properties.Resources.ACTIVATION_KEY_UPDATED, "gsport", MessageBoxButtons.OK);
            //        // MessageBox.Show("Activation key has been updated ", "gsport", MessageBoxButtons.OK);
            //        //Edited By Suhana for GSP-450
            //    }
            //    else if (LicenseKey.Trim() == "" && ComputerKey.ToUpper().Contains("COMPUTERKEY"))
            //    {

            //        //Added By Sumit for Load Progress Bar-----START

            //        //process.StartInfo.FileName = Application.StartupPath + "\\yugamiruPGB.exe";

            //        Process[] allp = Process.GetProcesses();
            //        foreach (Process p in allp)
            //        {
            //            if (p.ProcessName.ToUpper().Contains("yugamiruPGB".ToUpper()))
            //            {
            //                p.CloseMainWindow();
            //            }
            //        }



            //        //Added by sumit load progress bar---------END
            //        //Edited By Suhana for GSP-450
            //        // MessageBox.Show("No License found on this PC", "gsport", MessageBoxButtons.OK);
            //        MessageBox.Show(Properties.Resources.NO_LICENSE_FOUND, "gsport", MessageBoxButtons.OK);
            //        //Edited By Suhana for GSP-450
            //        //Process myp = new Process();
            //        //myp.StartInfo.FileName = Application.StartupPath + "\\yugamiruPGB.exe";
            //        //myp.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
            //        //myp.Start();
            //    }
            //    //WebComCation.KeyValidator kv = new KeyValidator();
            //    //kv.ShowDialog();
            //    //goto ReCheck;
            //    //remove the following after 1.0 testing
            //    //MessageBox.Show("AK={" + LicenseKey + Environment.NewLine + "},  CK={" + ComputerKey + Environment.NewLine + "},  status={" + st.ToString() + Environment.NewLine + "}, ComputerID=" + keyRequest.GetComputerID()+Environment.NewLine+ShowDebugDetails(), "Sumit Testing");
            //    return st;
            //}
            //Added by Sumit GSP-788 on 07-Sep-2018 1103--------END
            #endregion //only comments
            else if (LicenseKey.Trim() == "" || ComputerKey.Trim() == "")
            {
                //redirect to License Activation Screen
                st = LicenseStatus.Unknown;


                ql.ReadKeys(ref LicenseKey, ref ComputerKey);
                if (LicenseKey.Trim().Length == 0 && ComputerKey.Trim().Length == 0)
                {

                    Process[] allp = Process.GetProcesses();
                    foreach (Process p in allp)
                    {
                        if (p.ProcessName.ToUpper().Contains("yugamiruPGB".ToUpper()))
                        {
                            p.CloseMainWindow();
                        }
                    }
                    //Edited By Suhana for GSP-450
                    //MessageBox.Show(Properties.Resources.NO_LICENSE_FOUND, "gsport", MessageBoxButtons.OK);//--commented by Rajnish For GSP-783--//
                    // MessageBox.Show("No License found on this PC", "gsport", MessageBoxButtons.OK);
                    //Edited By Suhana for GSP-450
                    //--Added By Rajnish for GSP-783--//
                    foreach (Form frm in Application.OpenForms)
                    {
                        //Added by Sumit GSP-1335   --------START
                        //frm.Refresh();
                        //Added by Sumit GSP-1335   --------END
                        frm.Invoke(new Action(() => { MessageBox.Show(frm, Properties.Resources.NO_LICENSE_FOUND, "gsport", MessageBoxButtons.OK); }));
                        //System.Threading.Thread.CurrentThread.IsBackground = true;
                        break;
                    }
                    //--------------------------------//
                    st = LicenseStatus.Unknown;
                    //remove the following after 1.0 testing
                    //MessageBox.Show("AK={" + LicenseKey + Environment.NewLine + "},  CK={" + ComputerKey + Environment.NewLine + "},  status={" + st.ToString() + Environment.NewLine + "}, ComputerID=" + keyRequest.GetComputerID()+Environment.NewLine+ShowDebugDetails(), "Sumit Testing");
                    return st;
                }
                else if (LicenseKey.Trim().Length > 0 && ComputerKey.Trim() == "")
                {
                    //--Added by Rajnish For GSP-768//
                    sActivationKey = LicenseKey;
                    sExpiryDate = ql.ExpiryDate.ToString("dd-MMM-yyyy");
                    sActivationDate = ql.InstallationDate.ToString("dd-MMM-yyyy");
                    //------------------------------//

                    //Edited By Suhana for GSP-450
                    #region GSP-771
                    //--If case Added by Rajnish For Background License check--//
                    if (!IsCallFromBC && !IsFirstTimeCheck)//--!IsFirstTimeCheck Added by Rajnish To Handle GSP-771--//
                    {
                        // MessageBox.Show(Properties.Resources.ACTIVATION_KEY_EXPIRED, "gsport", MessageBoxButtons.OK);
                        //Added by Sumit GSP-788 on 07-Sep-2018 1213-----------START

                        Process[] allp = Process.GetProcesses();
                        foreach (Process p in allp)
                        {
                            if (p.ProcessName.ToUpper().Contains("yugamiruPGB".ToUpper()))
                            {
                                p.CloseMainWindow();
                            }
                        }
                        //Added by Sumit GSP-788 on 07-Sep-2018 1213-------END

                        //--Added by Rajnish For GSP-768--//
                        objfrmCustomMessageBox.lblMessage.Text = Properties.Resources.EXISTING_LICENSE_EXPIRED;
                        objfrmCustomMessageBox.tbDetails.Text = "Activation key : " + sActivationKey + Environment.NewLine + "Activation Date : " + sActivationDate + Environment.NewLine + "Expiry Date : " + sExpiryDate;
                        objfrmCustomMessageBox.btnOK.Focus();
                        objfrmCustomMessageBox.TopMost = true;
                        //objfrmCustomMessageBox.ShowDialog();
                        //--------------------------------//

                        //--Commented By Rajnish for GSP-783--//
                        //MessageBox.Show(Properties.Resources.EXISTING_LICENSE_EXPIRED, "gsport", MessageBoxButtons.OK); 
                        //------------------------------------//

                        //--Added By Rajnish for GSP-783--//
                        foreach (Form frm in Application.OpenForms)
                        {
                            //frm.Invoke(new Action(() => { MessageBox.Show(frm, Properties.Resources.EXISTING_LICENSE_EXPIRED, "gsport", MessageBoxButtons.OK); }));
                            frm.Invoke(new Action(() => { objfrmCustomMessageBox.ShowDialog(); }));

                            //Sumit GSP-806 on 18-Sep-2018-----------START
                            if (!Utility.IsInternetConnected())
                            {
                                //No internet so that there is no possibility of activation so that we can close the application.
                                Cleaner.ScrosheetCleaner(); // Sumit GSP-1299
                                Environment.Exit(0);
                                //return LicenseStatus.Expired;
                            }
                            //Sumit GSP-806 on 18-Sep-2018-----------END


                            break;
                        }
                        //--------------------------------//
                    }
                    #endregion                   


                    //  MessageBox.Show("Application Activation Key has been expired.", "gsport", MessageBoxButtons.OK);
                    //Edited By Suhana for GSP-450
                }
                else if (LicenseKey.Trim().Length > 0 && ComputerKey.Trim().Length > 0)
                {
                    //MessageBox.Show("No License found on this PC", "gsport", MessageBoxButtons.OK);
                    //Edited By Suhana for GSP-450
                    //MessageBox.Show(Properties.Resources.ACTIVATION_KEY_UPDATED, "gsport", MessageBoxButtons.OK);//--commented By Rajnish for GSP-783--//
                    //--Added By Rajnish for GSP-783--//
                    foreach (Form frm in Application.OpenForms)
                    {
                        frm.Invoke(new Action(() => { MessageBox.Show(frm, Properties.Resources.ACTIVATION_KEY_UPDATED, "gsport", MessageBoxButtons.OK); }));
                        break;
                    }
                    //--------------------------------//
                    // MessageBox.Show("Activation key has been updated ", "gsport", MessageBoxButtons.OK);
                    //Edited By Suhana for GSP-450
                }
                else if (LicenseKey.Trim() == "" && ComputerKey.ToUpper().Contains("COMPUTERKEY"))
                {

                    //Added By Sumit for Load Progress Bar-----START

                    //process.StartInfo.FileName = Application.StartupPath + "\\yugamiruPGB.exe";

                    Process[] allp = Process.GetProcesses();
                    foreach (Process p in allp)
                    {
                        if (p.ProcessName.ToUpper().Contains("yugamiruPGB".ToUpper()))
                        {
                            p.CloseMainWindow();
                        }
                    }



                    //Added by sumit load progress bar---------END
                    //Edited By Suhana for GSP-450
                    // MessageBox.Show("No License found on this PC", "gsport", MessageBoxButtons.OK);
                    //MessageBox.Show(Properties.Resources.NO_LICENSE_FOUND, "gsport", MessageBoxButtons.OK);//--commented By Rajnish for GSP-783--//

                    //--Added By Rajnish for GSP-783--//
                    foreach (Form frm in Application.OpenForms)
                    {
                        //Added by Sumit GSP-1335   --------START
                        //frm.Refresh();
                        //Application.VisualStyleState = System.Windows.Forms.VisualStyles.VisualStyleState.ClientAndNonClientAreasEnabled;
                        //System.Threading.Thread.CurrentThread.IsBackground = false;
                        //SendKeys.
                        //Added by Sumit GSP-1335   --------END
                        
                        frm.Invoke(new Action(() => { MessageBox.Show(frm, Properties.Resources.NO_LICENSE_FOUND, "gsport", MessageBoxButtons.OK); }));
                        //System.Threading.Thread.CurrentThread.IsBackground = true;
                        break;
                    }
                    //--------------------------------//

                    //Edited By Suhana for GSP-450
                    //Process myp = new Process();
                    //myp.StartInfo.FileName = Application.StartupPath + "\\yugamiruPGB.exe";
                    //myp.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
                    //myp.Start();
                }
                //WebComCation.KeyValidator kv = new KeyValidator();
                //kv.ShowDialog();
                //goto ReCheck;
                //remove the following after 1.0 testing
                //MessageBox.Show("AK={" + LicenseKey + Environment.NewLine + "},  CK={" + ComputerKey + Environment.NewLine + "},  status={" + st.ToString() + Environment.NewLine + "}, ComputerID=" + keyRequest.GetComputerID()+Environment.NewLine+ShowDebugDetails(), "Sumit Testing");
                return st;
            }
            else if (ComputerKey.Contains("#Status-ComputerKey\u0001"))
            {
                Process[] allp = Process.GetProcesses();
                foreach (Process p in allp)
                {
                    if (p.ProcessName.ToUpper().Contains("yugamiruPGB".ToUpper()))
                    {
                        p.CloseMainWindow();
                    }
                }
                //Edited By Suhana for GSP-450
                //MessageBox.Show(Properties.Resources.NO_LICENSE_FOUND, "gsport", MessageBoxButtons.OK); //--commented by Rajnish For GSP-783--//
                // MessageBox.Show("No License found on this PC", "gsport", MessageBoxButtons.OK);

                //--Added By Rajnish for GSP-783--//
                foreach (Form frm in Application.OpenForms)
                {
                    //Added by Sumit GSP-1335   --------START
                    //frm.Refresh();
                    
                    //Added by Sumit GSP-1335   --------END

                    frm.Invoke(new Action(() => { MessageBox.Show(frm, Properties.Resources.NO_LICENSE_FOUND, "gsport", MessageBoxButtons.OK); }));
                    
                    break;
                }
                //--------------------------------//
                //Edited By Suhana for GSP-450
                //remove the following after 1.0 testing
                //MessageBox.Show("AK={" + LicenseKey + Environment.NewLine + "},  CK={" + ComputerKey + Environment.NewLine + "},  status={" + st.ToString() + Environment.NewLine + "}, ComputerID=" + keyRequest.GetComputerID()+Environment.NewLine+ShowDebugDetails(), "Sumit Testing");
                return st;
            }
            else if (LicenseKey.Trim() != "" && ComputerKey.Trim() != "")
            {
                //Check Offline
                bool licensevalid = false;
                ql.ValidateLicenseEx(ComputerKey, keyRequest.GetComputerID());

                ELicenseStatus status = ql.GetStatus();
                //If system date tempered                
                if (status == ELicenseStatus.EKeyTampered)
                {
                    //Close ProcessBar if running
                    Process[] ps = Process.GetProcesses();
                    foreach (Process kp in ps)
                    {
                        if (kp.ProcessName.ToUpper().Contains("YUGAMIRUPGB"))
                        {
                            kp.CloseMainWindow();
                        }
                    }
                    //
                    //Edited By Suhana for GSP-450
                    //MessageBox.Show(Properties.Resources.CANNOT_LAUNCH_APP + Environment.NewLine + Properties.Resources.SOMETHING_WRONG_WITH_APP, "gsport");//--Commented by Rajnish For GSP-783--//

                    //--Added By Rajnish for GSP-783--//
                    foreach (Form frm in Application.OpenForms)
                    {
                        frm.Invoke(new Action(() => { MessageBox.Show(frm, Properties.Resources.CANNOT_LAUNCH_APP + Environment.NewLine + Properties.Resources.SOMETHING_WRONG_WITH_APP, "gsport"); }));
                        break;
                    }
                    //--------------------------------//

                    // MessageBox.Show("Cannot launch Application" + Environment.NewLine + "Something is wrong with this system. Most probably system date & time is not correct. ", "gsport");
                    //Edited By Suhana for GSP-450
                    Cleaner.ScrosheetCleaner(); // Sumit GSP-1299
                    Environment.Exit(0);
                }


                //if(status==ELicenseStatus.EKeyDemo || ELicenseStatus.EKeyPermanent || ELicenseStatus.)  
                string EndDate = Utility.DateTimeToString(ql.ExpiryDate);

                int DaysLeft1 = ql.DaysLeft;
                if (DaysLeft1 > 0)
                {
                    st = LicenseStatus.Valid;
                }
                // else
                {
                    //Check Online If License renewed.

                    string rCK = GetRenewedComputerKey(LicenseKey, ComputerKey);
                    if (rCK.Trim().ToUpper().Contains("LANGUAGE INVALID"))//Added by Rajnish for GSP - 880 and GSP - 881
                    {
                        //Close ProcessBar if running
                        Process[] ps = Process.GetProcesses();
                        foreach (Process kp in ps)
                        {
                            if (kp.ProcessName.ToUpper().Contains("YUGAMIRUPGB"))
                            {
                                kp.CloseMainWindow();
                            }
                        }
                        //                        
                        //Edited by sumit GSP-1059----START                        
                        //Environment.Exit(0);
                        return LicenseStatus.Invalid;
                        //Edited by sumit GSP-1059----END

                    }
                    if ((rCK.Trim().ToUpper().Contains("EXPIRED") || DaysLeft1 <= 0)&& (!rCK.Trim().ToUpper().Contains("ReleasedFromServer".ToUpper())))//ReleasedFromServer
                    {
                        st = LicenseStatus.Expired;
                        //Edited By Suhana for GSP-450

                        //Close ProcessBar if running
                        Process[] ps = Process.GetProcesses();
                        foreach (Process kp in ps)
                        {
                            if (kp.ProcessName.ToUpper().Contains("YUGAMIRUPGB"))
                            {
                                kp.CloseMainWindow();
                            }
                        }
                        //

                        // MessageBox.Show("Existing license has been expired.", "gsport");

                        //Testing Sumit 31_Aug_18 START
                        //MessageBox.Show("LineNo: 570 veryfyPreActivated()"+Environment.NewLine+"rCK= "+rCK+Environment.NewLine +"LicenseKey= "+ LicenseKey+Environment.NewLine+ "ComputerKey= "+ ComputerKey, "Testing");
                        //Testing Sumit 31_Aug_18 END

                        //--commented By Rajnish for GSP-783--//
                        //MessageBox.Show(Properties.Resources.EXISTING_LICENSE_EXPIRED, "gsport");
                        //Edited By Suhana for GSP-450

                        //--Added by Rajnish For GSP-768//
                        sActivationKey = LicenseKey;
                        sExpiryDate = ql.ExpiryDate.ToString("dd-MMM-yyyy");
                        sActivationDate = ql.InstallationDate.ToString("dd-MMM-yyyy");
                        //------------------------------//
                        //--Added By Rajnish For GSP-768--//
                        objfrmCustomMessageBox.lblMessage.Text = Properties.Resources.EXISTING_LICENSE_EXPIRED;
                        objfrmCustomMessageBox.tbDetails.Text = "Activation key : " + sActivationKey + Environment.NewLine + "Activation Date : " + sActivationDate + Environment.NewLine + "Expiry Date : " + sExpiryDate;
                        objfrmCustomMessageBox.btnOK.Focus();
                        objfrmCustomMessageBox.TopMost = true;
                        //objfrmCustomMessageBox.ShowDialog();
                        //-------------------------------//

                        //--Added By Rajnish for GSP-783--//
                        foreach (Form frm in Application.OpenForms)
                        {
                            //frm.Invoke(new Action(() => { MessageBox.Show(frm, Properties.Resources.EXISTING_LICENSE_EXPIRED, "gsport"); }));
                            frm.Invoke(new Action(() => { objfrmCustomMessageBox.ShowDialog(); }));
                            //Sumit GSP-806 on 18-Sep-2018-----------START
                            if (!Utility.IsInternetConnected())
                            {
                                //No internet so that there is no possibility of activation so that we can close the application.
                                Cleaner.ScrosheetCleaner(); // Sumit GSP-1299
                                Environment.Exit(0);                                
                            }                            
                            break;
                        }
                        return st;
                        //--------------------------------//
                    }
                    else if (rCK.Trim().ToUpper().Contains("ReleasedFromServer".ToUpper()))
                    {
                        st = LicenseStatus.ReleasedFromServer;                       
                        Process[] ps = Process.GetProcesses();
                        foreach (Process kp in ps)
                        {
                            if (kp.ProcessName.ToUpper().Contains("YUGAMIRUPGB"))
                            {
                                kp.CloseMainWindow();
                            }
                        }
                        //Edited By Sumit GSP-1082-----START
                        #region old code
                        //objfrmCustomMessageBox.lblMessage.Text = "Existing license is not valid" + Environment.NewLine + "in current context";
                        //objfrmCustomMessageBox.tbDetails.Text = "Activation key : " + LicenseKey;
                        //objfrmCustomMessageBox.btnOK.Focus();                        
                        //foreach (Form frm in Application.OpenForms)
                        //{                           
                        //    frm.Invoke(new Action(() => { objfrmCustomMessageBox.ShowDialog(); }));                           
                        //    if (!Utility.IsInternetConnected() || IsCallFromBC)
                        //    {
                        //        //No internet so that there is no possibility of activation so that we can close the application.

                        //        Environment.Exit(0);                           
                        //    }                        
                        //    else
                        //    {

                        //    }
                        //    break;
                        //}
                        #endregion
                        objfrmCustomMessageBox.lblMessage.Text = Properties.Resources.INVALID_LICENSE_KEY;
                        objfrmCustomMessageBox.tbDetails.Text = LicenseKey;
                        objfrmCustomMessageBox.btnOK.Focus();
                        frmReleaseLicense.DeleteKey();
                        foreach (Form frm in Application.OpenForms)
                        {
                            frm.Invoke(new Action(() => { objfrmCustomMessageBox.ShowDialog(); }));
                            if (!Utility.IsInternetConnected() || IsCallFromBC)
                            {
                                //No internet so that there is no possibility of activation so that we can close the application.
                                Cleaner.ScrosheetCleaner(); // Sumit GSP-1299
                                Environment.Exit(0);
                            }
                            else
                            {

                            }
                            break;
                        }
                        //Edited By Sumit GSP-1082-----END
                        return st;                        
                    }
                    else
                    {
                        if (runcount < 2)
                            goto ReCheck;
                    }
                }

            }
            //remove the following after 1.0 testing
            //MessageBox.Show("AK={"+LicenseKey+Environment.NewLine+"},  CK={" + ComputerKey + Environment.NewLine + "},  status={" +st.ToString()+ Environment.NewLine + "}, ComputerID=" + keyRequest.GetComputerID()+Environment.NewLine+ShowDebugDetails(), "Sumit Testing");

            //Added by Sumit GSP-789 on 11-Sep-18------------START
            QlmLicenseLib.QlmLicense qlex = new QlmLicenseLib.QlmLicense();
            //qlex.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
            //    , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
            //qlex.PublicKey = ProductInfo.ProductEncryptionKey;
            //qlex.ReadKeys(ref LicenseKey, ref ComputerKey);
            ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
            ql.PublicKey = ProductInfo.ProductEncryptionKey;
            ql.ReadKeys(ref LicenseKey, ref ComputerKey);
            int DaysLeftToExp = ql.DaysLeft;
            DaysLeftToExp = ql.DaysLeft;
            if (DaysLeftToExp <= 0)
            {
                //--Commented By Rajnish for GSP-783--//
                //MessageBox.Show(Properties.Resources.EXISTING_LICENSE_EXPIRED, "gsport");

                //--Added by Rajnish For GSP-768//
                sActivationKey = LicenseKey;
                sExpiryDate = ql.ExpiryDate.ToString("dd-MMM-yyyy");
                sActivationDate = ql.InstallationDate.ToString("dd-MMM-yyyy");
                //------------------------------//
                //--Added By Rajnish For GSP-768--//
                objfrmCustomMessageBox.lblMessage.Text = Properties.Resources.EXISTING_LICENSE_EXPIRED;
                objfrmCustomMessageBox.tbDetails.Text = "Activation key : " + sActivationKey + Environment.NewLine + "Activation Date : " + sActivationDate + Environment.NewLine + "Expiry Date : " + sExpiryDate;
                objfrmCustomMessageBox.btnOK.Focus();
                objfrmCustomMessageBox.TopMost = true;
                //objfrmCustomMessageBox.ShowDialog();

                //foreach (Form frm in Application.OpenForms)
                //{
                //    frm.Invoke(new Action(() => { objfrmCustomMessageBox.ShowDialog(); }));
                //}
                //-------------------------------//



                //--Added By Rajnish for GSP-783--//
                foreach (Form frm in Application.OpenForms)
                {
                    //frm.Invoke(new Action(() => { MessageBox.Show(frm, Properties.Resources.EXISTING_LICENSE_EXPIRED, "gsport"); }));
                    frm.Invoke(new Action(() => { objfrmCustomMessageBox.ShowDialog(); }));
                    //Sumit GSP-806 on 18-Sep-2018-----------START
                    if (!Utility.IsInternetConnected())
                    {
                        //No internet so that there is no possibility of activation so that we can close the application.
                        Cleaner.ScrosheetCleaner(); // Sumit GSP-1299
                        Environment.Exit(0);

                        //return LicenseStatus.Expired;
                    }
                    //Sumit GSP-806 on 18-Sep-2018-----------END
                    break;
                }
                //--------------------------------//

                st = LicenseStatus.Expired;
                return st;
            }
            //Added by Sumit GSP-789 on 11-Sep-18------------END



            ql.ValidateLicense(LicenseKey);                            //========================================================================
            //DaysLeft = ql.DaysLeft; // testing sumit
            //validity = ql.IsValid(); // testing sumit
            if (!ql.IsValid() && ql.DaysLeft <= 0)
            {
                if (!Utility.IsInternetConnected())
                {
                    System.Diagnostics.Process[] allp = System.Diagnostics.Process.GetProcesses();
                    foreach (System.Diagnostics.Process p in allp)
                    {
                        if (p.ProcessName.ToUpper().Contains("yugamiruPGB".ToUpper()))
                        {
                            p.CloseMainWindow();
                        }
                    }
                    //MessageBox.Show(Properties.Resources.CANNOT_LAUNCH_APP + Environment.NewLine + Properties.Resources.SOMETHING_WRONG_WITH_APP, "gsport");
                    //--Added By Rajnish for GSP-783--//
                    foreach (Form frm in Application.OpenForms)
                    {
                        frm.Invoke(new Action(() => { MessageBox.Show(frm, Properties.Resources.CANNOT_LAUNCH_APP + Environment.NewLine + Properties.Resources.SOMETHING_WRONG_WITH_APP, "gsport"); }));
                        break;
                    }
                    //--------------------------------//
                    Cleaner.ScrosheetCleaner(); // Sumit GSP-1299
                    Environment.Exit(0);
                }
            }
            //Added by Sumit for GSP-804,GSP-805,GSP-806,GSP-789------------------END



            return st;
        }
        /// <summary>
        /// Call this method if existing license has been expired. It will check from server if same license
        /// has been renewed then it will update local system with new ComputerKey
        /// </summary>
        /// <returns></returns>
        public static string GetRenewedComputerKey(string ActivationKey, string ComputerKey, string callingFrom = "")

        {
            bool bRenewedMsgOnce = true;
            string renewedCompKey = string.Empty;
            //=============GSP-304 start==============
            if (ActivationKey == null || ActivationKey.Trim().Length == 0)
            {
                //Read Activation key using soraco APIs
                string LicenseKey = "";// Registry_Handler.GetActivationKeyFromReg();
                //string ComputerKey = "";//Registry_Handler.GetComputerKeyFromReg();
                //string computerID = keyRequest.GetComputerID();
                QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();
                //store keys first.
                //string ak = ""; //Validation Key
                //string ck = ""; // Computer Key
                ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                    , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
                ql.PublicKey = ProductInfo.ProductEncryptionKey;
                ql.StoreKeysOptions = QlmLicenseLib.EStoreKeysOptions.EStoreKeysPerMachine;
                ql.ReadKeys(ref LicenseKey, ref ComputerKey);
                //ql.StoreKeys(ak, ck);
                ELicenseStatus statusOne = ql.GetStatus();
                //EndDate = Utility.DateTimeToString(ql.ExpiryDate);


                //Added by Sumit for GSP-804,GSP-805,GSP-806,GSP-789------------------START
                if (LicenseKey.Trim().Length != 0)
                {
                    ql.ValidateLicense(LicenseKey);                            //========================================================================
                    ql.ValidateLicenseEx(LicenseKey, keyRequest.GetComputerID());
                    statusOne = ql.GetStatus();
                    int DaysLeft = ql.DaysLeft; // testing sumit
                    bool validity = ql.IsValid(); // testing sumit
                    ELicenseStatus es = ql.GetStatus();
                    string xin = "";
                    ql.ValidateIntegrity(ref xin);
                    //ql.VerifyTrust

                    if (!ql.IsValid() && ql.DaysLeft <= 0)
                    {
                        if (!Utility.IsInternetConnected())
                        {
                            System.Diagnostics.Process[] allp = System.Diagnostics.Process.GetProcesses();
                            foreach (System.Diagnostics.Process p in allp)
                            {
                                if (p.ProcessName.ToUpper().Contains("yugamiruPGB".ToUpper()))
                                {
                                    p.CloseMainWindow();
                                }
                            }
                            //MessageBox.Show(Properties.Resources.CANNOT_LAUNCH_APP + Environment.NewLine + Properties.Resources.SOMETHING_WRONG_WITH_APP, "gsport");
                            //--Added By Rajnish for GSP-783--//
                            foreach (Form frm in Application.OpenForms)
                            {
                                frm.Invoke(new Action(() => { MessageBox.Show(frm, Properties.Resources.CANNOT_LAUNCH_APP + Environment.NewLine + Properties.Resources.SOMETHING_WRONG_WITH_APP, "gsport"); }));
                                break;
                            }
                            //--------------------------------//
                            Cleaner.ScrosheetCleaner(); // Sumit GSP-1299
                            Environment.Exit(0);
                        }
                    }
                }
                //Added by Sumit for GSP-804,GSP-805,GSP-806,GSP-789------------------END

                //string a = string.Empty;
                //string b = string.Empty;
                //ql.ReadKeysV3(ref a, ref b);
                //QlmLicense qm = new QlmLicense();
                //qm.StoreKeysOptions = EStoreKeysOptions.EStoreKeysPerMachine;
                //ql.DefaultWebServiceUrl = Environment.UserName;
                //string chkRelease = ql.ProxyUser;

                ActivationKey = LicenseKey;
            }


            //================GSP-304 end=======


            //===================
            //LicenseStatus st = LicenseStatus.Unknown;
            string APIUrl = string.Empty;
            string ComputerName = keyRequest.GetComputerName();
            string computerID = keyRequest.GetComputerID();
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string jsonQuery = "[{\"Computer_id\":\"" + keyRequest.GetComputerID() +
                                    "\",\"Computer_name\":\"" + keyRequest.GetComputerName() +
                                    "\",\"Activation_key\":\"" + ActivationKey + "\",\"Language\":\"" + sCurrent_Language.ToLower().Replace("japanese", "japan") + "\"}]";
                String queryString = jsonQuery;//jsonRawTemplate;//jsonQuery;
                var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(queryString);
                //3.
                JsonSerializer jsonSerializer = new JsonSerializer();
                //4.
                MemoryStream objBsonMemoryStream = new MemoryStream();
                //5.
                Newtonsoft.Json.Bson.BsonWriter bsonWriterObject = new Newtonsoft.Json.Bson.BsonWriter(objBsonMemoryStream);
                //6.
                jsonSerializer.Serialize(bsonWriterObject, studentObject);
                //text = queryString;
                byte[] requestByte = objBsonMemoryStream.ToArray();//= Encoding.Default.GetBytes(queryString);



                #region WebRequest

                //Connect to our Yugamiru Web Server
                //WebRequest webRequest = WebRequest.Create("http://gs-demo.jma.website/apioauthdata/index.php/home/validatelicense");
                WebRequest webRequest = WebRequest.Create(StarterLicense.validateURL);
                webRequest.Method = "POST";
                webRequest.ContentType = "application/json";
                webRequest.ContentLength = requestByte.Length;
                Stream webDataStream = null;
                try
                {
                    webDataStream = webRequest.GetRequestStream();
                    webDataStream.Write(requestByte, 0, requestByte.Length);
                }
                catch (Exception ex)
                {
                    //Added Sumit GSP-775 on 28-Aug-18 START
                    WebComCation.FaultManager.LogIt(ex);
                    //Added Sumit GSP-775 on 28-Aug-18 END
                    using (var client = new WebClient())
                    {
                        try
                        {
                            using (client.OpenRead("http://clients3.google.com/generate_204"))//for checking internet connectivity.
                            {
                                //System.Windows.Forms.MessageBox.Show("You are not connected to internet");
                            }
                        }
                        catch (Exception ex1)
                        {
                            // Commented by sumit GSP-356-----------START

                            //Sumit GSP-804,805,806,789---------START

                            //if (!ql.IsValid() && ql.DaysLeft <= 0)
                            //{
                            //}

                            //Sumit GSP-804,805,806,789------------END




                            if (frmViewLicense.ViewLicenseActive)
                                //System.Windows.Forms.MessageBox.Show("PC is not connected to internet");
                                //Edited By Suhana For GSP-450
                                // System.Windows.Forms.MessageBox.Show("PC is not connected to internet");
                                System.Windows.Forms.MessageBox.Show(Properties.Resources.PC_NOT_CONNECTED);
                            //Edited By Suhana For GSP-450 
                            //by sumit GSP-356-----------END

                            //Added Sumit GSP-775 on 28-Aug-18 START
                            WebComCation.FaultManager.LogIt(ex);
                            //Added Sumit GSP-775 on 28-Aug-18 END

                            return "Connection Error";
                        }
                    }
                }
                string ed = webDataStream.ToString();

                // get the response from our stream

                WebResponse webResponse = webRequest.GetResponse();
                webDataStream = webResponse.GetResponseStream();

                // convert the result into a String
                StreamReader webResponseSReader = new StreamReader(webDataStream);
                //Try Catch added by sumit for GSP-1078-----------START
                //String responseFromServer = webResponseSReader.ReadToEnd();
                String responseFromServer = "";

                try
                {
                    responseFromServer = webResponseSReader.ReadToEnd();
                    //Sumit Added for GSP-1291-----START
                    string resChkQLMDown = responseFromServer.ToUpper();
                    if (resChkQLMDown.Contains("QLMSTATUS"))
                    {
                        //Means QLM is down
                        //Check if valid on server
                        if (resChkQLMDown.Contains("200") && !resChkQLMDown.Contains("INVALID"))
                        {
                            //Valid on Server
                            return ComputerKey;
                        }
                        if (resChkQLMDown.Contains("401") && resChkQLMDown.Contains("INVALID"))
                        {
                            //InValid on Server
                            return "invalid";
                        }
                    }
                    //Sumit Added for GSP-1291-----END
                }
                catch
                {
                    if (!Utility.IsInternetConnected())
                        System.Windows.Forms.MessageBox.Show(Properties.Resources.PC_NOT_CONNECTED);

                    return "Connection Error";
                }
                //Try Catch added by sumit for GSP-1078-----------END

                #region GSP-835 by Sumit
                string dirtyValue = responseFromServer; 
                if(!dirtyValue.ToUpper().Contains("ENGLISH") && !dirtyValue.ToUpper().Contains("JAPAN"))
                {                    
                    return "ReleasedFromServer";
                }
                foreach (System.Data.DataRow dr in Utility.GetRemoverStringsTable().Rows)
                {
                    dirtyValue = dirtyValue.Replace(dr[0].ToString(), "");
                }
                if(dirtyValue.ToUpper().Contains("ComputerKeyLanguage".ToUpper()))
                {
                    return "ReleasedFromServer";
                }
                #endregion 

                #region License_Expired_Case
                if (responseFromServer.Contains("invalid") && responseFromServer.Contains("expired"))
                {
                    int n1 = responseFromServer.IndexOf("Message");
                    string p1 = responseFromServer.Substring(n1 + 10);
                    p1 = p1.Replace("\0", "");
                    p1 = p1.Replace(@"\u", "");
                    p1 = p1.Replace(@"�", "");//                    
                    p1 = p1.Replace(@":", "");
                    p1 = p1.Substring(0, 112);
                    // MessageBox.Show("Activation Key Expired");//+ p1);
                    //Edited By Suhana For GSP-450
                    //MessageBox.Show(Properties.Resources.ACTIVATION_KEY_EXPIRED);//--Commented By Rajnish for GSP-783--//
                    //Edited By Suhana For GSP-450

                    //--Added By Rajnish For GSP-768--//
                    objfrmCustomMessageBox.lblMessage.Text = Properties.Resources.ACTIVATION_KEY_EXPIRED;
                    objfrmCustomMessageBox.tbDetails.Text = "Activation key : " + sActivationKey + Environment.NewLine + "Activation Date : " + sActivationDate + Environment.NewLine + "Expiry Date : " + sExpiryDate;
                    objfrmCustomMessageBox.btnOK.Focus();
                    objfrmCustomMessageBox.TopMost = true;
                    //objfrmCustomMessageBox.ShowDialog();
                    //----------------------//

                    //--Added By Rajnish for GSP-783--//
                    foreach (Form frm in Application.OpenForms)
                    {
                        frm.Invoke(new Action(() => { objfrmCustomMessageBox.ShowDialog(); }));
                        break;
                    }
                    //--------------------------------//

                    //textBox1.Text = "License Key Expired";//+ p1;
                    return "expired";
                    //Application.Exit();
                }
                #endregion


                #region Invalid_License
                else if (responseFromServer.Contains("invalid"))
                {
                    //Edited By Suhana For GSP-450
                    //MessageBox.Show(Properties.Resources.INVALID_LICENSE_KEY);//--commented by Rajnish for GSP-783--//
                    // MessageBox.Show("Invalid Activation Key");

                    if (responseFromServer.Contains("Language Invalid"))//If condition added for GSP-881
                    {
                        if (IsFirstTimeLanguageInvalid)
                        {
                            foreach (Form frm in Application.OpenForms)
                            {
                                frm.Invoke(new Action(() => { MessageBox.Show(frm, Properties.Resources.LANGUAGE_INVALID_KEY); }));
                                break;
                            }
                            IsFirstTimeLanguageInvalid = false;
                        }
                        return "Language Invalid";
                    }
                    else
                    {
                        //--Added By Rajnish for GSP-783--//
                        foreach (Form frm in Application.OpenForms)
                        {
                            frm.Invoke(new Action(() => { MessageBox.Show(frm, Properties.Resources.INVALID_LICENSE_KEY); }));
                            break;
                        }
                    }
                    //--------------------------------//
                    //Edited By Suhana For GSP-450
                    ////Application.Exit();
                    //textBox1.Text = "Invalid Activation Key";
                    return "invalid";
                }
                else if (responseFromServer.ToUpper().Contains("u0010Status".ToUpper()) &&
                    responseFromServer.ToUpper().Contains("u0002ComputerKey\0\u0001\0\0\0\0\0".ToUpper()))
                {
                    //Testing Sumit 31_Aug_18 START
                    //MessageBox.Show("LineNo: 765 GetRenewedComputerKey()" + Environment.NewLine + "Response from Server = "+responseFromServer+
                    //    Environment.NewLine+"JSONSentQry= "+queryString+ Environment.NewLine + "LicenseKey= " + ActivationKey + Environment.NewLine + "ComputerKey= " + ComputerKey, "Testing");
                    //Testing Sumit 31_Aug_18 END
                    return "Non Activated Key";
                }
                #endregion


                #region Valid_License
                string sw = responseFromServer.Replace("?", "");
                sw = sw.Replace("\0", "");
                sw = sw.Replace(@"\u", "");
                sw = sw.Replace(@"�", "");//
                //sw = sw.Replace(sw[7].ToString(), "");//
                sw = sw.Replace("\u0002", "-");
                sw = sw.Replace("\u0016", "");
                sw = sw.Replace("\u0006", "");
                sw = sw.Replace("\u0010", "");
                sw = sw.Replace("~", "");

                string CPK = sw;
                //Added by Sumit on 07-Dec-2018 GSP-942 -----------START
                try
                {
                    sw = (sw.Split('-').Length == 3) ? sw.Split('-')[1] : sw;
                    sw = sw.ToUpper().Replace("COMPUTERKEY", "").Replace(" ", "");
                    CPK = sw;
                }
                catch
                {
                    sw = CPK;
                }
                //Added by Sumit on 07-Dec-2018 GSP-942 -----------START
                string strrm = sw;
                CPK = CPK.Replace("BStatus-ComputerKey ", ""); //"B\0\0\0\u0010Status\0�\0\0\0\u0002ComputerKey\0 \0\0\0V5HA050Z00BGA11H8U85321RP2VSTNN\0\0"
                CPK = CPK.Replace("VStatus-ComputerKey ", "");
                CPK = CPK.Replace("-LicenseType-S-MessageLicense Key Activated-Keyvalid", "");
                CPK = CPK.Replace("-LicenseType\u0001-Message\u000eValid License-KeyValid-Languagevalid", "");
                CPK = CPK.Replace("-Languagevalid", "");
              //  CPK = CPK.Replace("-Language\benglish", "");
              //  CPK = CPK.Replace("XStatus-ComputerKey ", "");

                #region Store_Computer_Key_Locally
                //Keys.ReadKey();

                QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();
                Keys.ActivationKey = ActivationKey;

                Keys.ComputerKey = CPK;
                renewedCompKey = CPK;
                //LicenseStatus st = LicenseStatus.Unknown;
                //Fetch locally stored values first
                //string LicenseKey = txtLicKey.Text; // Registry_Handler.GetActivationKeyFromReg();

                ql = new QlmLicenseLib.QlmLicense();
                //store keys first.
                string ak = Keys.ActivationKey; //Validation Key
                string ck = renewedCompKey; // Computer Key

                //Sumit GSP-775 Testing Comment need ot rollback if not works
                //if (ck.ToUpper().Contains("STATUS"))

                if (ck.ToUpper().Contains("STATUS"))
                {
                    //Testing Sumit 31_Aug_18 START
                    //MessageBox.Show("LineNo: 806 GetRenewedComputerKey()" + Environment.NewLine + "CK= " + ck + Environment.NewLine + "LicenseKey= " + ActivationKey + Environment.NewLine + "ComputerKey= " + ComputerKey, "Testing");

                    //string sjosSend = jsonQuery + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine;
                    //sjosSend += "CK=   " + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine;
                    //sjosSend += responseFromServer;
                    //File.WriteAllText("test_GSP775", sjosSend);
                    //Testing Sumit 31_Aug_18 END
                    return "Non Activated Key";

                }

                //Added by Sumit GSP-775 on 04-Sep-18 1322 ---START
                ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                    , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
                ql.PublicKey = ProductInfo.ProductEncryptionKey;
                string IsKeyDeleted = string.Empty;
                ql.DeleteKeys(out IsKeyDeleted);
                //Added by Sumit GSP-775 on 04-Sep-18 1322 ---END


                ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                    , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
                ql.PublicKey = ProductInfo.ProductEncryptionKey;
                ql.StoreKeysOptions = QlmLicenseLib.EStoreKeysOptions.EStoreKeysPerMachine;//sumit testing july
                ql.EvaluationPerUser = false;//Sumit testing July
                ql.FavorMachineLevelLicenseKey = true;//Sumit testing July
                ql.DefaultWebServiceUrl = Environment.UserName;
                //ql.ProxyUser = Environment.UserName;//Sumit testing July

                //Added by Sumit for GSP-821 on 24-Sep-2018-----------START                
                ql.ProxyUser = DateTime.Now.ToString("yyyy MM dd HH:mm:ss").Replace(" ", "").Replace(":", "");
                //Added by Sumit for GSP-821 on 24-Sep-2018-----------END

                ql.StoreKeys(ak, ck);
                //Added by Sumit GSP-705 ----START
                SecureAccessManager.AccessRightsHandler.SetLicenseReleaseableForAll();
                //Added by Sumit GSP-705 ----END



                //Added by Sumit for GSP-804,GSP-805,GSP-806,GSP-789------------------START   
                if (ak.Trim().Length != 0)
                {
                    ql.ValidateLicense(ak);
                    if (!ql.IsValid() && ql.DaysLeft <= 0)
                    {
                        System.Diagnostics.Process[] allp = System.Diagnostics.Process.GetProcesses();
                        foreach (System.Diagnostics.Process p in allp)
                        {
                            if (p.ProcessName.ToUpper().Contains("yugamiruPGB".ToUpper()))
                            {
                                p.CloseMainWindow();
                            }
                        }
                        //MessageBox.Show(Properties.Resources.CANNOT_LAUNCH_APP + Environment.NewLine + Properties.Resources.SOMETHING_WRONG_WITH_APP, "gsport");                   
                        //--Added By Rajnish for GSP-783--//
                        foreach (Form frm in Application.OpenForms)
                        {
                            frm.Invoke(new Action(() => { MessageBox.Show(frm, Properties.Resources.CANNOT_LAUNCH_APP + Environment.NewLine + Properties.Resources.SOMETHING_WRONG_WITH_APP, "gsport"); }));
                            break;
                        }
                        //--------------------------------//
                        Cleaner.ScrosheetCleaner(); // Sumit GSP-1299
                        Environment.Exit(0);
                    }
                }
                //Added by Sumit for GSP-804,GSP-805,GSP-806,GSP-789------------------END




                if (ak == "")
                    return "invalid";

                #endregion
                #endregion

                if (ComputerKey.Length > 0 && CPK.Trim().ToUpper() != ComputerKey && CPK != "")
                {
                    //Edited By Suhana For GSP-450
                    //System.Windows.Forms.MessageBox.Show("Activation Key renewed successfully", "GSPORT");
                    //System.Windows.Forms.MessageBox.Show(Properties.Resources.ACTIVATION_KEY_RENEWED, "GSPORT",MessageBoxButtons.OK);//--Commented by Rajnish for GSP-783--//

                    //--Added By Rajnish for GSP-783--//
                    foreach (Form frm in Application.OpenForms)
                    {
                        frm.Invoke(new Action(() => { MessageBox.Show(frm, Properties.Resources.ACTIVATION_KEY_RENEWED); }));
                        Utility.SavePresentTimeStamp(); // Added By Suhana
                        showRenewDate = Utility.GetSavedStamp();// Added By Suhana
                        break;
                    }
                    //--------------------------------//

                    //Edited By Suhana For GSP-450
                    if (callingFrom.Length == 0 && !IsCallFromBC)//&& !IsCallFromBC added by Rajnish to handle GSP-771-//
                    {
                        Process myp = new Process();
                        myp.StartInfo.FileName = Application.StartupPath + "\\yugamiruPGB.exe";
                        myp.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
                        myp.Start();
                    }
                }

                ////textBox1.Text = "Key activated successfully";
                //// close everything
                //webResponseSReader.Close();
                //webResponse.Close();
                //webDataStream.Close();
                ////this.Close();
                ////return LicenseStatus.Valid;
                #endregion
            }
            catch (Exception ex)
            {
                //Added Sumit GSP-775 on 28-Aug-18 START
                WebComCation.FaultManager.LogIt(ex);
                //Added Sumit GSP-775 on 28-Aug-18 END
            }
            finally
            {

            }
            //==================
            return renewedCompKey;
        }
        //created by Sumit GSP-1291
        public static bool IsLicenseLocallyValid()
        {
            bool isLicenseValid = false;
            string comp_key = "";
            string license_key = "";
            string computerID = WebComCation.keyRequest.GetComputerID();
            QlmLicenseLib.QlmLicense qls = new QlmLicenseLib.QlmLicense();
            qls.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
               , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
            qls.PublicKey = ProductInfo.ProductEncryptionKey;
            qls.ReadKeys(ref license_key, ref comp_key);

            qls.ValidateLicenseEx(comp_key, keyRequest.GetComputerID());
            qls.GetStatus();

            if (qls.DaysLeft > 0)
            {
                isLicenseValid = true;
            }
            return isLicenseValid;
        }
        public static LicenseStatus ActiavteLicense(string licenseKey, string lcKeyFromReleaseForm = "")//lcKeyFromReleaseForm Added by Sumit for GSP-1059
        {
            //string computerKey = string.Empty;
            if (lcKeyFromReleaseForm.Trim().Length > 0 && licenseKey == "")
            {
                licenseKey = lcKeyFromReleaseForm;
            }
            LicenseStatus st = LicenseStatus.Unknown;
            string APIUrl = string.Empty;
            string ComputerName = keyRequest.GetComputerName();
            string computerID = keyRequest.GetComputerID();
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string jsonQuery = "[{\"Computer_id\":\"" + keyRequest.GetComputerID() +
                                    "\",\"Computer_name\":\"" + keyRequest.GetComputerName() +
                                    "\",\"Activation_key\":\"" + licenseKey + "\",\"Language\":\"" + sCurrent_Language.ToLower().Replace("japanese", "japan") + "\"}]";
                String queryString = jsonQuery;//jsonRawTemplate;//jsonQuery;
                var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(queryString);
                //3.
                JsonSerializer jsonSerializer = new JsonSerializer();
                //4.
                MemoryStream objBsonMemoryStream = new MemoryStream();
                //5.
                Newtonsoft.Json.Bson.BsonWriter bsonWriterObject = new Newtonsoft.Json.Bson.BsonWriter(objBsonMemoryStream);
                //6.
                jsonSerializer.Serialize(bsonWriterObject, studentObject);
                //text = queryString;
                byte[] requestByte = objBsonMemoryStream.ToArray();//= Encoding.Default.GetBytes(queryString);



                #region WebRequest

                //Connect to our Yugamiru Web Server
                //WebRequest webRequest = WebRequest.Create("http://gs-demo.jma.website/apioauthdata/index.php/home/activatekeydata");
                WebRequest webRequest = WebRequest.Create(StarterLicense.activateURL);
                webRequest.Method = "POST";
                webRequest.ContentType = "application/json";
                webRequest.ContentLength = requestByte.Length;
                Stream webDataStream = null;
                try
                {
                    webDataStream = webRequest.GetRequestStream();
                    webDataStream.Write(requestByte, 0, requestByte.Length);
                }
                catch (Exception ex)
                {
                    //Added Sumit GSP-775 on 28-Aug-18 START
                    WebComCation.FaultManager.LogIt(ex);
                    //Added Sumit GSP-775 on 28-Aug-18 END
                    using (var client = new WebClient())
                    {
                        try
                        {
                            using (client.OpenRead("http://clients3.google.com/generate_204"))
                            {
                                //System.Windows.Forms.MessageBox.Show("You are not connected to internet");
                            }
                        }
                        catch (Exception ex1)
                        {
                            //Edited By Suhana for GSP-450
                            System.Windows.Forms.MessageBox.Show(Properties.Resources.PC_NOT_CONNECTED);
                            // System.Windows.Forms.MessageBox.Show("PC is not connected to internet");
                            //Edited By Suhana for GSP-450
                            //Added Sumit GSP-775 on 28-Aug-18 START
                            WebComCation.FaultManager.LogIt(ex1);
                            //Added Sumit GSP-775 on 28-Aug-18 END
                            return LicenseStatus.Error;
                        }
                    }
                }
                string ed = webDataStream.ToString();

                // get the response from our stream

                WebResponse webResponse = webRequest.GetResponse();
                webDataStream = webResponse.GetResponseStream();

                // convert the result into a String
                StreamReader webResponseSReader = new StreamReader(webDataStream);
                //Try Catch added by sumit for GSP-1078-----------START
                //String responseFromServer = webResponseSReader.ReadToEnd();
                String responseFromServer = "";

                try
                {
                    responseFromServer = webResponseSReader.ReadToEnd();
                }
                catch
                {
                    if (!Utility.IsInternetConnected())
                        System.Windows.Forms.MessageBox.Show(Properties.Resources.PC_NOT_CONNECTED);

                    return LicenseStatus.Error;
                }
                //Try Catch added by sumit for GSP-1078-----------END


                #region License_Expired_Case
                if (responseFromServer.Contains("invalid") && responseFromServer.Contains("expired"))
                {
                    int n1 = responseFromServer.IndexOf("Message");
                    string p1 = responseFromServer.Substring(n1 + 10);
                    p1 = p1.Replace("\0", "");
                    p1 = p1.Replace(@"\u", "");
                    p1 = p1.Replace(@"�", "");//                    
                    p1 = p1.Replace(@":", "");
                    p1 = p1.Substring(0, 112);
                    //Edited By Suhana for GSP-450

                    //--Added By Rajnish For GSP-768--//
                    objfrmCustomMessageBox.lblMessage.Text = Properties.Resources.ACTIVATION_KEY_EXPIRED;
                    objfrmCustomMessageBox.tbDetails.Text = "Activation key : " + sActivationKey + Environment.NewLine + "Activation Date : " + sActivationDate + Environment.NewLine + "Expiry Date : " + sExpiryDate;
                    objfrmCustomMessageBox.btnOK.Focus();
                    objfrmCustomMessageBox.TopMost = true;
                    //objfrmCustomMessageBox.ShowDialog();
                    //----------------------//

                    //--Added By Rajnish for GSP-783--//
                    foreach (Form frm in Application.OpenForms)
                    {
                        frm.Invoke(new Action(() => { objfrmCustomMessageBox.ShowDialog(); }));
                        //frm.Invoke(new Action(() => { MessageBox.Show(frm, Properties.Resources.ACTIVATION_KEY_EXPIRED); }));
                        break;
                    }
                    //--------------------------------//
                    //MessageBox.Show(Properties.Resources.ACTIVATION_KEY_EXPIRED);//+ p1);//--commented by Rajnish for GSP-783--//
                    //  MessageBox.Show("Activation Key Expired");//+ p1);
                    //Edited By Suhana for GSP-450
                    //textBox1.Text = "License Key Expired";//+ p1;

                    return LicenseStatus.Expired;
                    //Application.Exit();
                }
                #endregion


                #region Invalid_License
                else if (responseFromServer.Contains("invalid"))
                {
                    //Edited By Suhana for GSP-450
                    //  MessageBox.Show("Invalid Activation Key");
                    //MessageBox.Show(Properties.Resources.INVALID_LICENSE_KEY);//--Commented By Rajnish for GSP-783--//
                    //--Added By Rajnish for GSP-783--//
                    foreach (Form frm in Application.OpenForms)
                    {
                        frm.Invoke(new Action(() => { MessageBox.Show(frm, Properties.Resources.INVALID_LICENSE_KEY); }));
                        break;
                    }
                    //--------------------------------//
                    //Edited By Suhana for GSP-450
                    ////Application.Exit();
                    //textBox1.Text = "Invalid License Key";
                    return LicenseStatus.Invalid;
                }
                #endregion


                #region Valid_License
                string sw = responseFromServer.Replace("?", "");
                sw = sw.Replace("\0", "");
                sw = sw.Replace(@"\u", "");
                sw = sw.Replace(@"�", "");//
                //sw = sw.Replace(sw[7].ToString(), "");//
                sw = sw.Replace("\u0002", "-");
                sw = sw.Replace("\u0016", "");
                sw = sw.Replace("\u0006", "");
                sw = sw.Replace("\u0010", "");

                string CPK = sw;
                string strrm = sw;
                CPK = CPK.Replace("Status-ComputerKey ", "");
                CPK = CPK.Replace("-LicenseType-S-MessageLicense Key Activated-Keyvalid", "");

                #region Store_Computer_Key_Locally

                //Keys.ReadKey();

                QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();
                //Keys.ActivationKey = licenseKey;
                //Keys.ComputerKey = CPK;
                //Registry_Handler.SaveComputerKeyToRegistry(CPK);
                ////Keys.StoreAKey();
                //Keys.ReadKey();
                //Registry_Handler.SaveCurrentRunDateToReg();
                #endregion

                //Added by Sumit GSP-775 on 04-Sep-18 1322 ---START
                ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                    , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
                ql.PublicKey = ProductInfo.ProductEncryptionKey;
                string IsKeyDeleted = string.Empty;
                ql.DeleteKeys(out IsKeyDeleted);
                //Added by Sumit GSP-775 on 04-Sep-18 1322 ---END
                //New Code Added by Sumit GSP-775 can be rolled back later---------START
                #region storeKeys In PC New
                ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                    , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
                ql.PublicKey = ProductInfo.ProductEncryptionKey;
                ql.StoreKeysOptions = QlmLicenseLib.EStoreKeysOptions.EStoreKeysPerMachine;//sumit testing july
                ql.EvaluationPerUser = false;//Sumit testing July
                ql.FavorMachineLevelLicenseKey = true;//Sumit testing July
                ql.DefaultWebServiceUrl = Environment.UserName;
                //ql.ProxyUser = Environment.UserName;//Sumit testing July
                //Added by Sumit for GSP-821 on 24-Sep-2018-----------START                
                ql.ProxyUser = DateTime.Now.ToString("yyyy MM dd HH:mm:ss").Replace(" ", "").Replace(":", "");
                //Added by Sumit for GSP-821 on 24-Sep-2018-----------END

                ql.StoreKeys(licenseKey, CPK);
                //Added by Sumit GSP-705 ----START
                SecureAccessManager.AccessRightsHandler.SetLicenseReleaseableForAll();
                //Added by Sumit GSP-705 ----END







                #endregion
                //New Code Added by Sumit GSP-775 can be rolled back later---------END


                #endregion
                //Edited By Suhana for GSP-450
                // System.Windows.Forms.MessageBox.Show("Activation Key activated successfully", "GSPORT");
                //System.Windows.Forms.MessageBox.Show(Properties.Resources.KEY_ACTIVATED, "GSPORT");//--Commented By Rajnish for GSP-783--//

                //--Added By Rajnish for GSP-783--//
                //--Added By Rajnish for GSP-783--//
                if (lcKeyFromReleaseForm.Length == 0)//Condition added by sumit GSP-1059
                {

                    foreach (Form frm in Application.OpenForms)
                    {
                        frm.Invoke(new Action(() => { MessageBox.Show(frm, Properties.Resources.KEY_ACTIVATED, "GSPORT"); }));
                        break;
                    }
                    //--------------------------------//

                    //Edited By Suhana for GSP-450
                    Cursor.Current = Cursors.WaitCursor;
                    //textBox1.Text = "Key activated successfully";
                    Process myp = new Process();
                    myp.StartInfo.FileName = Application.StartupPath + "\\yugamiruPGB.exe";
                    myp.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
                    myp.Start();
                }
                // close everything
                //webResponseSReader.Close();
                //webResponse.Close();
                //webDataStream.Close();
                //this.Close();
                return LicenseStatus.Valid;
                #endregion
            }
            catch (Exception ex)
            {
                //Added Sumit GSP-775 on 28-Aug-18 START
                WebComCation.FaultManager.LogIt(ex);
                //Added Sumit GSP-775 on 28-Aug-18 END
            }
            finally
            {

            }


            return st;
        }

        static string ShowDebugDetails(string message = "Line Number",
    [CallerLineNumber] int lineNumber = 0,
    [CallerMemberName] string caller = "")
        {
            return Environment.NewLine + message + " at line " + lineNumber + " (" + caller + ")";
        }
    }
}
