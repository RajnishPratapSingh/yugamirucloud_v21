﻿namespace LicenseInformation
{
    partial class frmCloudSycnSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCloudSycnSettings));
            this.btnRefresh = new System.Windows.Forms.Button();
            this.flpnlPCs = new System.Windows.Forms.FlowLayoutPanel();
            this.btnSaveSync = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblPCsAvlblToSync = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblPCsSyncedCount = new System.Windows.Forms.Label();
            this.lblTotalPCCount = new System.Windows.Forms.Label();
            this.chkAll = new System.Windows.Forms.CheckBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnRefresh
            // 
            this.btnRefresh.Location = new System.Drawing.Point(6, 12);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(75, 30);
            this.btnRefresh.TabIndex = 0;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // flpnlPCs
            // 
            this.flpnlPCs.AutoScroll = true;
            this.flpnlPCs.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.flpnlPCs.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flpnlPCs.Location = new System.Drawing.Point(0, 63);
            this.flpnlPCs.Name = "flpnlPCs";
            this.flpnlPCs.Padding = new System.Windows.Forms.Padding(0, 0, 0, 40);
            this.flpnlPCs.Size = new System.Drawing.Size(408, 396);
            this.flpnlPCs.TabIndex = 1;
            // 
            // btnSaveSync
            // 
            this.btnSaveSync.Location = new System.Drawing.Point(87, 12);
            this.btnSaveSync.Name = "btnSaveSync";
            this.btnSaveSync.Size = new System.Drawing.Size(240, 30);
            this.btnSaveSync.TabIndex = 2;
            this.btnSaveSync.Text = "Save Sync Settings";
            this.btnSaveSync.UseVisualStyleBackColor = true;
            this.btnSaveSync.Click += new System.EventHandler(this.btnSaveSync_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(333, 12);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(70, 30);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lblPCsAvlblToSync
            // 
            this.lblPCsAvlblToSync.AutoSize = true;
            this.lblPCsAvlblToSync.Location = new System.Drawing.Point(274, 4);
            this.lblPCsAvlblToSync.Name = "lblPCsAvlblToSync";
            this.lblPCsAvlblToSync.Padding = new System.Windows.Forms.Padding(0, 5, 0, 5);
            this.lblPCsAvlblToSync.Size = new System.Drawing.Size(75, 23);
            this.lblPCsAvlblToSync.TabIndex = 0;
            this.lblPCsAvlblToSync.Tag = "Non Selected:";
            this.lblPCsAvlblToSync.Text = "Non Selected:";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.lblPCsAvlblToSync);
            this.panel1.Controls.Add(this.lblPCsSyncedCount);
            this.panel1.Controls.Add(this.lblTotalPCCount);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 459);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(408, 37);
            this.panel1.TabIndex = 4;
            // 
            // lblPCsSyncedCount
            // 
            this.lblPCsSyncedCount.AutoSize = true;
            this.lblPCsSyncedCount.Location = new System.Drawing.Point(161, 5);
            this.lblPCsSyncedCount.Name = "lblPCsSyncedCount";
            this.lblPCsSyncedCount.Padding = new System.Windows.Forms.Padding(0, 5, 0, 5);
            this.lblPCsSyncedCount.Size = new System.Drawing.Size(58, 23);
            this.lblPCsSyncedCount.TabIndex = 5;
            this.lblPCsSyncedCount.Tag = "Selected : ";
            this.lblPCsSyncedCount.Text = "Selected : ";
            // 
            // lblTotalPCCount
            // 
            this.lblTotalPCCount.AutoSize = true;
            this.lblTotalPCCount.Location = new System.Drawing.Point(3, 5);
            this.lblTotalPCCount.Name = "lblTotalPCCount";
            this.lblTotalPCCount.Padding = new System.Windows.Forms.Padding(0, 5, 0, 5);
            this.lblTotalPCCount.Size = new System.Drawing.Size(111, 23);
            this.lblTotalPCCount.TabIndex = 5;
            this.lblTotalPCCount.Tag = "Total PCs in network: ";
            this.lblTotalPCCount.Text = "Total PCs in network: ";
            this.lblTotalPCCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkAll
            // 
            this.chkAll.AutoSize = true;
            this.chkAll.Checked = true;
            this.chkAll.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.chkAll.Location = new System.Drawing.Point(8, 46);
            this.chkAll.Name = "chkAll";
            this.chkAll.Size = new System.Drawing.Size(37, 17);
            this.chkAll.TabIndex = 5;
            this.chkAll.Text = "All";
            this.chkAll.ThreeState = true;
            this.chkAll.UseVisualStyleBackColor = true;
            this.chkAll.Visible = false;
            this.chkAll.CheckedChanged += new System.EventHandler(this.chkAll_CheckedChanged);
            // 
            // frmCloudSycnSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(408, 496);
            this.Controls.Add(this.chkAll);
            this.Controls.Add(this.flpnlPCs);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnSaveSync);
            this.Controls.Add(this.btnRefresh);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmCloudSycnSettings";
            this.Text = "Yugamiru Cloud: Data Synchronization Settings";
            this.Load += new System.EventHandler(this.frmCloudSycnSettings_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.FlowLayoutPanel flpnlPCs;
        private System.Windows.Forms.Button btnSaveSync;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lblPCsAvlblToSync;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblPCsSyncedCount;
        private System.Windows.Forms.Label lblTotalPCCount;
        private System.Windows.Forms.CheckBox chkAll;
    }
}